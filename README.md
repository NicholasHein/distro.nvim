# `distro.nvim`

## Setup

**Requirements:**

- [`lazy.nvim`](https://github.com/folke/lazy.nvim)

```lua
require("lazy").setup {
  {
    url = "https://gitlab.com/NicholasHein/distro.nvim.git",
    import = "distro.lazy",
    opts = {
      -- Enable the mason package registries
      --registries = { mason = true },
    },
  },
  -- { import = "distro.lazy.ext.langs.all" },
  -- { import = "plugins" }
}
```

### `mason.nvim` Package Registry

There are two Mason package registries:

- Schema registry package source (requires `yq`: `:MasonInstall yq`)
- Lua registry package source

## Development

```lua
require("lazy").setup({
  dev = { path = "~/Projects" },
  spec = {
    {
      url = "https://gitlab.com/NicholasHein/distro.nvim.git",
      import = "distro.lazy",
      dev = true,
      -- ...
    },
  },
})
```

### `mason.nvim` Packages

#### Schema Package Registry

Simply add a package definition file conforming to the specification[^mason-pkgspec]
under: `packages/<name>/package.yaml`.

Refer to `mason-registry` for more info[^mason-registry-instructions].

[^mason-pkgspec]: <https://github.com/mason-org/mason-registry/blob/main/CONTRIBUTING.md#package-specification>

[^mason-registry-instructions]: <https://github.com/mason-org/mason-registry/blob/main/CONTRIBUTING.md#testing>

#### Lua Package Registry

Refer to `mason.nvim` for more info[^mason-api] (see the example in the
tests[^mason-test-lua-registry]).

[^mason-api]: <https://github.com/williamboman/mason.nvim/blob/main/doc/reference.md>

[^mason-test-lua-registry]: <https://github.com/williamboman/mason.nvim/tree/main/tests/helpers/lua/dummy-registry>

```lua
-- lua/distro-mason/registry.lua
return {
  ["example"] = "distro-mason.registry.example",
}

-- lua/distro-mason/registry/example.lua
local Package = require "mason-core.package"

return Package.new {
  -- ...
}
```

##### Using a PURL ID

**Package URL Specification:** <https://github.com/package-url/purl-spec>

```lua
-- lua/distro-mason/registry/example.lua
local Package = require "mason-core.package"

return Package.new {
	schema = "registry+v1",
	name = "example",
	description = [[This is an example "Package URL" (PURL) package.]],
	licenses = { "MIT" },
	categories = { "LSP" },
	languages = { "FakeLang" },
	homepage = "https://example.com",
	source = { id = "pkg:cargo/example@1.0.0" },
}
```

##### Manual Installer

```lua
-- lua/distro-mason/registry/example.lua
local Package = require "mason-core.package"

return Package.new {
	name = "example",
	desc = [[This is an example package.]],
	categories = { Package.Cat.LSP },
	languages = { Package.Lang.FakeLang },
	homepage = "https://example.com",
	---@async
	---@param ctx InstallContext
	install = function(ctx)
		ctx.receipt:with_primary_source { type = "cargo" }
	end
}
```
