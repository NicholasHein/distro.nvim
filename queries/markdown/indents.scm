;; extends

((list_item) @indent.begin
  (#set! indent.immediate 1))

((list_item (block_continuation)) @indent.dedent
  (#set! indent.immediate 1))

(ERROR) @indent.auto
