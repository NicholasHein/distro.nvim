# Tree-Sitter Queries

## Resources

- Syntax Reference:
  <https://tree-sitter.github.io/tree-sitter/using-parsers#pattern-matching-with-queries>
- NeoVim Captures Reference:
  <https://github.com/nvim-treesitter/nvim-treesitter/blob/master/CONTRIBUTING.md#parser-configurations>
