;; extends

; Highlight table headers
(table
  [
    "["
    "]"
    (bare_key)
    (dotted_key)
  ] @markup.heading)

; Overlay highlighting for keys
(table
  [
    (bare_key)
    (dotted_key)
  ] @property)
