local Config = require("distro.config")
if not Config.is_init then
	return
end

if Config.experimental.fancy_intro then
	-- vim.iter() is only available in 0.10
	-- TODO: why does this not work: vim.version.range(">=0.10"):has(vim.version())
	if vim.version.gt(vim.version(), { 0, 9 }) then
		require("distro.contrib.intro")
	else
		Config.log:warn("nvim version must be >= 0.10 to enable experimental.fancy_intro")
	end
end

if Config.extras.scratch_buffer_commands then
	local scratch = require("distro.utils.buffer.scratch")
	scratch.setup()
end

if Config.extras.split_terminal then
	local splitterm = require("distro.utils.terminal.split")
	splitterm.setup()
end

require("distro.extras.diagnostics").setup {}
