local once = require("distro.core.once")

once(function()
	local visual = require("lib.editor.visual")
	vim.keymap.set("x", [[<M-`>]], visual.expr_surround_selection("`"), {
		desc = "surround with ``",
		buffer = vim.api.nvim_get_current_buf(),
	})
end, { buf = true })
