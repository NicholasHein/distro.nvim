# Version: MAJOR[.MINOR[.PATCH]][EXTRA][LOCALVERSION]
# (eg. v1.0.8-rc3-foo2)
VERSION_MAJOR = 0
VERSION_MINOR = 4
VERSION_PATCH =
VERSION_EXTRA = -rc1

ifeq ("$(origin ${VERSION_EXTRA})", "command line")
localversion = ${VERSION_EXTRA}
else
localversion = $(file <$(or ${LOCALVERSION},localversion))
endif

VERSION  = ${VERSION_MAJOR}$(if ${VERSION_MINOR},.${VERSION_MINOR}$(if ${VERSION_PATCH},.${VERSION_PATCH}))
VERSION := ${VERSION}$(strip ${VERSION_EXTRA})$(strip ${localversion})

.PHONY: all
all:

.PHONY: version
version:
	@echo "${VERSION}"

targets :=
obj :=

$(if $(filter __%, ${MAKECMDGOALS}), \
	$(error targets prefixed with '__' are only for internal use))

out = ${CURDIR}
me := $(lastword ${MAKEFILE_LIST})
abs_srctree := $(realpath $(dir ${me}))
VPATH := ${abs_srctree}

NVIM = $(shell command -v foobar)
define nvim-mkspell
$(NVIM) --headless \
	--cmd 'mkspell! $@ $^' \
	--cmd 'qa!'
endef

VIM = $(shell command -v vim)
define vim-mkspell
$(VIM) -f -U NONE \
	--cmd 'mkspell! $@ $^' \
	--cmd 'qa!'
endef

MKSPELL = $(or $(if $(NVIM), ${nvim-mkspell}), $(if $(VIM), ${vim-mkspell}))

spell :=
spell += en.utf-8.add.spl

spell-files = $(addprefix ${out}/spell/,${spell})
targets += ${spell-files}

.PHONY: spell
spell: ${spell-files}

${out}/%.add.spl: %.add
	$(if $(MKSPELL),,$(error nvim/vim required for mkspell))
	$(MKSPELL)

clean-files = $(strip $(wildcard ${obj}))

.PHONY: clean
clean:
	-@$(if ${clean-files}, \
		rm -rf ${clean-files}, \
		:)

distclean-files = $(strip $(wildcard ${targets}) ${clean-files})

.PHONY: distclean
distclean:
	-@$(if ${distclean-files}, \
		rm -rf ${distclean-files}, \
		:)

compose-services = \
	alpine archlinux debian ubuntu
COMPOSE_SERVICE = $(firstword ${compose-services})

.PHONY: compose
compose: compose-run-${COMPOSE_SERVICE}

compose-targets = $(addprefix compose-,${compose-services})
.PHONY: ${compose-targets}
${compose-targets}: compose-%: compose-run-%

.PHONY: compose-run-%
compose-run-%: docker-compose.yml
	docker-compose -f $< build nvim-$*
	docker-compose -f $< run --rm nvim-$*

$(shell mkdir -p $(dir ${targets}))

.DELETE_ON_ERROR:
.SUFFIXES:
.SECONDARY:
