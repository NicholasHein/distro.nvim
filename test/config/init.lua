local uv = vim.loop

vim.g.mapleader = ","

require("bootstrap.lazy").bootstrap({
	dev = { path = uv.os_homedir() },
}):setup("plugins")
