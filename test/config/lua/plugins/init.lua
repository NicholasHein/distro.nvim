return {
	{
		dir = vim.loop.os_homedir() .. "/distro.nvim",
		import = "distro.lazy",
		opts = {
			colorscheme = "nightfox",
		},
	},
	-- TODO: { "site/distro.nvim", import = "distro.lazy" },
	{
		"EdenEast/nightfox.nvim",
		lazy = false,
		opts = {
			options = {
				transparent = true,
				dim_inactive = false,
			},
		},
	},
	{ import = "distro.lazy.ext.langs.all" },
}
