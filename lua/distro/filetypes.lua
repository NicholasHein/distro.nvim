vim.cmd [[filetype plugin indent on]]

vim.filetype.add({
	extension = {
		["automount"] = "systemd",
		["defconfig"] = "make",
		["device"] = "systemd",
		["gitconfig"] = "gitconfig",
		["h"] = "c",
		["its"] = "dts",
		["mk"] = "make",
		["mount"] = "systemd",
		["org"] = "org",
		["path"] = "systemd",
		["scope"] = "systemd",
		["service"] = "systemd",
		["slice"] = "systemd",
		["socket"] = "systemd",
		["swap"] = "systemd",
		["target"] = "systemd",
		["timer"] = "systemd",
	},
	filename = {
		[".config"] = "make",
		[".defconfig"] = "make",
		[".envrc"] = "sh",
		[".gitlab-ci.yml"] = "yaml.gitlab",
		["Config.in"] = "kconfig",
		["Kbuild"] = "make",
		["PKGBUILD"] = "bash",
		["defconfig"] = "make",
		["docker-compose.yml"] = "yaml.docker-compose",
	},
	pattern = {
		[".*/systemd/.*%.automount"] = "systemd",
		[".*/systemd/.*%.conf"] = "systemd",
		[".*/systemd/.*%.device"] = "systemd",
		[".*/systemd/.*%.mount"] = "systemd",
		[".*/systemd/.*%.path"] = "systemd",
		[".*/systemd/.*%.scope"] = "systemd",
		[".*/systemd/.*%.service"] = "systemd",
		[".*/systemd/.*%.slice"] = "systemd",
		[".*/systemd/.*%.socket"] = "systemd",
		[".*/systemd/.*%.swap"] = "systemd",
		[".*/systemd/.*%.target"] = "systemd",
		[".*/systemd/.*%.timer"] = "systemd",
	}
})

vim.treesitter.language.register("bash", { "zsh" })
