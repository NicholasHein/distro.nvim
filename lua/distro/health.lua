-- See `:h health-dev`

local M = {}

function M.check()
	vim.health.start("Running dummy check for distro.nvim")
	vim.health.info("This test doesn't actually do anything right now")
	vim.health.ok("Dummy check passed")
end

return M
