return {
	{
		"nvim-telescope/telescope.nvim",
		dependencies = {
			"nvim-lua/popup.nvim",
			{
				"neovim/nvim-lspconfig",
				optional = true,
				opts = {
					buffer = {
						handlers = {
							code_action = function()
								require("telescope.builtin")
								    .lsp_code_actions()
							end,
							definition = function()
								require("telescope.builtin")
								    .lsp_definitions()
							end,
							implementation = function()
								require("telescope.builtin")
								    .lsp_implementations()
							end,
							references = function()
								require("telescope.builtin")
								    .lsp_references()
							end,
							document_symbol = function()
								require("telescope.builtin")
								    .lsp_document_symbols()
							end,
							workspace_symbol = function()
								require("telescope.builtin")
								    .lsp_dynamic_workspace_symbols()
							end,
						},
						keymaps = {},
					},
				},
			},
		},
		cmd = "Telescope",
		keys = {
			{ [[?]],  [[<Nop>]] },
			{
				[[??]],
				function() require("telescope.builtin").resume() end,
				desc =
				"telescope: resume"
			},

			-- Find files
			{
				[[<M-f>]],
				function() require("telescope.builtin").find_files {} end,
				desc =
				"telescope: find files"
			},
			{
				[[<M-F>]],
				function() require("telescope.builtin").find_files { no_ignore = true } end,
				desc = "telescope: find files (no ignore)",
			},
			{
				[[<M-C-F>]],
				function()
					require("telescope.builtin").find_files { no_ignore = true, cwd = vim.fn
					    .expand("%:p:h") }
				end,
				desc = "telescope: find files from current file",
			},
			{ [[?o]], function() require("telescope.builtin").oldfiles() end, desc = "telescope: old files", },

			-- Grepping
			{
				[[?f]],
				function() require("telescope.builtin").current_buffer_fuzzy_find() end,
				desc = "telescope: fuzzy find in current buffer",
			},
			{ [[?F]], function() require("telescope.builtin").live_grep() end, desc = "telescope: live grep" },

			-- :help
			{ [[?h]], function() require("telescope.builtin").help_tags() end, desc = "telescope: help tags" },
			{
				[[?H]],
				function() require("telescope.builtin").help_tags(require("telescope.themes").get_cursor()) end,
				desc = "telescope: help tags",
			},

			-- Man
			{
				[[?m]],
				function() require("telescope.builtin").man_pages({ sections = { "ALL" } }) end,
				desc = "telescope: man pages",
			},
			{
				[[?M]],
				function()
					require("telescope.builtin").man_pages({
						sections = { "ALL" },
						theme = require("telescope.themes").get_cursor()
					})
				end,
				desc = "telescope: man pages",
			},
		},
		opts = {
			defaults = {
				winblend = 20,
				dynamic_preview_title = true,
				path_display = { "shorten" },
				cycle_layout_list = {
					"flex",
					"horizontal",
					"vertical",
					"bottom_pane",
					-- "center",
					"cursor",
				},
				vimgrep_arguments = {
					"rg",
					"--color=never",
					"--no-heading",
					"--with-filename",
					"--line-number",
					"--column",
					"--smart-case",
					"--trim", -- Added
				},
				mappings = {
					i = {
						["<C-e>"] = function(buf)
							require("telescope.actions").select_vertical(
								buf)
						end,
						["<C-o>"] = function(buf)
							require("telescope.actions").select_horizontal(
								buf)
						end,
						["<C-t>"] = function(buf) require("telescope.actions").select_tab(buf) end,
						["<C-q>"] = function(buf)
							local actions = require("telescope.actions")
							local act = actions.send_to_qflist + actions.open_qflist
							act(buf)
						end,
						["<C-K>"] = function(buf)
							require("telescope.actions")
							    .cycle_history_prev(buf)
						end,
						["<C-J>"] = function(buf)
							require("telescope.actions")
							    .cycle_history_next(buf)
						end,
						["<M-]>"] = function(buf)
							require("telescope.actions.layout")
							    .cycle_layout_next(buf)
						end,
						["<M-[>"] = function(buf)
							require("telescope.actions.layout")
							    .cycle_layout_prev(buf)
						end,
					},
					n = {
						["<C-e>"] = function(buf)
							require("telescope.actions").select_vertical(
								buf)
						end,
						["<C-o>"] = function(buf)
							require("telescope.actions").select_horizontal(
								buf)
						end,
						["<C-t>"] = function(buf) require("telescope.actions").select_tab(buf) end,
						["<C-q>"] = function(buf)
							local actions = require("telescope.actions")
							local act = actions.send_to_qflist + actions.open_qflist
							act(buf)
						end,
						["<M-]>"] = function(buf)
							require("telescope.actions.layout")
							    .cycle_layout_next(buf)
						end,
						["<M-[>"] = function(buf)
							require("telescope.actions.layout")
							    .cycle_layout_prev(buf)
						end,
					}
				}
			},
			pickers = {
				-- FILES:
				find_files                    = {
					-- find_command = { "fd", "--type", "f" }
					theme = "ivy",
					follow = true,
					hidden = true,
					no_ignore = false,
					use_regex = true,
				},

				-- FUZZY-FIND:
				current_buffer_fuzzy_find     = {
					theme = "cursor",
					-- results_title = true,
					path_display = { "tail" }
				},
				live_grep                     = {},

				-- VIM:
				autocommands                  = {},
				buffers                       = { theme = "ivy" },
				oldfiles                      = { theme = "ivy" },
				commands                      = { theme = "ivy" },
				command_history               = { theme = "ivy" },
				filetypes                     = {},
				highlights                    = {},
				jumplist                      = { theme = "ivy" },
				keymaps                       = { theme = "ivy" },
				loclist                       = { theme = "ivy" },
				marks                         = { theme = "ivy" },
				vim_options                   = { theme = "ivy" },
				pickers                       = {},
				quickfix                      = { theme = "ivy" },
				registers                     = { theme = "cursor" },
				search_history                = { theme = "ivy" },
				colorscheme                   = {},
				help_tags                     = {},

				-- COMMAND-LINE:
				man_pages                     = {},

				-- GIT:
				git_files                     = { theme = "ivy" },
				git_status                    = { theme = "ivy" },
				git_commits                   = { theme = "ivy" },
				git_bcommits                  = { theme = "ivy" },
				git_branches                  = { theme = "ivy" },
				git_stash                     = { theme = "ivy" },

				-- WRITING:
				spell_suggest                 = { theme = "cursor" },

				-- LSP:
				lsp_document_definitions      = { theme = "cursor" },
				lsp_implementations           = { theme = "cursor" },
				lsp_type_definitions          = { theme = "cursor" },
				lsp_references                = { theme = "cursor" },
				lsp_document_symbols          = { theme = "cursor" },
				lsp_dynamic_workspace_symbols = {},
				lsp_code_actions              = { theme = "cursor" },
				lsp_document_diagnostics      = { theme = "cursor" },
				lsp_workspace_diagnostics     = {},
			},
		},
	},
	{
		"nvim-telescope/telescope-fzf-native.nvim",
		build = "make",
		enabled = vim.fn.executable("make") == 1,
		cond = function()
			local sys = require("lib.sys")

			local has_sysdeps = sys.is_exe_in_path("rg") and sys.is_exe_in_path("fd")
			if not has_sysdeps then
				vim.notify_once("telescope: missing dependency (requires ripgrep and fdfind)",
					vim.log.levels.WARN)
			end
			return has_sysdeps
		end,
		config = function()
			local function load()
				require("telescope").load_extension("fzf")
			end

			local Config = require("lazy.core.config")
			local telstat = Config.plugins["telescope.nvim"]
			if telstat and telstat._.loaded then
				load()
			else
				vim.api.nvim_create_autocmd("User", {
					pattern = "LazyLoad",
					callback = function(ev)
						if ev.data == "telescope.nvim" then
							load()
							return true -- Delete autocmd
						end
					end,
				})
			end
		end,
	},
}
