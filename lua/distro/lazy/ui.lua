local LspClient = require("distro.lsp.client")

return {
	{
		"nvim-tree/nvim-tree.lua",
		lazy = false,
		dependencies = { "nvim-tree/nvim-web-devicons" },
		-- Breaking change:
		-- <https://github.com/nvim-tree/nvim-tree.lua/commit/cdb40dc42e5521367b9b1c40b8683331cf3500d7>
		commit =
				vim.version.lt(vim.version(), { 0, 8 }) and "949913f1860eb85024fa1967dbd89ac797777b0d" or "*",
		init = function()
			vim.g.loaded_netrw = 1
			vim.g.loaded_netrwPlugin = 1
		end,
		opts = {
			diagnostics = { enable = true },
			reload_on_bufenter = true,
			sync_root_with_cwd = true,
			respect_buf_cwd = true,
			-- This seems to be broken as of 03ae603[^1]
			--
			-- [^1]: <https://github.com/nvim-tree/nvim-tree.lua/commit/03ae60313b1138bfed5e5de889cdbf80756e1397>
			--
			--hijack_unnamed_buffer_when_opening = true,
			hijack_cursor = false,
			git = { ignore = false },
			view = { preserve_window_proportions = true },
			actions = {
				change_dir = {
					enable = true,
					global = true,
				},
				open_file = {
					window_picker = { enable = false }, -- It kinda annoys me
				},
			},
			on_attach = function(bufnr)
				local map = function(lhs, callback, desc)
					if type(lhs) == "string" then
						lhs = { lhs }
					end
					assert(type(lhs) == "table")

					for _, l in ipairs(lhs) do
						vim.api.nvim_buf_set_keymap(bufnr, "n", l, "", {
							noremap = true,
							silent = true,
							nowait = true,
							desc = desc,
							callback = function()
								callback()
							end,
						})
					end
				end

				local api = require("nvim-tree.api")
				-- Core Functionality
				map({ "<Space>", "<C-Space>", "q" }, api.tree.close, "close")
				map("R", api.tree.reload, "reload")
				map("S", api.tree.search_node, "search")
				-- Navigation
				map({ "<CR>", "o" }, api.node.open.edit, "expand/open")
				map("L", api.tree.expand_all, "expand all")
				map("H", api.tree.collapse_all, "collapse all")
				map("K", api.node.navigate.sibling.first, "jump to first sibling")
				map("J", api.node.navigate.sibling.last, "jump to last sibling")
				map(",k", api.node.navigate.sibling.prev, "goto previous sibling")
				map(",j", api.node.navigate.sibling.next, "goto next sibling")
				map("P", api.node.navigate.parent, "jump to parent")
				-- Editing
				map("s", api.node.open.vertical, "vertical split")
				map("i", api.node.open.horizontal, "horizontal split")
				map({ "t", "<C-t>" }, api.node.open.tab, "open in new tab")
				map("O", api.node.open.preview, "preview")
				map("!", api.node.run.cmd, "run command")
				map("@", api.node.run.system, "open with system program")
				map("I", api.node.show_info_popup, "show info")
				map("<M-c>", api.fs.copy.absolute_path, "copy absolute path")
				map("<M-C>", api.fs.copy.relative_path, "copy relative path")
				-- Traversal
				map("C", api.tree.change_root_to_node, "change directory")
				map("u", api.tree.change_root_to_parent, "change directory to parent")
				-- File Operations
				map("a", api.fs.create, "create file")
				map("d", api.fs.remove, "delete file")
				map("D", api.fs.trash, "move file to trash")
				map("r", api.fs.rename, "rename: absolute path") -- FIXME: broken by <https://github.com/nvim-tree/nvim-tree.lua/commit/a38f9a55a4b55b0aa18af7abfde2c17a30959bdf>
				map("<C-R>", api.fs.rename_sub, "rename: dirname")
				-- Clipboard
				map("c", api.fs.copy.node, "copy file")
				map("x", api.fs.cut, "cut file")
				map("p", api.fs.paste, "paste file")
				-- Marks
				map("m", api.marks.toggle, "toggle bookmark")
				map("M", api.marks.bulk.move, "bulk-move all marked files")
				map("<C-M>", api.marks.clear, "clear file marks")
				-- Other
				map("g?", api.tree.toggle_help, "toggle help")
			end,
		},
		keys = {
			{
				"<Space>",
				function()
					local api = require("nvim-tree.api")
					api.tree.focus()
				end,
				desc = "nvim-tree: focus on the tree window",
			},
			{
				"<C-Space>",
				function()
					local api = require("nvim-tree.api")

					local fname = vim.fn.expand("%")
					fname = vim.fn.fnamemodify(fname, ":p")

					api.tree.focus()
					api.tree.find_file(fname)
				end,
				desc = "nvim-tree: find file from the buffer under the cursor",
			},
		},
	},
	{
		"antosha417/nvim-lsp-file-operations",
		dependencies = {
			"nvim-lua/plenary.nvim",
			"nvim-tree/nvim-tree.lua",
		},
		opts = {},
		-- FIXME: nvim-lsp-file-operations doesn't support nvim-tree's updated FolderCreated/FolderRemoved event signatures
		config = function(_, opts)
			require("lsp-file-operations").setup(opts)

			for _, modname in ipairs({ "did-create", "did-delete" }) do
				local opmod = require(("lsp-file-operations.%s"):format(modname))
				local inner_cb = opmod.callback

				opmod.callback = function(data)
					-- HACK: if fname isn't set, try to pass folder_name
					inner_cb { fname = data.fname or data.folder_name }
				end
			end
		end,
	},
	{
		"stevearc/dressing.nvim",
		opts = {},
	},
	{
		"rcarriga/nvim-notify",
		opts = {},
	},
	{
		-- FIXME: Switch to upstream once pull request is merged
		--   "feat: add MarkSignLineHL": <https://github.com/chentoast/marks.nvim/pull/93>
		-- "chentoast/marks.nvim",
		"rapan931/marks.nvim",
		branch = "feat-linehl",
		opts = { sign_priority = 1000 },
	},
	{
		"kshenoy/vim-signature",
		init = function()
			vim.g.SignatureMarkTextHLDynamic = 1
			vim.g.SignatureMarkerTextHLDynamic = 1
		end,
	},
	{
		"folke/which-key.nvim",
		event = "VeryLazy",
		opts = {
			plugins = {
				spelling = { enabled = true },
			},
		},
		config = true,
	},
	{
		"folke/noice.nvim", -- Replaces UI for messages, cmdline, and pum
		optional = true,
		event = "VeryLazy",
		dependencies = {
			{ "MunifTanjim/nui.nvim" },
			{
				"folke/which-key.nvim",
				optional = true,
				opts = {
					defaults = {
						["<leader>k?"] = { name = "+noice" },
					},
				},
			},
		},
		opts = {
			presets = {
				bottom_search = true,
				command_palette = true,
				long_message_to_split = true,
				inc_rename = false,
			},
			popupmenu = { backend = "cmp" },
			cmdline = {
				view = "cmdline",
				format = {
					search_down = { view = "cmdline" },
					search_up = { view = "cmdline" },
				},
			},
			lsp = {
				override = {
					["vim.lsp.util.convert_input_to_markdown_lines"] = true,
					["vim.lsp.util.stylize_markdown"] = true,
					["cmp.entry.get_documentation"] = true,
				},
			},
			routes = {
				-- Editor messages
				{
					view = "mini",
					opts = { replace = true },
					filter = {
						event = "msg_show",
						any = {
							{ find = "%d+L, %d+B" },
							{ find = "; after #%d+" },
							{ find = "; before #%d+" },
							{ find = "%d more lines" },
							{ find = "%d fewer lines" },
							{ find = "%d lines yanked" },
							{ find = "%d lines [<>]ed %d time[s]?" },
							{ find = "Already at oldest change" },
							{ find = "No more valid diagnostics" },
							{ find = "Already at newest change" },
							{ find = "search hit BOTTOM",          warning = true },
							{ find = "search hit TOP",             warning = true },
							{ find = "Pattern not found:",         error = true },
						},
					},
				},
				-- :Noice ...: Noice command output
				{
					view = "cmdline_popup",
					opts = {
						lang = "lua",
						enter = true,
						replace = true,
						title = "Noice",
						close = { keys = { "q" }, events = { "BufLeave" } },
					},
					filter = {
						event = "noice",
						kind = { "stats", "debug" },
						min_height = 20,
					},
				},
				-- :lua ...: multi-line messages show up as multiple notifications, so can't filter by height
				{
					view = "cmdline_popup",
					opts = {
						lang = "lua",
						enter = true,
						replace = true,
						merge = true,
						close = { keys = { "q" }, events = { "BufLeave" } },
					},
					filter = {
						event = "msg_show",
						kind = "",
						cmdline = ":lua .+",
					},
				},
				-- : ...: multi-line output
				{
					view = "cmdline_popup",
					opts = {
						enter = true,
						replace = true,
						merge = true,
						close = { keys = { "q" }, events = { "BufLeave" } },
					},
					filter = {
						event = "msg_show",
						kind = "",
						cmdline = ":.+",
						min_height = 10,
					},
				},
			},
		},
		keys = {
			{ [[<leader>M]], function() require("noice").cmd("last") end,    desc = "noice: last message" },
			{ [[<leader>D]], function() require("noice").cmd("dismiss") end, desc = "noice: dismiss all" },
		},
	},
	{
		"folke/trouble.nvim",
		cmd = { "TroubleToggle", "Trouble" },
		opts = { use_diagnostic_signs = true },
		keys = {
			-- TODO: document_diagnostics/workspace_diagnostics
		},
	},
	{
		"stevearc/aerial.nvim",
		dependencies = {
			"nvim-treesitter/nvim-treesitter",
			"nvim-tree/nvim-web-devicons",
		},
		opts = {
			on_attach = function(bufnr)
				vim.keymap.set("n", [[<leader>a]], function() require("aerial").open {} end, {
					buffer = bufnr,
					desc = "aerial: open",
				})
			end,
		},
		config = function(_, opts)
			local aerial = require("aerial")
			aerial.setup(opts)

			local telescope = require("telescope")
			telescope.load_extension("aerial")
		end
	},
	{
		"kevinhwang91/nvim-ufo",
		dependencies = {
			{ "kevinhwang91/promise-async" },
			{
				"neovim/nvim-lspconfig",
				opts = {
					capabilities = {
						textDocument = {
							foldingRange = {
								dynamicRegistration = false,
								lineFoldingOnly = true,
							},
						},
					},
				},
			},
		},
		init = function()
			vim.o.foldcolumn = "auto"
			vim.o.foldlevel = 99
			vim.o.foldlevelstart = 99
		end,
		---@class NvimUfoOpts
		opts = {
			---@alias NvimUfoMainProviderKind
			---| '"lsp"'
			---| '"treesitter"'

			---@alias NvimUfoFallbackProviderKind
			---| '"indent"' # Provide folding based on line indentation

			---@class NvimUfoProviderSelectionWithFallback
			---@field [1] NvimUfoMainProviderKind
			---@field [2]? NvimUfoFallbackProviderKind

			---@alias NvimUfoProviderSelection NvimUfoProviderSelectionWithFallback | NvimUfoMainProviderKind | NvimUfoFallbackProviderKind

			---@param bufnr integer
			---@return NvimUfoProviderSelection
			provider_selector = function(bufnr, _, _)
				---@type NvimUfoMainProviderKind
				local main_provider = "treesitter"
				---@type NvimUfoFallbackProviderKind
				local fallback_provider = "indent"

				if #vim.lsp.get_clients({ bufnr = bufnr }) > 0 then
					-- Use lsp as "main" provider and indentation level as a "fallback" provider
					main_provider = "lsp"
				end
				return { main_provider, fallback_provider }
			end,
		},
	},
	{
		"nvim-treesitter/nvim-treesitter-context",
		dependencies = { "nvim-treesitter/nvim-treesitter" },
		---@class NvimTreesitterContextOpts
		opts = {
			max_lines = 1,
			multiline_threshold = 1,
			trim_scope = "inner",
			mode = "topline",
			---@type fun(bufnr: integer): boolean
			on_attach = function(bufnr)
				vim.keymap.set("n", [[<leader>C]], function()
					require("treesitter-context").go_to_context(vim.v.count1)
				end, {
					buffer = bufnr,
					desc = "treesitter-context: jump up to context",
				})
				return true
			end,
		},
	},
}
