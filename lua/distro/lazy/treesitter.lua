return {
	{
		"nvim-treesitter/nvim-treesitter",
		-- BUG:
		--   See: <https://github.com/nvim-treesitter/nvim-treesitter/issues/3655>
		--   Fixed by (?): <https://github.com/nvim-treesitter/nvim-treesitter/pull/3693>
		--   Forked Repo: 'ObserverOfTime/nvim-treesitter' (commit = 'eeb816291')
		--   Last good commit: 4cccb6f494eb255b32a290d37c35ca12584c74d0
		-- NOTES:
		--   - Is it a version difference between NeoVim 0.7 and 0.8?
		commit =
				vim.version.lt(vim.version(), { 0, 8 }) and "4cccb6f494eb255b32a290d37c35ca12584c74d0" or nil,
		build = function()
			local ts_install_utils = require("nvim-treesitter.install")
			local ts_update_fn = ts_install_utils.update({ with_sync = true })
			ts_update_fn()
		end,
		main = "nvim-treesitter.configs",
		dependencies = {
			{
				"williamboman/mason.nvim",
				---@param opts distro.lazy.MasonOpts
				opts = function(_, opts)
					vim.list_extend(opts.ensure_installed, { "tree-sitter-cli" })
				end,
			},
		},
		opts = {
			ensure_installed = {
				"comment",
				"csv",
				"diff",
				"embedded_template",
				"gpg",
				"objdump",
				"passwd",
				"regex",
				"ssh_config",
				"todotxt",
				"toml",
			},
			auto_install = true,
			indent = { enable = true },
			highlight = {
				enable = true,
				disable = function(_ --[[lang]], buf)
					local max_filesize = 100 * 1024 -- 100 KB
					local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
					if ok and stats and stats.size > max_filesize then
						return true
					end
				end,
				additional_vim_regex_highlighting = {},
			},
		}
	},
	{
		"nvim-treesitter/playground",
		lazy = true,
		cmd = {
			"TSPlaygroundToggle",
			"TSNodeUnderCursor",
			"TSHighlightCapturesUnderCursor",
		},
	},
}
