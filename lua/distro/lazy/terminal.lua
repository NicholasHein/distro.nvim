return {
	{ "direnv/direnv.vim" },
	{
		"akinsho/toggleterm.nvim",
		version = "*",
		opts = {
			direction = "float",
			float_opts = { winblend = 15 },
			on_open = function(term)
				vim.keymap.set(
					{ "n", "t" },
					[[<M-CR>]],
					[[<cmd>close<CR>]],
					{
						silent = true,
						buffer = term.bufnr,
						desc = "toggleterm: close",
					}
				)
			end,
		},
		cmd = "ToggleTerm",
		keys = {
			{ [[<M-CR>]], [[<cmd>ToggleTerm<cr>]], desc = "toggleterm: open" }
		},
		config = function(_, opts)
			local toggleterm = require("toggleterm")
			toggleterm.setup(opts)

			local Terminal = require("toggleterm.terminal").Terminal
			if vim.fn.exepath("lazygit") ~= "" then
				local lazygit_term = Terminal:new({
					cmd = "lazygit",
					direction = "float",
					hidden = true,
					float_opts = { border = "double" },
				})

				vim.api.nvim_set_keymap("n", "<leader>g", "", {
					noremap = true,
					silent = true,
					desc = "toggleterm: open lazygit",
					callback = function()
						lazygit_term:toggle()
					end,
				})
			end
		end,
	},
}
