return {
	{
		"windwp/nvim-autopairs",
		event = "InsertEnter",
		dependencies = {
			{
				"hrsh7th/nvim-cmp",
				optional = true,
				opts = function(_, opts)
					opts.events = opts.events or {}
					opts.events.confirm_done = opts.events.confirm_done or {}
					table.insert(opts.events.confirm_done, function(...)
						local pairs_cmp = require("nvim-autopairs.completion.cmp")
						local fn = pairs_cmp.on_confirm_done()
						return fn(...)
					end)
				end,
			},
		},
		opts = {
			check_ts = true,
		},
	},
	{
		-- TODO: https://github.com/RRethy/nvim-treesitter-endwise/issues/41
		vim.version.gt(vim.version(), { 0, 10, 2 })
		and "brianhuster/nvim-treesitter-endwise"
		or "RRethy/nvim-treesitter-endwise",
		dependencies = {
			{
				"nvim-treesitter/nvim-treesitter",
				opts = {
					endwise = { enable = true },
				},
			},
		},
	},
	{
		"tpope/vim-endwise",
		enabled = false,
	},
}
