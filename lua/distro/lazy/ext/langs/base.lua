return {
	{ import = "distro.lazy.lang.c" },
	{ import = "distro.lazy.lang.cpp" },
	{ import = "distro.lazy.lang.python" },
	{ import = "distro.lazy.lang.rust" },
	{ import = "distro.lazy.lang.sh" },
}
