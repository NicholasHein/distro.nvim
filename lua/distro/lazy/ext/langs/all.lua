return {
	-- Import all base & default langs
	{ import = "distro.lazy.ext.langs.default" },
	-- Extra langs:
	{ import = "distro.lazy.lang.ansible" },
	{ import = "distro.lazy.lang.awk" },
	{ import = "distro.lazy.lang.devicetree" },
	{ import = "distro.lazy.lang.dot" },
	{ import = "distro.lazy.lang.ghdl" },
	{ import = "distro.lazy.lang.jq" },
	{ import = "distro.lazy.lang.kconfig" },
	{ import = "distro.lazy.lang.mermaid" },
	{ import = "distro.lazy.lang.meson" },
	{ import = "distro.lazy.lang.query" },
	{ import = "distro.lazy.lang.robot" },
	{ import = "distro.lazy.lang.systemd" },
	{ import = "distro.lazy.lang.tex" },
	{ import = "distro.lazy.lang.text" },
	{ import = "distro.lazy.lang.typescript" },
	{ import = "distro.lazy.lang.udev" },
}
