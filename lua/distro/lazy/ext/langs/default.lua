return {
	-- Import all base langs
	{ import = "distro.lazy.ext.langs.base" },
	-- Extra langs:
	{ import = "distro.lazy.lang.bitbake" },
	{ import = "distro.lazy.lang.cmake" },
	{ import = "distro.lazy.lang.css" },
	{ import = "distro.lazy.lang.docker" },
	{ import = "distro.lazy.lang.html" },
	{ import = "distro.lazy.lang.json" },
	{ import = "distro.lazy.lang.lua" },
	{ import = "distro.lazy.lang.make" },
	{ import = "distro.lazy.lang.markdown" },
	{ import = "distro.lazy.lang.org" },
	{ import = "distro.lazy.lang.vim" },
	{ import = "distro.lazy.lang.xml" },
	{ import = "distro.lazy.lang.yaml" },
}
