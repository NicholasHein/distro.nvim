---@enum LualinePathType
local LualinePathType = {
	FILENAME = 0,
	RELATIVE = 1,
	ABSOLUTE = 2,
	ABSOLUTE_TILDE = 3,
	FILENAME_AND_PARENT_TILDE = 4,
}

return {
	{
		"nvim-lualine/lualine.nvim",
		dependencies = { "nvim-tree/nvim-web-devicons" },
		---@class LualineOpts
		opts = {
			extensions = { "fzf", "lazy", "man", "mason", "nvim-dap-ui", "nvim-tree", "toggleterm" },
			options = {
				theme = "horizon",
			},
			-- +-------------------------------------------------+
			-- | A | B | C                             X | Y | Z |
			-- +-------------------------------------------------+
			sections = {
				lualine_c = {
					{
						"filename",
						path = LualinePathType.RELATIVE,
					},
				},
				lualine_x = {
					{
						function()
							local lazy_status = require("lazy.status")
							return lazy_status.updates()
						end,
						cond = function()
							local lazy_status = require("lazy.status")
							return lazy_status.has_updates()
						end,
						color = { fg = "#ff9e64" },
					},
					"encoding",
					"fileformat",
					"filetype",
				},
			},
			inactive_sections = {
				lualine_c = {
					{
						"filename",
						path = LualinePathType.RELATIVE,
					},
				},
			},
		},
	},
	{
		"akinsho/bufferline.nvim",
		version = "*",
		dependencies = "nvim-tree/nvim-web-devicons",
		opts = {
			options = {
				mode = "tabs",
				diagnostics = "nvim_lsp",
				always_show_bufferline = false,
			},
		},
	},
}
