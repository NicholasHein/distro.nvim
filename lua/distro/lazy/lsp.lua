return {
	{
		"neovim/nvim-lspconfig",
		dependencies = {
			{
				"williamboman/mason-lspconfig.nvim",
				dependencies = { "williamboman/mason.nvim" },
				opts = {
					ensure_installed = {},
				},
			},
		},
		---@class LspConfigOptions
		opts = {
			capabilities = {},
			servers = {},
			blacklist = {},
			buffer = {
				keymaps = {},
				handlers = {
					hover = vim.lsp.buf.hover,
					signature_help = vim.lsp.buf.signature_help,
					rename = vim.lsp.buf.rename,
					code_action = vim.lsp.buf.code_action,
					definition = vim.lsp.buf.definition,
					declaration = vim.lsp.buf.declaration,
					implementation = vim.lsp.buf.implementation,
					references = vim.lsp.buf.references,
					document_symbol = vim.lsp.buf.document_symbol,
					workspace_symbol = vim.lsp.buf.workspace_symbol,

					-- Diagnostic
					diagnostic_next = function() vim.diagnostic.goto_next { float = true } end,
					diagnostic_prev = function() vim.diagnostic.goto_prev { float = true } end,
				},
			},
			diagnostic = {
				virtual_text = true,
				signs = { priority = 100, },
				severity_sort = true,
			},
			signs = {
				Error = " ",
				Warn = " ",
				Hint = " ",
				Info = " ",
			}
		},
		---@param opts LspConfigOptions
		config = function(_, opts)
			local lspconfig = require("lspconfig")

			lspconfig.util.default_config = vim.tbl_deep_extend("force", lspconfig.util.default_config, {
				capabilities = opts.capabilities,
			})

			for name, config in pairs(opts.servers) do
				local is_enabled = not vim.list_contains(opts.blacklist, name)
				if is_enabled and config.before_setup then
					is_enabled = config.before_setup()
				end

				if is_enabled then
					lspconfig[name].setup(config)
				end
			end

			local Win = require("lib.ui.win")
			local handler = opts.buffer.handlers
			opts.buffer.keymaps = vim.tbl_deep_extend("keep", opts.buffer.keymaps, {
				-- Info
				["K"] = { function() handler.hover() end, opts = { desc = "lsp: show hover info" } },
				["<C-s>"] = {
					function() handler.signature_help() end,
					mode = "i",
					opts = {
						desc = "lsp: show signature info" }
				},

				-- Actions
				["<M-r>"] = { function() handler.rename() end, opts = { desc = "lsp: rename" } },
				["ga"] = {
					function() handler.code_action() end,
					mode = { "n", "v" },
					opts = {
						desc = "lsp: perform code action" }
				},

				-- Goto
				["gd"] = { function() handler.definition() end, opts = { desc = "lsp: goto definition" } },
				["gD"] = { function() handler.declaration() end, opts = { desc = "lsp: goto declaration" } },
				["gi"] = {
					function() handler.implementation() end,
					opts = {
						desc = "lsp: goto implementation" }
				},
				["gr"] = { function() handler.references() end, opts = { desc = "lsp: goto references" } },
				["god"] = {
					function()
						Win:current():split("horizontal")
						handler.definition()
					end,
					opts = { desc = "lsp: goto definition (split)" }
				},
				["ged"] = {
					function()
						Win:current():split("vertical")
						handler.definition()
					end,
					opts = { desc = "lsp: goto definition (vsplit)" }
				},
				["gtd"] = {
					function()
						Win:current():split("tab")
						handler.definition()
					end,
					opts = { desc = "lsp: goto definition (tab split)" }
				},

				-- Searching
				["gs"] = {
					function() handler.document_symbol() end,
					opts = {
						desc = "lsp: document symbol" }
				},
				["gS"] = {
					function() handler.workspace_symbol() end,
					opts = {
						desc = "lsp: workspace symbol" }
				},

				-- Diagnostics
				["<M-n>"] = {
					function() handler.diagnostic_next() end,
					opts = {
						desc = "diagnostic: goto next" }
				},
				["<M-N>"] = {
					function() handler.diagnostic_prev() end,
					opts = {
						desc = "diagnostic: goto prev" }
				},
			})

			vim.api.nvim_create_autocmd("LspAttach", {
				group = vim.api.nvim_create_augroup("LspBufferMappings", {}),
				desc = "lsp: set up buffer mappings",
				callback = function(ev)
					local mapper = require("lib.keymap")
					mapper.map_local(opts.buffer.keymaps, ev.buf)
				end,
			})

			for suffix, icon in pairs(opts.signs) do
				local name = ("DiagnosticSign%s"):format(suffix)
				vim.fn.sign_define(name, {
					text = icon,
					texthl = name,
					numhl = "",
				})
			end
			vim.diagnostic.config(opts.diagnostic)
		end,
	},
	{
		"hinell/lsp-timeout.nvim",
		dependencies = { "neovim/nvim-lspconfig" },
		enabled = false,
		init = function()
			vim.g.lspTimeoutConfig = {
				filetypes = {
					-- clangd doesn't handle this well...
					-- rust-analyzer also gets screwed up
					ignore = { "c", "cpp", "rust" },
				},
			}
		end,
	},
	{
		"kosayoda/nvim-lightbulb",
		opts = {
			sign = { priority = 5 },
		},
		config = function(_, opts)
			local nvim_lightbulb = require("nvim-lightbulb")
			nvim_lightbulb.setup(opts)

			vim.api.nvim_create_autocmd("LspAttach", {
				group = vim.api.nvim_create_augroup("LightbulbOnAttach", {}),
				callback = function(ev)
					local augroup = vim.api.nvim_create_augroup("NvimLightbulb", {})
					vim.api.nvim_clear_autocmds({ group = augroup, buffer = ev.buf })

					vim.api.nvim_create_autocmd({ "CursorHold", "CursorHoldI" }, {
						group = augroup,
						buffer = ev.buf,
						desc = "nvim-lightbulb: update lightbulb",
						callback = function()
							nvim_lightbulb.update_lightbulb()
						end,
					})
				end,
			})
		end,
	},
	{
		"onsails/lspkind-nvim",
		opts = { mode = "symbol" },
		config = function(_, opts)
			local lspkind = require("lspkind")
			lspkind.init(opts)
		end
	},
	{
		"RRethy/vim-illuminate",
		opts = {
			delay = 500, -- milliseconds
			filetypes_denylist = { "gitcommit" },
		},
		config = function(_, opts)
			local illuminate = require("illuminate")
			illuminate.configure(opts)

			vim.api.nvim_create_autocmd("LspAttach", {
				group = vim.api.nvim_create_augroup("IlluminateOnAttach", {}),
				callback = function(ev)
					local client = vim.lsp.get_client_by_id(ev.data.client_id)
					illuminate.on_attach(client)
				end,
			})
		end
	},
}
