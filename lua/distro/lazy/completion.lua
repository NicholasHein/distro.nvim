---Make a lazy cmp mapping
---@param fn fun(cmp: table, fallback: fun())
---@return fun(fallback: fun())
local function cmp_wrap(fn)
	return function(fallback)
		local cmp = require("cmp")
		fn(cmp, fallback)
	end
end

---Make a lazy cmp mapping for a set of modes
---@param fn fun(cmp: table, fallback: fun())
---@param modes? string[]
---@return table<string, fun(fallback: fun())>
local function cmp_mode_map(fn, modes)
	modes = modes or { "i" }
	local callback = cmp_wrap(fn)

	local mapping = {}
	for _, mode in ipairs(modes) do
		mapping[mode] = callback
	end
	return mapping
end

---@enum cmprank
local CmpRank = {
	low = 1000,
	normal = 100,
	high = 10,
	max = 1,
}

---@class CmpSource: table
---@field name string
---@field group_index? integer

---@class CmpGroup: CmpSource[]

---Set the group index of a group of sources
---@param rank cmprank|integer
---@param group CmpSource[]
---@return CmpGroup
local function cmp_rank_sources(rank, group)
	for _, source in ipairs(group) do
		source.group_index = rank
	end
	return group
end

---Map group indices to groups of sources
---@param mapping table<cmprank|integer, CmpSource[]>
---@return CmpGroup
local function cmp_map_ranks(mapping)
	local all_sources = {}
	for rank, sources in pairs(mapping) do
		vim.list_extend(all_sources, cmp_rank_sources(rank, sources))
	end
	return all_sources
end

return {
	{
		"hrsh7th/nvim-cmp",
		lazy = false,
		dependencies = {
			"onsails/lspkind-nvim",
			"nvim-tree/nvim-web-devicons",
		},
		---@class distro.lazy.completion.NvimCmpOpts
		opts = {
			pumheight = 10,
			cmdline = {},
			filetype = {},
			events = {},
			settings = {
				preselect = "none",
				view = {
					docs = {
						auto_open = true,
					},
				},
				experimental = {
					ghost_text = true,
				},
				formatting = {
					format = function(entry, vim_item)
						local try = require("distro.core.try")
						local lspkind = try.require("lspkind")
						local icons = try.require("nvim-web-devicons")

						vim_item.menu = ("[%s]"):format(entry.source.name)

						if lspkind then
							if icons then
								local icon, hl_group = icons.get_icon(entry
									:get_completion_item()
									.label)
								if icon then
									vim_item.kind = icon
									vim_item.kind_hl_group = hl_group
									return vim_item
								end
							end
							return lspkind.cmp_format {
								mode = "symbol",
								show_labelDetails = true,
							} (entry, vim_item)
						else
							return vim_item
						end
					end
				},
				mapping = {
					["<Esc>"] = cmp_mode_map(function(cmp, fallback)
						if cmp.visible() and cmp.get_active_entry() then
							cmp.abort()
						else
							fallback()
						end
					end, { "i", "s", "c" }),
					["<C-u>"] = cmp_mode_map(function(cmp) cmp.scroll_docs(-4) end),
					["<C-d>"] = cmp_mode_map(function(cmp) cmp.scroll_docs(4) end),
					["<C-Space>"] = cmp_mode_map(function(cmp) cmp.complete() end, { "i", "c" }),
					["<C-c>"] = cmp_mode_map(function(cmp) cmp.abort() end),
					["<C-e>"] = {
						i = cmp_wrap(function(cmp) cmp.abort() end),
						c = cmp_wrap(function(cmp) cmp.close() end),
					},
					["<CR>"] = cmp_mode_map(function(cmp, fallback)
						if cmp.visible() and cmp.get_selected_entry() then
							cmp.confirm({
								behavior = cmp.ConfirmBehavior.Replace,
								select = false,
							})
						else
							fallback()
						end
					end, { "i", "c" }),
					["<Tab>"] = cmp_mode_map(
						function(cmp, fallback)
							if cmp.visible() then
								cmp.select_next_item()
							else
								fallback()
							end
						end, { "i", "s", "c" }),
					["<S-Tab>"] = cmp_mode_map(
						function(cmp, fallback)
							if cmp.visible() then
								cmp.select_prev_item()
							else
								fallback()
							end
						end, { "i", "s", "c" }),
				},
				sources = { --[[NOTE: these are added dynamically]] },
			},
		},
		config = function(_, opts)
			vim.o.pumheight = opts.pumheight

			local cmp = require("cmp")
			cmp.setup(opts.settings)

			for ft, o in pairs(opts.filetype) do
				if o.add_global_sources ~= false then
					vim.list_extend(o.config.sources, opts.settings.sources)
				end
				cmp.setup.filetype(ft, o.config)
			end

			for ctype, o in pairs(opts.cmdline) do
				if o.add_global_sources ~= false then
					vim.list_extend(o.config.sources, opts.settings.sources)
				end
				cmp.setup.cmdline(ctype, o.config)
			end

			for name, list in pairs(opts.events) do
				for _, callback in ipairs(list) do
					cmp.event:on(name, callback)
				end
			end
		end,
	},
	{
		"hrsh7th/cmp-nvim-lsp",
		event = "LspAttach",
		dependencies = {
			"neovim/nvim-lspconfig",
			"hrsh7th/cmp-nvim-lsp-signature-help",
			{
				"hrsh7th/nvim-cmp",
				opts = function(_, opts)
					opts.settings = opts.settings or {}
					opts.settings.sources = opts.settings.sources or {}
					opts.settings.sources = vim.list_extend(opts.settings.sources or {},
						cmp_rank_sources(CmpRank.max, {
							{ name = "nvim_lsp" },
							{ name = "nvim_lsp_signature_help" },
						}))
				end,
			},
		},
		config = function()
			local lspcmp = require("cmp_nvim_lsp")
			local lspconfig = require("lspconfig")
			lspconfig.util.default_config = vim.tbl_deep_extend("force", lspconfig.util.default_config, {
				capabilities = lspcmp.default_capabilities(),
			})
		end,
	},
	{
		"L3MON4D3/LuaSnip",
		event = "InsertEnter",
		version = "v2.*",
		build = "make install_jsregexp",
		dependencies = {
			{
				"rafamadriz/friendly-snippets",
				config = function()
					require("luasnip.loaders.from_vscode").lazy_load {}
				end,
			},
		},
		opts = {
			history = true,
			delete_check_events = "TextChanged",
			ext_opts = {
				["choiceNode"] = {
					active = {
						virt_text = { { "●", "GruvboxOrange" } },
					},
				},
				["insertNode"] = {
					active = {
						virt_text = { { "●", "GruvboxBlue" } },
					},
				},
			},
			snip_env = {
				s = function(...)
					local ls = require("luasnip")

					local snip = ls.s(...)
					table.insert(getfenv(2).ls_file_snippets, snip)
				end,
				parse = function(...)
					local ls = require("luasnip")

					local snip = ls.parser.parse_snippet(...)
					table.insert(getfenv(2).ls_file_snippets, snip)
				end,
			},
		},
		config = function(_, opts)
			local ls = require("luasnip")
			local ls_types = require("luasnip.util.types")
			local ls_from_lua = require("luasnip.loaders.from_lua")

			local ext_opts = {}
			for name, value in pairs(opts.ext_opts) do
				-- Convert stuff like "choiceNode" to ls_types.choiceNode, which is an enum type
				ext_opts[ls_types[name]] = value
			end
			opts.ext_opts = ext_opts

			ls.setup(opts)
			ls_from_lua.load {}
		end,
	},
	{
		"saadparwaiz1/cmp_luasnip",
		event = "InsertEnter",
		dependencies = {
			"L3MON4D3/LuaSnip",
			"hrsh7th/cmp-nvim-lsp",
			{
				"hrsh7th/nvim-cmp",
				opts = function(_, opts)
					opts.settings = opts.settings or {}

					opts.settings.sources = vim.list_extend(opts.settings.sources or {},
						cmp_rank_sources(CmpRank.normal + 1, {
							{ name = "luasnip" }
						}))

					opts.settings.snippet = opts.settings.snippet or {}
					opts.settings.snippet.expand = function(args)
						local luasnip = require("luasnip")
						luasnip.lsp_expand(args.body)
					end


					opts.settings.mapping = vim.tbl_deep_extend("force", opts.settings.mapping or {},
						{
							["<Tab>"] = cmp_mode_map(function(cmp, fallback)
								local luasnip = require("luasnip")
								local Win = require("lib.ui.win")

								if cmp.visible() then
									cmp.select_next_item()
								elseif luasnip.expand_or_locally_jumpable() then
									luasnip.expand_or_jump()
								elseif Win:current():cursor_follows_word() then
									cmp.complete()
								else
									fallback()
								end
							end, { "i", "s" }),
							["<S-Tab>"] = cmp_mode_map(function(cmp, fallback)
								local luasnip = require("luasnip")

								if cmp.visible() then
									cmp.select_prev_item()
								elseif luasnip.jumpable(-1) then
									luasnip.jump(-1)
								else
									fallback()
								end
							end, { "i", "s" }),
						})
				end,
			},
			{
				"neovim/nvim-lspconfig",
				opts = {
					-- Some language servers (vscode-*-language-server) only provides
					-- completion when snippet support is enabled[^1].
					--
					-- [^1]: <https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#jsonls>
					capabilities = {
						textDocument = { completion = { completionItem = { snippetSupport = true } } },
					},
				},
			},
		},
	},
	{
		"hrsh7th/cmp-cmdline",
		dependencies = {
			"dmitmel/cmp-cmdline-history",
			"hrsh7th/cmp-path",
			{
				"hrsh7th/nvim-cmp",
				opts = function(_, opts)
					opts.cmdline = opts.cmdline or {}
					opts.cmdline[":"] = opts.cmdline[":"] or {}
					opts.cmdline[":"].config = opts.cmdline[":"].config or {}
					opts.cmdline[":"].config.sources = vim.list_extend(
						opts.cmdline[":"].config.sources or {},
						cmp_map_ranks({
							[CmpRank.high] = {
								{ name = "cmdline_history", max_item_count = 5 },
								{ name = "cmdline" },
								{ name = "nvim_lua" },
								{ name = "path" },
							},
						}))
				end,
			},
		},
	},
	{
		"hrsh7th/cmp-buffer",
		event = "InsertEnter",
		dependencies = {
			{
				"hrsh7th/nvim-cmp",
				opts = function(_, opts)
					local cl = opts.cmdline or {}
					opts.cmdline = cl

					for _, ctype in ipairs({ "/", "?" }) do
						local o = opts.cmdline[ctype] or {}
						opts.cmdline[ctype] = o

						o.config = o.config or {}
						o.config.sources = vim.list_extend(o.config.sources or {},
							cmp_rank_sources(CmpRank.normal, {
								{ name = "buffer" },
							}))
					end

					opts.settings = opts.settings or {}
					opts.settings.sources = opts.settings.sources or {}
					opts.settings.sources = vim.list_extend(opts.settings.sources or {},
						cmp_rank_sources(CmpRank.low, {
							{ name = "buffer" },
						}))
				end,
			},
		},
	},
	{
		"f3fora/cmp-spell",
		event = "InsertEnter",
		dependencies = {
			{
				"hrsh7th/nvim-cmp",
				opts = function(_, opts)
					opts.settings = opts.settings or {}
					opts.settings.sources = opts.settings.sources or {}
					opts.settings.sources = vim.list_extend(opts.settings.sources or {},
						cmp_rank_sources(CmpRank.low, {
							{ name = "spell" },
						}))
				end,
			},
		},
	},
	{
		"hrsh7th/cmp-path",
		event = { "InsertEnter", "CmdlineEnter" },
		dependencies = {
			{
				"hrsh7th/nvim-cmp",
				opts = function(_, opts)
					opts.settings = opts.settings or {}
					opts.settings.sources = opts.settings.sources or {}
					opts.settings.sources = vim.list_extend(opts.settings.sources or {},
						cmp_rank_sources(CmpRank.low, {
							{ name = "path" },
						}))
				end,
			},
		},
	},
	{
		"hrsh7th/cmp-calc",
		event = "InsertEnter",
		dependencies = {
			{
				"hrsh7th/nvim-cmp",
				opts = function(_, opts)
					opts.settings = opts.settings or {}
					opts.settings.sources = opts.settings.sources or {}
					opts.settings.sources = vim.list_extend(opts.settings.sources or {},
						cmp_rank_sources(CmpRank.low, {
							{ name = "calc" },
						}))
				end,
			},
		},
	},
	{ "rafamadriz/friendly-snippets",   optional = true },
	{ "hrsh7th/cmp-vsnip",              enabled = false },
	{ "quangnguyen30192/cmp-nvim-tags", enabled = false },
}
