return {
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				vimls = require("distro.lsp.server.vimls"):new():config(),
			},
		},
	},
	{
		"williamboman/mason-lspconfig.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "vimls" })
		end
	},
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			require("lib.type.list").join(opts.ensure_installed, { "vim", "vimdoc" })
		end,
	},
}
