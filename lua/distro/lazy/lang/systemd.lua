return {
	{
		"williamboman/mason.nvim",
		---@param opts distro.lazy.MasonOpts
		opts = function(_, opts)
			list.join(opts.ensure_installed, { "systemdlint" })
		end,
	},
}
