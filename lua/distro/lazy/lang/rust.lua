return {
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				rust_analyzer = require("distro.lsp.server.rust_analyzer"):new():config(),
			},
		},
	},
	{
		"williamboman/mason-lspconfig.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "rust_analyzer" })
		end
	},
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			require("lib.type.list").join(opts.ensure_installed, { "rust" })
		end,
	},
	{
		"saecki/crates.nvim",
		tag = "stable",
		dependencies = {
			{
				"hrsh7th/nvim-cmp",
				opts = function(_, opts)
					local o = opts.filetype.toml or {}
					opts.filetype.toml = o
					o.config = o.config or {}
					o.config.sources = o.config.sources or {}
					table.insert(o.config.sources, { name = "crates", group_index = 10 })
					o.add_global_sources = true
				end,
			},
		},
		config = true,
	},
	{
		"mrcjkb/rustaceanvim",
		dependencies = {
			{
				"neovim/nvim-lspconfig",
				opts = function(_, opts)
					-- Prevent conflicts with rustaceanvim
					opts.servers.rust_analyzer = nil
				end,
			},
		},
		version = "*",
		lazy = false,
	},
}
