return {
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				jqls = require("distro.lsp.server.jqls"):new():config(),
			},
		},
	},
	{
		"williamboman/mason-lspconfig.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "jqls" })
		end
	},
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "jq" })
		end,
	},
}
