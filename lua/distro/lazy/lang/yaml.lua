return {
	{
		"neovim/nvim-lspconfig",
		dependencies = {
			"b0o/schemastore.nvim",
		},
		opts = {
			servers = {
				yamlls = require("distro.lsp.server.yamlls"):new():config(),
			},
		},
	},
	{
		"someone-stole-my-name/yaml-companion.nvim",
		dependencies = {
			"neovim/nvim-lspconfig",
			"nvim-lua/plenary.nvim",
			"nvim-telescope/telescope.nvim",
			{
				"nvim-lualine/lualine.nvim",
				optional = true,
				---@param opts LualineOpts
				opts = function(_, opts)
					local llx = opts.sections.lualine_x
					llx[#llx + 1] = {
						function()
							local schema = require("yaml-companion").get_buf_schema(0)
							if schema.result[1].name == "none" then
								return ""
							end
							return schema.result[1].name
						end,
						cond = function()
							local schema = require("yaml-companion").get_buf_schema(0)
							return schema.result[1].name ~= "none"
						end,
					}
				end,
			},
		},
		opts = {
			schemas = {
				{
					name = "dt-meta-schema-base",
					uri =
					"https://raw.githubusercontent.com/devicetree-org/dt-schema/refs/heads/main/dtschema/meta-schemas/base.yaml",
				},
			},
		},
		config = function(_, opts)
			require("yaml-companion").setup(opts)
			require("telescope").load_extension("yaml_schema")
		end,
	},
	{
		"williamboman/mason-lspconfig.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "yamlls" })
		end
	},
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			require("lib.type.list").join(opts.ensure_installed, { "yaml" })
		end,
	},
}
