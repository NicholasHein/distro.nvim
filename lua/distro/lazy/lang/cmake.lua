return {
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				cmake = require("distro.lsp.server.cmake"):new():config(),
			},
		},
	},
	{
		"williamboman/mason-lspconfig.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "cmake" })
		end
	},
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			require("lib.type.list").join(opts.ensure_installed, { "cmake" })
		end,
	},
}
