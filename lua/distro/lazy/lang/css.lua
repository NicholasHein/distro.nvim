return {
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				cssls = require("distro.lsp.server.cssls"):new():config(),
			},
		},
	},
	{
		"williamboman/mason-lspconfig.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "cssls" })
		end
	},
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			require("lib.type.list").join(opts.ensure_installed, { "css" })
		end,
	},
}
