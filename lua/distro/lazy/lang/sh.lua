return {
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				bashls = require("distro.lsp.server.bashls"):new():config(),
			},
		},
	},
	{
		"williamboman/mason-lspconfig.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "bashls" })
		end
	},
	{
		"williamboman/mason.nvim",
		---@param opts distro.lazy.MasonOpts
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "shellcheck" })
		end,
	},
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			require("lib.type.list").join(opts.ensure_installed, { "bash", "fish" })
		end,
	},
}
