return {
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				ts_ls = require("distro.lsp.server.ts_ls"):new():config(),
			},
		},
	},
	{
		"williamboman/mason-lspconfig.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "ts_ls" })
		end
	},
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			require("lib.type.list").join(opts.ensure_installed, { "typescript", "javascript" })
		end,
	},
}
