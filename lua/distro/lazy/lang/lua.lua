return {
	{
		"windwp/nvim-autopairs",
		opts = {
			ts_config = {
				lua = { "string" },
			},
		},
	},
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				lua_ls = require("distro.lsp.server.lua_ls"):new():config(),
			},
		},
	},
	{
		"folke/lazydev.nvim",
		ft = "lua",
		dependencies = {
			{ "Bilal2453/luvit-meta", lazy = true },
			{
				"hrsh7th/nvim-cmp",
				---@param opts distro.lazy.completion.NvimCmpOpts
				opts = function(_, opts)
					local ft = opts.filetype
					ft.lua = ft.lua or { config = { sources = {} } }
					vim.list_extend(ft.lua.config.sources, {
						name = "lazydev",
						group_index = 0,
					})
				end,
			},
		},
		opts = {
			library = {
				{ path = "luvit-meta/library", words = { "vim%.uv" } },
				"distro.nvim",
			},
		}
	},
	{
		"williamboman/mason-lspconfig.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "lua_ls" })
		end
	},
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			require("lib.type.list").join(opts.ensure_installed, { "lua", "luadoc" })
		end,
	},
}
