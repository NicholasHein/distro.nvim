return {
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				dockerls = require("distro.lsp.server.dockerls"):new():config(),
			},
		},
	},
	{
		"williamboman/mason-lspconfig.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "dockerls" })
		end
	},
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			require("lib.type.list").join(opts.ensure_installed, { "dockerfile" })
		end,
	},
}
