return {
	{
		"kdheepak/cmp-latex-symbols",
		ft = "tex",
		dependencies = {
			{
				"hrsh7th/nvim-cmp",
				opts = function(_, opts)
					local o = opts.filetype.tex or {}
					opts.filetype.tex = o
					o.config = o.config or {}
					o.config.sources = o.config.sources or {}
					table.insert(o.config.sources, { name = "latex_symbols", group_index = 10 })
					o.add_global_sources = true
				end
			}
		},
	},
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				texlab = require("distro.lsp.server.texlab"):new():config(),
			},
		},
	},
	{
		"williamboman/mason-lspconfig.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "texlab" })
		end
	},
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			require("lib.type.list").join(opts.ensure_installed, { "latex", "bibtex" })
		end,
	},
}
