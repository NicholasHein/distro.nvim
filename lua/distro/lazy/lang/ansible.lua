return {
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				ansiblels = require("distro.lsp.server.ansiblels"):new():config(),
			},
		},
	},
	{
		"williamboman/mason-lspconfig.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "ansiblels" })
		end
	},
	{
		"williamboman/mason.nvim",
		---@param opts distro.lazy.MasonOpts
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "ansible-lint" })
		end,
	},
}
