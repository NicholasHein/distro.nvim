local list = require("lib.type.list")

return {
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				clangd = require("distro.lsp.server.clangd"):new():config(),
			},
		},
	},
	{
		"williamboman/mason-lspconfig.nvim",
		opts = function(_, opts)
			list.join(opts.ensure_installed, { "clangd" })
		end
	},
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			require("lib.type.list").join(opts.ensure_installed, { "c", "cpp", "doxygen" })
		end,
	},
	{
		"jay-babu/mason-nvim-dap.nvim",
		opts = function(_, opts)
			list.join(opts.ensure_installed, { "cppdbg", "codelldb" })
		end
	},
}
