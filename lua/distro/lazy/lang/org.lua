local list = require("lib.type.list")

return {
	{
		"nvim-orgmode/orgmode",
		dependencies = {
			{
				"hrsh7th/nvim-cmp",
				optional = true,
				opts = function(_, opts)
					local o = opts.filetype.org or {}
					opts.filetype.org = o
					o.config = o.config or {}
					o.config.sources = o.config.sources or {}
					table.insert(o.config.sources, { name = "orgmode" })
					o.add_global_sources = true
				end,
			},
		},
		event = "VeryLazy",
		config = true,
	},
	{
		"joaomsa/telescope-orgmode.nvim",
		config = function()
			require("telescope").load_extension("orgmode")
		end,
		keys = {
			{
				[[?o]],
				function() require("telescope").extensions.orgmode.refile_headings() end,
				desc = "telescope: file headings",
			},
			{
				[[?O]],
				function() require("telescope").extensions.orgmode.search_headings() end,
				desc = "telescope: all headings",
			},
		},
	},
	{
		"akinsho/org-bullets.nvim",
		config = true,
	},
	{
		"lukas-reineke/headlines.nvim",
		dependencies = { "nvim-treesitter/nvim-treesitter" },
		---@class HeadlinesOptions
		opts = {
			-- Default value for fat_headline_lower_string doesn't render well
			markdown = { fat_headline_lower_string = "═" },
			rmd = { fat_headline_lower_string = "═" },
			norg = { fat_headline_lower_string = "═" },
			org = { fat_headline_lower_string = "═" },
		},
	},
	{
		"michaelb/sniprun",
		branch = "master",
		build = "sh install.sh",
		config = true,
	},
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				ltex = require("distro.lsp.server.ltex"):new():config(),
			},
		},
	},
	{
		"williamboman/mason-lspconfig.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "ltex" })
		end
	},
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			list.join(opts.highlight.additional_vim_regex_highlighting, { "org" })
			list.join(opts.ensure_installed, { "org" })
		end
	},
}
