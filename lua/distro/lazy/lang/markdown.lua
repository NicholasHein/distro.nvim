return {
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				dprint = require("distro.lsp.server.dprint"):new():config(),
				marksman = require("distro.lsp.server.marksman"):new():config(),
				vale_ls = require("distro.lsp.server.vale_ls"):new():config(),
			},
		},
	},
	{
		"williamboman/mason-lspconfig.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, {
				"dprint",
				"marksman",
				"vale_ls",
			})
		end
	},
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			local list = require("lib.type.list")
			list.join(opts.ensure_installed, { "markdown", "markdown_inline", "rst" })
			list.join(opts.highlight.additional_vim_regex_highlighting, { "markdown" })
		end
	},
}
