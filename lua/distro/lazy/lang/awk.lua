return {
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			local list = require("lib.type.list")
			list.join(opts.ensure_installed, { "awk" })
		end
	},
}
