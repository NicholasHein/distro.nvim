return {
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				ghdl_ls = require("distro.lsp.server.ghdl_ls"):new():config(),
			},
		},
	},
	{
		"williamboman/mason-lspconfig.nvim",
		opts = function(_, opts)
			--vim.list_extend(opts.ensure_installed, { "ghdl_ls" })
			-- FIXME: this may need to be added manually[^1]
			--
			-- [^1]: <https://github.com/williamboman/mason.nvim/tree/main/tests/helpers/lua/dummy-registry>
		end
	},
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			require("lib.type.list").join(opts.ensure_installed, { "verilog" })
		end,
	},
}
