return {
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				swift_mesonls = require("distro.lsp.server.swift_mesonls"):new():config(),
				mesonlsp = require("distro.lsp.server.mesonlsp"):new():config(),
			},
		},
	},
	{
		"williamboman/mason-lspconfig.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "mesonlsp" })
		end
	},
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			require("lib.type.list").join(opts.ensure_installed, { "meson" })
		end,
	},
}
