return {
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				dotls = require("distro.lsp.server.dotls"):new():config(),
			},
		},
	},
	{
		"williamboman/mason-lspconfig.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "dotls" })
		end
	},
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			require("lib.type.list").join(opts.ensure_installed, { "dot" })
		end,
	},
}
