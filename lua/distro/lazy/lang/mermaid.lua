return {
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			require("lib.type.list").join(opts.ensure_installed, { "mermaid" })
			--vim.list_extend(opts.highlight.disable, { "mermaid" }) -- Can't handle "graph" types
		end,
	},
}
