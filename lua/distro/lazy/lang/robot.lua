return {
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				robotframework_ls = require("distro.lsp.server.robotframework_ls"):new():config(),
			},
		},
	},
	{
		"williamboman/mason-lspconfig.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "robotframework_ls" })
		end
	},
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			require("lib.type.list").join(opts.ensure_installed, { "robot" })
		end,
	},
}
