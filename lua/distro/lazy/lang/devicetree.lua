return {
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				ginko_ls = require("distro.lsp.server.ginko_ls"):new():config(),
			},
		},
	},
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			local list = require("lib.type.list")
			list.join(opts.ensure_installed, { "devicetree" })
		end
	},
	{
		-- FIXME: this package is not yet supported by mason-lspconfig
		"williamboman/mason.nvim",
		---@param opts distro.lazy.MasonOpts
		opts = function(_, opts)
			list.join(opts.ensure_installed, { "ginko_ls" })
		end
	},
}
