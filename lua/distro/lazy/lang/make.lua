return {
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			local list = require("lib.type.list")
			list.join(opts.highlight.additional_vim_regex_highlighting, { "make" })
			list.join(opts.ensure_installed, { "make", "meson", "ninja" })
		end
	},
}
