return {
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				html = require("distro.lsp.server.html"):new():config(),
			},
		},
	},
	{
		"williamboman/mason-lspconfig.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "html" })
		end
	},
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			require("lib.type.list").join(opts.ensure_installed, { "html" })
		end,
	},
}
