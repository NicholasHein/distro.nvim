return {
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				typos_lsp = require("distro.lsp.server.typos_lsp"):new():config(),
				vale_ls = require("distro.lsp.server.vale_ls"):new():config(),
			},
		},
	},
	{
		"williamboman/mason-lspconfig.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "typos_lsp", "vale_ls" })
		end
	},
}
