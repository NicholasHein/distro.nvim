return {
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				lemminx = require("distro.lsp.server.lemminx"):new():config(),
			},
		},
	},
	{
		"williamboman/mason-lspconfig.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "lemminx" })
		end
	},
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			require("lib.type.list").join(opts.ensure_installed, { "xml", "dtd" })
		end,
	},
}
