return {
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				bitbake_ls = require("distro.lsp.server.bitbake_ls"):new():config(),
			},
		},
	},
	{
		"williamboman/mason.nvim",
		---@param opts distro.lazy.MasonOpts
		opts = function(_, opts)
			local distro_config = require("distro.config")
			if distro_config.registries.mason then
				vim.list_extend(opts.ensure_installed, { "language-server-bitbake" })
			end

			-- Required for building Latex parser
			vim.list_extend(opts.ensure_installed, { "tree-sitter-cli" })
		end,
	},
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, {
				"bitbake",
				"latex", -- Required for documentation highlighting
			})
		end,
	},
	{
		"kergoth/vim-bitbake",
		enabled = false,
		ft = "bitbake",
	},
}
