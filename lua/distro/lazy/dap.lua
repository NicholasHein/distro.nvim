local mapper = require("lib.keymap")
local dap_keymap_cache = {}

local function dap_apply_keymaps(new_maps)
	dap_keymap_cache = mapper.mask_buf_mappings(new_maps)
end

local function dap_restore_keymaps(new_maps)
	for lhs, opts in pairs(new_maps) do
		mapper.del(lhs, { mode = opts.mode })
	end

	mapper.restore(dap_keymap_cache)
	dap_keymap_cache = {}
end

return {
	{
		"mfussenegger/nvim-dap",
		keys = {
			{ [[<F5>]],      function() require("dap").continue() end,          desc = "dap: start/continue" },
			{ [[<F6>]],      function() require("dap").run_last() end,          desc = "dap: re-run last adapter/config" },
			{ [[<leader>b]], function() require("dap").toggle_breakpoint() end, desc = "dap: start/continue" },
		},
		opts = {
			defaults = {
				fallback = {
					external_terminal = { command = "alacritty", args = { "-e" } },
				},
			},
			signs = {
				DapStopped = { text = '➡', texthl = 'DapStopped', linehl = '', numhl = '' },
				DapBreakpoint = { text = '⦿', texthl = 'DapBreakpoint', linehl = '', numhl = '' },
				DapLogPoint = { text = '⦾', texthl = 'DapBreakpoint', linehl = '', numhl = '' },
				DapBreakpointRejected = { text = '⦿', texthl = 'DapBreakpointRejected', linehl = '', numhl = '' },
			},
			ext = {
				vscode = {
					launch_js = {
						enable = true,
						-- path = ".vscode/launch.json",
						type_to_filetypes = nil,
					},
				},
			},
			keymaps = {
				["<F10>"] = { function() require("dap").step_over() end, opts = { desc = "dap: step over" } },
				["<F11>"] = { function() require("dap").step_into() end, opts = { desc = "dap: step into" } },
				["<F12>"] = { function() require("dap").step_out() end, opts = { desc = "dap: step out" } },
				["<leader>k"] = { function() require("dap.ui.widgets").hover() end, opts = { desc = "dap: hover" } },
				["<leader>dp"] = { function() require("dap.ui.widgets").preview() end, opts = { desc = "dap: preview" } },
				["<leader>df"] = {
					function()
						local widgets = require("dap.ui.widgets")
						widgets.centered_float(widgets.frames)
					end,
					opts = { desc = "dap: show frames" },
				},
				["<leader>ds"] = {
					function()
						local widgets = require("dap.ui.widgets")
						widgets.centered_float(widgets.scopes)
					end,
					opts = { desc = "dap: show scopes" },
				},
			},
			buffer_types = {
				["dap-repl"] = {
					auto_completion = function(ev)
						local autocompl = require("dap.ext.autocompl")
						autocompl.attach(ev.buf)
					end,
				},
			},
			listeners = {
				after = {
					event_initialized = {
						keymaps = function(opts)
							dap_apply_keymaps(opts.keymaps)
						end,
					},
					event_terminated = {
						keymaps = function(opts)
							dap_restore_keymaps(opts.keymaps)
						end,
					},
				},
				before = {},
			},
		},
		config = function(_, opts)
			local dap = require("dap")

			-- Fallback configs
			for name, value in pairs(opts.defaults.fallback) do
				dap.defaults.fallback[name] = value
			end

			-- Signs
			for name, sign in ipairs(opts.signs) do
				vim.fn.sign_define(name, sign)
			end

			-- Buffer type autocmds
			local aug = vim.api.nvim_create_augroup("DapBufferTypes", {})
			for filetype, callbacks in pairs(opts.buffer_types) do
				vim.api.nvim_create_autocmd("FileType", {
					pattern = filetype,
					group = aug,
					desc = "dap: setup buffer",
					callback = function(ev)
						for _, fn in pairs(callbacks) do
							fn(ev)
						end
					end
				})
			end

			-- Listeners
			for _, evtype in ipairs({ "after", "before" }) do
				for evname, evhandlers in pairs(opts.listeners[evtype]) do
					for key, callback in pairs(evhandlers) do
						dap.listeners[evtype][evname][key] = function(...) callback(opts, ...) end
					end
				end
			end

			-- Launch JSON
			local launchjs_opts = opts.ext.vscode.launch_js
			if launchjs_opts.enable then
				local dap_vscode = require("dap.ext.vscode")
				dap_vscode.load_launchjs(launchjs_opts.path, launchjs_opts.type_to_filetypes)
			end
		end,
	},
	{
		"rcarriga/nvim-dap-ui",
		dependencies = {
			{
				"mfussenegger/nvim-dap",
				opts = {
					keymaps = {
						["<leader>K"] = {
							function()
								---@diagnostic disable-next-line: missing-parameter
								require("dapui").eval()
							end,
							mode = { "n", "v" },
							opts = { desc = "dap: eval" },
						},
					},
					listeners = {
						after = {
							event_initialized = {
								dapui_toggle = function()
									require("dapui").open()
								end
							},
						},
						before = {
							event_terminated = {
								dapui_toggle = function()
									require("dapui").close()
								end
							},
							event_exited = {
								dapui_toggle = function()
									require("dapui").close()
								end
							},
						},
					},
				},
			},
			{ "nvim-neotest/nvim-nio" },
		},
		opts = {
			icons = { expanded = "▾", collapsed = "▸" },
		},
	},
	{
		"jay-babu/mason-nvim-dap.nvim",
		dependencies = { "williamboman/mason.nvim" },
		opts = {
			ensure_installed = {},
			handlers = {
				function(config)
					local mason = require("mason-nvim-dap")
					mason.default_setup(config)
				end
			},
		},
	},
	{
		"theHamsta/nvim-dap-virtual-text",
		lazy = true,
		dependencies = { "nvim-treesitter/nvim-treesitter" },
		opts = {},
	},
	{
		"LiadOz/nvim-dap-repl-highlights",
		lazy = true,
		dependencies = { "nvim-treesitter/nvim-treesitter" },
		opts = {},
	},
	{
		"nvim-telescope/telescope-dap.nvim",
		lazy = true,
		dependencies = { "nvim-telescope/telescope.nvim" },
		config = function()
			local telescope = require("telescope")
			telescope.load_extension("dap")
		end,
	},
}
