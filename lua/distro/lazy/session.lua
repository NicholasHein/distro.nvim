return {
	{
		"dstein64/vim-startuptime",
		cmd = "StartupTime",
		opts = {
			startuptime_tries = 10,
		},
		config = function(_, opts)
			vim.g.startuptime_tries = opts.startuptime_tries
		end,
	},
	{
		"folke/persistence.nvim",
		event = "BufReadPre",
		opts = {
			options = vim.opt.sessionoptions:get(),
		},
		keys = {
			{ [[<leader>qr]], function() require("persistence").load() end, desc = "persistence: restore a session" },
			{ [[<leader>ql]], function() require("persistence").load { last = true } end,
				desc = "persistence: restore last session" },
			{ [[<leader>qc]], function() require("persistence").stop() end, desc = "persistence: cancel the current session" },
		},
	},
}
