return {
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			local list = require("lib.type.list")
			opts.ensure_installed = list.join(opts.ensure_installed or {}, {
				"git_config",
				"git_rebase",
				"gitattributes",
				"gitcommit",
				"gitignore",
			})
		end
	},
	{
		"NeogitOrg/neogit",
		lazy = false,
		dependencies = {
			"nvim-lua/plenary.nvim",
			"nvim-telescope/telescope.nvim",
			{ "sindrets/diffview.nvim", optional = true }, -- Diff integration
		},
		---@class NeogitOptions
		opts = {
			kind = "split", -- NOTE: "floating" is experimental
			graph = "unicode",
			commit_editor = { kind = "split" },
			mappings = {
				popup = {
					-- Use <l> for traversal
					["l"] = false,
					["L"] = "LogPopup",
					-- Prevent accidental confusion between <p> (pull) and <P> (push)
					["p"] = false,
				},
				status = {
					["e"] = "VSplitOpen",
					["o"] = "SplitOpen",
					-- HACK: Due to how neogit is written[^1], all actions must have key bindings, otherwise you get errors like
					--
					--     E5108: Error executing lua: .../neogit/lua/neogit/buffers/status/init.lua:137: table index is nil
					--
					-- So, bind unused actions to random unused keys
					--
					-- [^1]: https://github.com/NeogitOrg/neogit/blob/11dea1b67bb153cd5b4a2de348ceefa69508dfce/lua/neogit/buffers/status/init.lua#L137
					["%"] = "OpenTree",
				},
			},
		},
		keys = {
			{ [[<C-g>]], function() require("neogit").open() end },
		},
		cmd = { "Neogit" },
	},
	{
		"sindrets/diffview.nvim",
		lazy = false,
		opts = {
			enhanced_diff_hl = true,
		},
		cmd = {
			"DiffviewOpen",
			"DiffviewClose",
			"DiffviewRefresh",
			"DiffviewLog",
			"DiffviewToggleFiles",
			"DiffviewFocusFiles",
			"DiffviewFileHistory",
		},
		keys = {
			{ [[<leader>do]], "<cmd>DiffviewOpen<cr>" },
		},
	},
	{
		"akinsho/git-conflict.nvim",
		version = "*",
		config = true,
	},
	{
		"lewis6991/gitsigns.nvim",
		opts = {
			current_line_blame = true,
			on_attach = function(bufnr)
				local gs = require("gitsigns")
				local set = vim.keymap.set

				local function range()
					return { vim.fn.line("."), vim.fn.line("v") }
				end

				-- Actions
				set("n", [[<leader>gs]], gs.stage_buffer,
					{ desc = "gitsigns: stage buffer", buffer = bufnr })
				set("n", [[<leader>gR]], gs.reset_buffer,
					{ desc = "gitsigns: reset buffer", buffer = bufnr })
				set("n", [[<leader>gd]], gs.diffthis, { desc = "gitsigns: diff this", buffer = bufnr })
				set("n", [[<leader>gD]], function() gs.diffthis("~") end,
					{ desc = "gitsigns: diff all", buffer = bufnr })

				set("n", [[<leader>gb]], function()
					gs.blame_line { full = true }
				end, { desc = "gitsigns: full blame", buffer = bufnr })

				set("n", [[<leader>Gs]], gs.stage_hunk, { desc = "gitsigns: stage hunk", buffer = bufnr })
				set("n", [[<leader>Gr]], gs.undo_stage_hunk,
					{ desc = "gitsigns: restore hunk", buffer = bufnr })
				set("n", [[<leader>GR]], gs.reset_hunk, { desc = "gitsigns: reset hunk", buffer = bufnr })

				set("v", [[<leader>Gs]], function() gs.stage_hunk(range()) end,
					{ desc = "gitsigns: stage range", buffer = bufnr })
				set("v", [[<leader>GR]], function() gs.reset_hunk(range()) end,
					{ desc = "gitsigns: reset range", buffer = bufnr })

				-- Navigation
				local function nav(lhs, fn, desc)
					set("n", lhs, function()
						if vim.wo.diff then return lhs end
						vim.schedule(fn)
						return [[<Ignore>]]
					end, { desc = desc, buffer = bufnr, expr = true })
				end

				nav([[<leader>gn]], gs.next_hunk, "gitsigns: next hunk")
				nav([[<leader>gN]], gs.prev_hunk, "gitsigns: next hunk")

				-- Text object
				set({ "o", "x" }, [[<leader>gg]], [[:<C-U>Gitsigns select_hunk<CR>]],
					{ desc = "gitsigns: select hunk", buffer = bufnr })
			end
		},
	},
	{
		"tpope/vim-fugitive",
		version = "*",
		cmd = "Git",
		keys = {
			{ [[<leader>gB]], "<cmd>Git blame<cr>" },
		},
	},
	{
		"tanvirtin/vgit.nvim",
		enabled = false,
		requires = { "nvim-lua/plenary.nvim" },
		commit =
		    vim.version.lt(vim.version(), { 0, 8 }) and "ee9081c304b44509b2f4267f1f7addc303f9fb9b" or nil,
		opts = {},
		keys = {
			{
				"<leader>b",
				function() require("vgit").buffer_blame_preview() end,
				desc = "vgit: buffer blame preview"
			},
		},
	},
}
