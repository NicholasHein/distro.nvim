return {
	{
		"folke/tokyonight.nvim",
		lazy = true,
		priority = 1000,
		branch = "main",
		opts = {
			style = "storm",
			transparent = true,
		},
	},
	{
		"EdenEast/nightfox.nvim",
		lazy = true,
		priority = 1000,
		dependencies = {
			{
				"rcarriga/nvim-notify",
				optional = true,
				-- Work-around for <https://github.com/rcarriga/nvim-notify/issues/159>
				opts = { background_colour = "#000000" },
			},
		},
		opts = {
			options = {
				transparent = false,
				dim_inactive = true,
				module_default = false,
				modules = {
					cmp = true,
					illuminate = true,
					telescope = true,
					treesitter = true,
					diagnostic = { enable = true, background = true, },
					native_lsp = { enable = true, background = true, },
				},
			},
		},
	},
	{
		"svrana/neosolarized.nvim", -- Alternatively: use "overcache/NeoSolarized"
		lazy = true,
		priority = 1000,
		dependencies = { "tjdevries/colorbuddy.nvim" },
		cond = function()
			return vim.fn.has("nvim-0.8.0") ~= 0
		end,
		opts = {
			comment_italics = true,
			background_set = false,
		},
	},
	{
		"nanotech/jellybeans.vim",
		lazy = true,
		priority = 1000,
		config = function()
			local overrides = {
				background = {
					ctermbg = "none",
					["256ctermbg"] = "none",
				},
			}

			if vim.fn.has("termguicolors") and vim.api.nvim_get_option("termguicolors") then
				overrides.background["guibg"] = "none"
			end
			vim.api.nvim_set_var("jellybeans_overrides", overrides)
		end,
	},
}
