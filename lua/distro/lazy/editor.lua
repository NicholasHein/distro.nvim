return {
	{
		"cappyzawa/trim.nvim",
		opts = {
			ft_blocklist = { "mail" },
		},
		event = { "BufWritePre" },
		cmd = { "Trim", "TrimToggle" },
	},
	{
		"numToStr/Comment.nvim",
		lazy = false,
		dependencies = {
			{
				"JoosepAlviste/nvim-ts-context-commentstring",
				opts = { enable_autocmd = false },
			},
		},
		opts = {
			pre_hook = function(ctx)
				local tscms_integ = require("ts_context_commentstring.integrations.comment_nvim")

				local pre_hook = tscms_integ.create_pre_hook()
				return pre_hook(ctx)
			end,
		},
	},
	{
		"lukas-reineke/indent-blankline.nvim",
		main = "ibl",
		---@class IndentBlankLineOpts
		opts = {
			indent = {
				char = [[▎]],
				tab_char = [[▎]],
			},
			scope = {
				show_start = false,
				show_end = false,
			},
		},
		config = function(_, opts)
			local ibl = require("ibl")
			ibl.setup(opts)

			local lcs = vim.opt.listchars:get()
			lcs.tab = [[  ]]
			lcs.leadmultispace = [[ ]]
			vim.opt.listchars = lcs

			-- local hooks = require("ibl.hooks")
			-- local hook_id = hooks.register(
			-- 	hooks.type.ACTIVE,
			-- 	function(bufnr)
			--
			-- 	end
			-- )
		end,
	},
	{
		"folke/todo-comments.nvim",
		cmd = { "TodoTrouble", "TodoTelescope" },
		config = true,
	},
	{
		"bloznelis/before.nvim",
		optional = true,
		lazy = true,
		config = true,
		keys = {
			{ [[<M-b>]], function() require("before").jump_to_last_edit() end, desc = "before: previous edit" },
			{ [[<M-B>]], function() require("before").jump_to_next_edit() end, desc = "before: next edit" },
		},
	},
	{
		"tpope/vim-commentary",
		enabled = false,
	},
	{
		-- Source: <https://www.vim.org/scripts/script.php?script_id=987>
		-- Mirror: <https://github.com/vim-scripts/DoxygenToolkit.vim>
		-- Forked to add oneLineLicense option
		url = "https://gitlab.com/NicholasHein/doxygentoolkit.vim.git",
		branch = "patched",
		enabled = false,
		ft = { "c", "cpp" },
		init = function()
			local sys = require("lib.sys")

			local full_name = sys.user_get_full_name()
			if not full_name then
				vim.notify("doxygentoolkit.vim: failed to get full name for user", vim.log.levels.DEBUG)
				full_name = vim.env.USER or "???"
			end

			-- See `:h doxygen-syntax`
			vim.g.load_doxygen_syntax = 1

			vim.g.DoxygenToolkit_briefTag_pre = ""
			vim.g.DoxygenToolkit_authorName = full_name
			vim.g.DoxygenToolkit_licenseTag = ("Copyright %u %s"):format(sys.datetime().year, full_name)
			vim.g.DoxygenToolkit_compactDoc = "yes"
			vim.g.DoxygenToolkit_oneLineLicense = 0
		end,
	},
}
