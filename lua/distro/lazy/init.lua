-- Core plugin specs and setup
-- NOTE: init.lua is used so it is merged in the main plugin spec like the other top-level modules

-- See <https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/plugins/init.lua>

local aug = vim.api.nvim_create_augroup("DistroLazy", { clear = true })

local config = require("distro.config")
config.init({
	hook = {
		lazy_call = function(fn)
			vim.api.nvim_create_autocmd("User", {
				group = aug,
				pattern = "VeryLazy",
				callback = function() fn() end,
			})
		end,
	},
})

local spec = require("lazy.core.config").spec.plugins.distro
if spec then
	vim.opt.rtp:append(spec.dir)
end

return {
	{ "folke/lazy.nvim", version = "*" },
	{
		"distro.nvim",
		priority = 10000,
		lazy = false,
		config = true,
		cond = true
		--version = "*",
	},
	{
		"williamboman/mason.nvim",
		lazy = true,
		cmd = "Mason",
		build = { ":MasonUpdate" },
		---@class distro.lazy.MasonOpts: MasonSettings
		---@field ensure_installed? string[]
		opts = {
			ensure_installed = {},
		},
		config = function(_, opts)
			local mason = require("mason")
			local mr = require("mason-registry")
			local distro_config = require("distro.config")

			mason.setup(opts)

			if distro_config.registries.mason then
				local lazy = require("lazy")
				---@type LazyPlugin
				local distro_plugin = assert((vim.tbl_filter(function(plugin)
					return plugin.name == "distro.nvim"
				end, lazy.plugins()) or {})[1])

				-- yq is required for parsing yaml files in "file" registries
				local yq = mr.get_package("yq")
				if not yq:is_installed() then
					yq:install()
				end

				opts.registries = vim.list_extend({
					("file:%s"):format(distro_plugin.dir),
					"lua:distro-mason.registry",
				}, opts.registries or { "github:mason-org/mason-registry" })

				-- Setup must be called again to add the new registries
				mason.setup(opts)
			end

			-- Stolen (with love) from LazyVim
			mr:on("package:install:success", function()
				vim.defer_fn(function()
					local lazy_event = require("lazy.core.handler.event")
					lazy_event.trigger({
						event = "FileType",
						buf = vim.api.nvim_get_current_buf(),
					})
				end, 100)
			end)

			local function ensure_installed()
				for _, tool in ipairs(opts.ensure_installed or {}) do
					local pkg = mr.get_package(tool)
					if not pkg:is_installed() then
						pkg:install()
					end
				end
			end

			mr.refresh(ensure_installed)
		end,
	},
}
