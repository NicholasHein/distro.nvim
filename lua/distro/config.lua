local Util = require("lib.core")

local M = {}
M.log = require("lib.log.builtin"):new({ name = "distro", prefix = "distro" })

local config
---@class DistroConfig
local config_defaults = {
	---@type string|fun()
	colorscheme = function()
		require("tokyonight").load()
	end,
	fallback = {
		colorscheme = "habamax",
	},
	experimental = {
		fancy_intro = false,
	},
	extras = {
		scratch_buffer_commands = false,
		split_terminal = false,
	},
	overrides = {
		["autocmds"] = false,
		["keymaps"] = false,
		["options"] = false,
	},
	registries = {
		mason = false,
	},
}

local core
---@class DistroCoreConfig
local core_defaults = {
	hook = {},
	modules = {
		base = "distro.config",
		user = "config",
		pre_config = {
			"options",
		},
		config = {
			{ module = "autocmds", cond = function(opts) return opts.lazy and vim.fn.argc(-1) == 0 end }
		},
		post_config = {
			-- Lazy-load autocmds when not opening a file
			{ module = "autocmds", cond = function(opts) return not opts.lazy or vim.fn.argc(-1) ~= 0 end },
			"keymaps",
		},
	},
}

---Load a single module
---@param name string
---@return table?
local function load_module(name)
	M.log:debug("loading %s", name)
	return Util.try(function()
		return require(name)
	end, { msg = ("Failed to load %s"):format(name) })
end

---Load a module with a user extension
---@param name string
function M.load(name)
	local mod
	if config and config.overrides and config.overrides[name] then
		mod = load_module(core.modules.user .. "." .. name)
	end
	if not mod then
		mod = load_module(core.modules.base .. "." .. name)
	end
	return mod
end

local function module_try_load(mod, opts)
	local name
	local enabled = true


	if type(mod) == "table" then
		name = mod.module
		enabled = mod.enabled ~= false
		if enabled and mod.cond then
			enabled = mod.cond(opts)
		end
	else
		name = mod
	end

	return M.load(name)
end

local function try_load_all(list, opts)
	for _, spec in pairs(list) do
		module_try_load(spec, opts)
	end
end

---@param o? DistroConfig
function M.setup(o)
	config = vim.tbl_deep_extend("force", config_defaults, o or {})

	local lazy = core.hook.lazy_call ~= nil

	-- Distro options need to be loaded at reinit() time, but allow for overrides here
	if config and config.overrides and config.overrides["options"] then
		load_module(core.modules.user .. ".options")
	end

	try_load_all(core.modules.config, { lazy = lazy })

	local function on_post_config()
		try_load_all(core.modules.post_config, { lazy = lazy })

		require("distro.filetypes")
	end

	if lazy then
		core.hook.lazy_call(on_post_config)
	else
		on_post_config()
	end

	Util.try(function()
		M.load_colorscheme(config.colorscheme)
	end, {
		msg = "could not load your colorscheme",
		on_error = function(msg)
			M.log:warn(msg)
			M.load_colorscheme(config.fallback.colorscheme)
		end,
	})
end

---@param scheme string|fun()
function M.load_colorscheme(scheme)
	if type(scheme) == "function" then
		scheme()
	else
		vim.cmd.colorscheme(scheme)
	end
end

---@param opts? DistroCoreConfig
function M.reinit(opts)
	core = vim.tbl_deep_extend("force", core_defaults, opts or {})

	try_load_all(core.modules.pre_config)

	-- UI options may have been changed, so reload them
	vim.cmd [[ do VimResized ]]

	M.is_init = true
end

---@param opts? DistroCoreConfig
function M.init(opts)
	if M.is_init then
		return
	end

	M.reinit(opts)
end

setmetatable(M, {
	__index = function(_, key)
		if config == nil then
			return vim.deepcopy(config_defaults)[key]
		end
		return config[key]
	end,
})

return M
