local M = {}

---@param o? DistroConfig
function M.setup(o)
	local config = require("distro.config")
	config.setup(o)
end

return M
