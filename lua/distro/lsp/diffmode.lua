local M = {}

---@class DiffModeConfig
local config = {
	disable_diagnostics = true,
}

local augroup
local function get_augroup(opts)
	if not augroup then
		augroup = vim.api.nvim_create_augroup("LspDiffMode", opts)
	end
	return augroup
end

---@param client_id integer
---@param bufnr integer
---@diagnostic disable-next-line: unused-local
function M.attach(client_id, bufnr)
	if not config.disable_diagnostics then
		return
	end

	vim.api.nvim_clear_autocmds({ group = get_augroup({ clear = false }), buffer = bufnr })

	vim.api.nvim_create_autocmd("OptionSet", {
		group = augroup,
		buffer = bufnr,
		desc = "lsp: configure for the diff mode",
		callback = function(e)
			if e.match ~= "diff" then
				return
			end

			local is_enabled = vim.opt_local.diff:get()
			local old_status = vim.v.option_oldlocal
			if old_status == is_enabled then
				return
			elseif is_enabled then
				vim.diagnostic.disable(e.buf)
				vim.b[e.buf].lsp_diffmode = true
			elseif not is_enabled and vim.b[e.buf].lsp_diffmode then
				vim.diagnostic.enable(e.buf)
			end
			vim.b[e.buf].lsp_diffmode = nil
		end,
	})
end

---Setup LSP diffmode
---@param o? DiffModeConfig Config options
function M.setup(o)
	config = vim.tbl_deep_extend("force", config, o or {})

	get_augroup({ clear = true })
end

return M
