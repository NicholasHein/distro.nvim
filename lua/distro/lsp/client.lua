local protocol = require("vim.lsp.protocol")
local LspMethods = protocol.Methods

---@class distro.lsp.Client: vim.lsp.Client
---@field private _super vim.lsp.Client
---@field server_capabilities lsp.ServerCapabilities
---@field workspace_folders lsp.WorkspaceFolder[]
local Client = {}

---@param client vim.lsp.Client
---@return distro.lsp.Client
function Client.new(client)
	vim.validate { client = { client, "table" } }

	local mt = getmetatable(client) or {}
	if mt.__index == Client.__index then
		return client --[[@as distro.lsp.Client]]
	end

	client.server_capabilities = client.server_capabilities or {}
	client.workspace_folders = client.workspace_folders or {}

	return setmetatable({ _super = client }, Client)
end

---@param filter? vim.lsp.get_clients.Filter
---@return distro.lsp.Client[]
function Client.list(filter)
	local clients = vim.lsp.get_clients(filter)
	return vim.tbl_map(function(client)
		return Client.new(client)
	end, clients)
end

---@param filter? vim.lsp.get_clients.Filter
---@return Iter
function Client.iter(filter)
	local clients = vim.lsp.get_clients(filter)
	return vim.iter(clients):map(function(client)
		return Client.new(client)
	end)
end

---@param id integer
---@return distro.lsp.Client?
function Client.get_by_id(id)
	vim.validate { id = { id, "number" } }

	local c = vim.lsp.get_client_by_id(id)
	if c then
		return Client.new(c)
	else
		return nil
	end
end

---@param tbl table
---@return boolean, table
function Client:extend_settings(tbl)
	vim.validate { tbl = { tbl, "table" } }

	local config = self.config
	config.settings = vim.tbl_deep_extend("force", config.settings or {}, tbl)

	local ok = self.notify(LspMethods.workspace_didChangeConfiguration, { settings = config.settings })
	return ok, config.settings
end

--- Check if the client supports a boolean capability
---@param name string
---@return boolean
function Client:has_capability(name)
	local cap = self.capabilities[name]
	assert(type(cap) == "nil" or type(cap) == "bool", "result only makes sense for boolean capability types")
	return cap == true
end

--- Check if the server that the client is connected to supports a boolean capability
---@param name string
---@return boolean
function Client:server_has_capability(name)
	local cap = self.server_capabilities[name]
	assert(type(cap) == "nil" or type(cap) == "bool", "result only makes sense for boolean capability types")
	return cap == true
end

function Client:__index(key)
	return rawget(self, "_super")[key]
end

return Client
