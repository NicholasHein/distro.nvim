local M = {}
local LspClient = require("lib.lsp.client")

---@class FormatConfig
local config = {
	events = { "BufWritePre" },
}

local global_enabled = true

local function to_onoff(v)
	return v and "on" or "off"
end

---@param client_id integer
---@param bufnr integer
function M.attach(client_id, bufnr)
	local client = LspClient:from_id(client_id)
	if not client:server_capabilities().documentFormattingProvider then
		return
	end

	local augroup = vim.api.nvim_create_augroup("LspFormatting", { clear = false })
	vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })

	local buf_enabled = true
	local function get_status()
		local var_enabled = vim.g.lsp_formatting ~= false
		return {
			enabled = {
				buf = buf_enabled,
				global = global_enabled,
				var = var_enabled,
				general = buf_enabled and global_enabled and var_enabled,
			}
		}
	end

	local function notify_status()
		local e = get_status().enabled
		vim.notify(("lsp: auto-formatting is %s (buf #%u: %s; global: %s%s)"):format(to_onoff(e.general), bufnr,
				to_onoff(e.buf), to_onoff(e.global), e.var and "" or "; disabled by g:lsp_formatting"),
			vim.log.levels.INFO)
	end

	local function set_status(status, set_global)
		if set_global then
			global_enabled = status
		else
			buf_enabled = status
		end

		notify_status()
	end

	local function format_buf(buf, force)
		buf = buf or bufnr
		force = force or false
		if not force and vim.opt.diff:get() then
			return
		end

		vim.lsp.buf.format({ bufnr = buf, async = false })
	end

	vim.api.nvim_create_autocmd(config.events, {
		group = augroup,
		buffer = bufnr,
		desc = "lsp: auto-formatting",
		callback = function(e)
			if not get_status().enabled.general then
				return
			end
			format_buf(e.buf)
		end,
	})
	local arg_values = {
		toggle = function(cmdinfo)
			if cmdinfo.bang then
				set_status(not global_enabled, true)
			else
				set_status(not buf_enabled, false)
			end
		end,
		on = function(cmdinfo)
			set_status(true, cmdinfo.bang)
		end,
		off = function(cmdinfo)
			set_status(false, cmdinfo.bang)
		end,
		show = function(_)
			notify_status()
		end,
		now = function(cmdinfo)
			format_buf(nil, cmdinfo.bang)
		end
	}
	vim.api.nvim_buf_create_user_command(bufnr, "LspFormat", function(cmdinfo)
		local func = arg_values["toggle"]

		if #cmdinfo.fargs >= 1 then
			local choice = cmdinfo.fargs[1]

			func = arg_values[choice]
			if not func then
				vim.notify(
					("LspFormat: invalid argument %s"):format(choice), vim.log.levels.ERROR)
				return
			end
		end

		func(cmdinfo)
	end, {
		nargs = "?",
		bang = true,
		desc = "lsp: control auto-formatting",
		force = true,
		---@diagnostic disable-next-line: unused-local
		complete = function(arglead, cmdline, cursorpos)
			local cmdinfo = vim.api.nvim_parse_cmd(string.sub(cmdline, 0, cursorpos), {})
			if #cmdinfo.args == 0 then
				return vim.tbl_keys(arg_values)
			end
			return {}
		end,
	})
end

---Setup LSP formatting
---@param o? FormatConfig Config options
function M.setup(o)
	config = vim.tbl_deep_extend("force", config, o or {})
end

return M
