local M = {}

---@class HoverConfig
local config = {
	events = {
		highlight = { "CursorHold", "CursorHoldI" },
		clear = { "CursorMoved" },
	},
}

---@param client_id integer
---@param bufnr integer
---@diagnostic disable-next-line: unused-local
function M.attach(client_id, bufnr)
	local augroup = vim.api.nvim_create_augroup("LspHover", {})
	vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })

	local client = vim.lsp.get_client_by_id(client_id)
	if not client.server_capabilities.documentHighlightProvider then
		return
	end

	vim.api.nvim_create_autocmd(config.events.highlight, {
		group = augroup,
		buffer = bufnr,
		desc = "lsp: hover",
		callback = function()
			vim.lsp.buf.document_highlight()
		end,
	})

	vim.api.nvim_create_autocmd(config.events.clear, {
		group = augroup,
		buffer = bufnr,
		desc = "lsp: end hover",
		callback = function()
			vim.lsp.buf.clear_references()
		end,
	})
end

---Setup LSP formatting
---@param o? HoverConfig Config options
function M.setup(o)
	config = vim.tbl_deep_extend("force", config, o or {})
end

return M
