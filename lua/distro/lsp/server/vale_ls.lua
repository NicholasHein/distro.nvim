local LspServer = require("lib.lsp.server")

---@class ValeLs: LspServer
local ValeLs = LspServer.define({ name = "vale_ls" })
return ValeLs
