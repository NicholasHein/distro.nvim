local LspServer = require("lib.lsp.server")

---@class RobotFrameworkLs: LspServer
local RobotFrameworkLs = LspServer.define({ name = "robotframework_ls" })
return RobotFrameworkLs
