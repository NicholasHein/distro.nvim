local LspServer = require("lib.lsp.server")

---@class VimLs: LspServer
local VimLs = LspServer.define({ name = "vimls" })
return VimLs
