local LspServer = require("lib.lsp.server")

-- NOTE: as of clangd 15, format-as-you-type ("a" in formatoptions) does not work well[^1].

-- NOTE: .clang-format-ignore is only supported by clang-format >=18, which is
-- still in development.  Emulate support for this feature for convenience.

---@class ClangdClient: LspClient
---@field emulate_format_ignore? boolean

---@class Clangd: LspServer
local Clangd = LspServer.define({
	name = "clangd",
	server = {
		cmd_env = { CLANGD_FLAGS = "--background-index-priority=low --malloc-trim" },
		handlers = {
			["textDocument/formatting"] = function(err, result, ctx, config)
				local bufnr = ctx.bufnr or nil
				if vim.b[bufnr].clangd_format_ignore then
					return
				end

				local request_handler = vim.lsp.handlers["textDocument/formatting"]
				if request_handler then
					request_handler(err, result, ctx, config)
				end
			end,
		},
	},
})

function Clangd:init(client, initialize_result)
	local clangd_version = vim.tbl_get(initialize_result, "serverInfo", "version")
	if not clangd_version then
		vim.notify("clangd: InitializeResult:serverInfo.version not provided", vim.log.levels.DEBUG)
		return
	end

	local ver = vim.version.parse(clangd_version)
	if not ver then
		vim.notify(("clangd: could not parse version '%s'"):format(clangd_version), vim.log.levels.DEBUG)
		return
	end

	vim.notify(("version: %s"):format(clangd_version), vim.log.levels.DEBUG)
	if not vim.version.gt(ver, { 18, 0, 0 }) then
		vim.notify("version not >=18; will emulate clang-format-ignore support", vim.log.levels.DEBUG)
		client.emulate_format_ignore = true
	end
	return true
end

function Clangd:attach(client, bufnr)
	if client.emulate_format_ignore and client:search_workspace({ ".clang-format-ignore" }) then
		vim.b[bufnr].clangd_format_ignore = true
		vim.notify(".clang-format-ignore found", vim.log.levels.DEBUG)
	else
		vim.b[bufnr].clangd_format_ignore = nil
	end
end

return Clangd
