local LspServer = require("lib.lsp.server")

---@class distro.lsp.server.AnsibleLs: LspServer
local AnsibleLs = LspServer.define { name = "ansiblels" }
return AnsibleLs
