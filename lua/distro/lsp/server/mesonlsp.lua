local LspServer = require("lib.lsp.server")

---@class MesonLsp: LspServer
local MesonLsp = LspServer.define({ name = "mesonlsp" })

local function get_mason_package(name)
	local mlsp = require("mason-lspconfig")
	local pkgname = mlsp.get_mappings().lspconfig_to_mason[name]
	if not pkgname then
		return nil
	end

	local mr = require("mason-registry")
	return mr.get_package(pkgname)
end

function MesonLsp:is_installed()
	local pkg = get_mason_package(self.name)
	return pkg and pkg:is_installed() or false
end

function MesonLsp:before_setup()
	local mlsp = require("mason-lspconfig")
	local servers = mlsp.get_available_servers({ filetype = "meson" })
	if not vim.tbl_contains(servers, self.name) then
		return false
	end

	local pkg = get_mason_package(self.name)
	if pkg and not pkg:is_installed() then
		pkg:install()

		vim.notify_once(
			("%s is now available in the Mason registry! Please add it to mason-lspconfig's ensure_installed option.")
			:format(self.name),
			vim.log.levels.INFO,
			{ title = ("LSP Server: %s"):format(self.name) }
		)
	end
end

return MesonLsp
