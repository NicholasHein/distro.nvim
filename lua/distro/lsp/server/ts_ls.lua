local LspServer = require("lib.lsp.server")

---@class distro.lsp.server.TsLs: LspServer
local TsLs = LspServer.define({ name = "ts_ls" })
return TsLs
