local LspServer = require("lib.lsp.server")

---@class Lemminx: LspServer
local Lemminx = LspServer.define({ name = "lemminx" })
return Lemminx
