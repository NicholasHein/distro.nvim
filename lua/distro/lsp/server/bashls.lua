local LspServer = require("lib.lsp.server")

---@class BashLs: LspServer
local BashLs = LspServer.define({
	name = "bashls",
	server = {
		filetypes = { "sh", "bash" },
		cmd_env = {
			GLOB_PATTERN = "*@(.sh|.inc|.bash|.command|.zsh|.bb)"
		},
	},
})

return BashLs
