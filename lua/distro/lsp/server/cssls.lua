local LspServer = require("lib.lsp.server")

---@class CssLs: LspServer
local CssLs = LspServer.define({ name = "cssls" })

function CssLs:init(client)
	client:assert_capability(
		"textDocument.completion.completionItem.snippetSupport",
		true,
		"snippet support is required for completion")
	return true
end

return CssLs
