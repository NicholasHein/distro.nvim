local LspServer = require("lib.lsp.server")

---@class GhdlLs: LspServer
local GhdlLs = LspServer.define({ name = "ghdl_ls" })
return GhdlLs
