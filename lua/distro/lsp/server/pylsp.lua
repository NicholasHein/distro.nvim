local LspServer = require("lib.lsp.server")

---@class PyLsp: LspServer
local PyLsp = LspServer.define({
	name = "pylsp",
	server = {
		settings = {
			pylsp = {
				plugins = {
					flake8 = { enabled = true },
					pycodestyle = { enabled = false }, -- Wrapped by flake8
					black = { enabled = true },
				},
			},
		},
	}
})

return PyLsp
