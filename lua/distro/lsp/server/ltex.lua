local LspServer = require("lib.lsp.server")

local MEMORY_LIMIT = "500M"

---@param limit string
---@param ... string
---@return string[]
local function systemd_scope_memory_limit_cmd(limit, ...)
	return { "systemd-run", "--user", "--scope", "-p", ("MemoryMax=%s"):format(limit), ... }
end

---@class LTex: LspServer
local LTex = LspServer.define({
	name = "ltex",
	server = {
		cmd = systemd_scope_memory_limit_cmd(MEMORY_LIMIT, "ltex-ls"),
		filetypes = { "bib", "plaintex", "rst", "tex", "pandoc" },
	},
})

---@param limit string
function LTex:set_memory_limit(limit)
	self.server.cmd = systemd_scope_memory_limit_cmd(limit)
end

return LTex
