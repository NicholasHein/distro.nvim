local LspServer = require("lib.lsp.server")
require("lib.type.string")

---@class LuaLs: LspServer
local LuaLs = LspServer.define({ name = "lua_ls" })
return LuaLs
