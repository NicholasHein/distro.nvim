local LspServer = require("lib.lsp.server")

---@class Texlab: LspServer
local Texlab = LspServer.define({ name = "texlab" })

function Texlab:init(client)
	local sys = require("lib.sys")
	local forward_search = nil

	local exe = sys.find_exe_in_path("zathura")
	if exe then
		forward_search = {
			executable = exe,
			args = {
				"--syntex-forward",
				"%l:1:%f",
				"%p",
			},
		}
	end

	if forward_search then
		client:extend_settings({
			texlab = { forwardSearch = forward_search },
		})
	end
	return true
end

return Texlab
