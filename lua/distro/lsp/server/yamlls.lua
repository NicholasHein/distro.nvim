local LspServer = require("lib.lsp.server")

---@class YamlLs: LspServer
local YamlLs = LspServer.define({ name = "yamlls" })

---@param client LspClient
function YamlLs:init(client)
	local core = require("distro.core")
	core.try_require("schemastore"):inspect(function(schemastore)
		local schemas = schemastore.yaml.schemas()
		client:extend_settings({
			yaml = {
				schemaStore = {
					enable = false,
					uri = "",
				},
				schemas = schemas,
			},
		})
	end)
	return true
end

return YamlLs
