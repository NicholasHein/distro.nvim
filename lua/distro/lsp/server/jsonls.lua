local LspServer = require("lib.lsp.server")

---@class JsonLs: LspServer
local JsonLs = LspServer.define({ name = "jsonls" })

---@param client LspClient
function JsonLs:init(client)
	client:assert_capability(
		"textDocument.completion.completionItem.snippetSupport",
		true,
		"snippet support is required for completion")

	local core = require("distro.core")
	core.try_require("schemastore"):inspect(function(schemastore)
		local schemas = schemastore.json.schemas()
		client:extend_settings({
			json = {
				schemas = schemas,
				validate = { enable = true },
			},
		})
	end)
	return true
end

return JsonLs
