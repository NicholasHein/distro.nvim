local LspServer = require("lib.lsp.server")

---@class SwiftMesonLs: LspServer
local SwiftMesonLs = LspServer.define({
	name = "swift_mesonls",
	is_deprecated = true,
})

function SwiftMesonLs:before_setup()
	-- MesonLsp is currently not provided by mason-lspconfig
	local MesonLsp = require("distro.lsp.server.mesonlsp")
	if MesonLsp:is_installed() then
		return false
	end
end

return SwiftMesonLs
