local LspServer = require("lib.lsp.server")

---@class distro.lsp.server.JqLs: LspServer
local JqLs = LspServer.define { name = "jqls" }
return JqLs
