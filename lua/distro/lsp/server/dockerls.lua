local LspServer = require("lib.lsp.server")

---@class DockerLs: LspServer
local DockerLs = LspServer.define({ name = "dockerls" })
return DockerLs
