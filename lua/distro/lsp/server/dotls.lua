local LspServer = require("lib.lsp.server")

---@class DotLs: LspServer
local DotLs = LspServer.define({ name = "dotls" })
return DotLs
