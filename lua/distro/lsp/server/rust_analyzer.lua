local LspServer = require("lib.lsp.server")
local LspClient = require("lib.lsp.client")
local lsplib = require("lib.lsp")
local Object = require("lib.object")
require("lib.type.string")

-- NOTE: to get the default server config for rust-analyzer, run:
--
--     rust-analyzer --print-config-schema

---@class RustAnalyzer: LspServer
local RustAnalyzer = LspServer.define({ name = "rust_analyzer" })
return RustAnalyzer
