local LspServer = require("lib.lsp.server")

---@class distro.lsp.server.GinkoLs
local GinkoLs = LspServer.define { name = "ginko_ls" }
return GinkoLs
