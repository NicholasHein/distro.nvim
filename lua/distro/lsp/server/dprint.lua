local LspServer = require("lib.lsp.server")

---@class distro.lsp.server.DPrint: LspServer
local DPrint = LspServer.define({
	name = "dprint",
	server = {
		filetypes = { "markdown" },
	},
})
return DPrint
