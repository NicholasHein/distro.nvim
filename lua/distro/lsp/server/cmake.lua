local LspServer = require("lib.lsp.server")

---@class CMake: LspServer
local CMake = LspServer.define({ name = "cmake" })
return CMake
