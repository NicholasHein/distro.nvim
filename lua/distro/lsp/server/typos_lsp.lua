local LspServer = require("lib.lsp.server")

---@class distro.lsp.server.TyposLsp: LspServer
local TyposLsp = LspServer.define { name = "typos_lsp" }
return TyposLsp
