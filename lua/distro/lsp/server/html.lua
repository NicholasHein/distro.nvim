local LspServer = require("lib.lsp.server")

---@class HtmlLs: LspServer
local HtmlLs = LspServer.define({ name = "html" })

function HtmlLs:init(client)
	client:assert_capability(
		"textDocument.completion.completionItem.snippetSupport",
		true,
		"snippet support is required for completion")
	return true
end

return HtmlLs
