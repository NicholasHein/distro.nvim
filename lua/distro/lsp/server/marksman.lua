local LspServer = require("lib.lsp.server")

---@class Marksman: LspServer
local Marksman = LspServer.define({ name = "marksman" })
return Marksman
