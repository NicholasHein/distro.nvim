local LspServer = require("lib.lsp.server")

---@class distro.lsp.server.BitbakeLs: LspServer
local BitbakeLs = LspServer.define({ name = "bitbake_ls" })
return BitbakeLs
