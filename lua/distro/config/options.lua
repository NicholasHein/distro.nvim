vim.g.mapleader = ","

vim.o.exrc = true

vim.o.mouse = ""
vim.o.background = "dark"
vim.o.termguicolors = true

vim.o.number = true
vim.o.ruler = true
vim.o.wrap = false
vim.o.signcolumn = "yes"
-- vim.o.cursorline = true
-- vim.o.cursorcolumn = true
vim.o.equalalways = false
vim.o.splitbelow = true
vim.o.splitright = true
vim.o.scrolloff = 1
vim.o.sidescrolloff = 1
vim.o.autochdir = false

vim.o.ignorecase = true
vim.o.smartcase = true

vim.opt.viewoptions = { "cursor", "curdir" }
vim.opt.diffopt:append { "iwhite", "vertical" }

vim.o.undofile = true
vim.o.backup = true
vim.o.backupdir = vim.fn.stdpath("state") .. "/backup//"

vim.o.list = true
vim.opt.listchars = {
	tab = [[» ┊]],
	trail = [[×]],
	nbsp = [[␣]],
	space = [[·]],
	eol = [[↲]],
	extends = [[›]],
	precedes = [[‹]],
}
vim.o.showbreak = [[↪ ]]
