local function load_global_maps(path)
	local Config = require("distro.config")
	Config.load(path)
end

local visual_mode = require("lib.editor.visual")
local event_map = {
	["CmdlineEnter"] = { name = "Command", path = "keymap.command" },
	["InsertEnter"] = { name = "Insert", path = "keymap.insert" },
	["TermEnter"] = { name = "Terminal", path = "keymap.terminal" },
	-- HACK: I don't believe there's a better event type for this
	["ModeChanged"] = {
		name = "Visual",
		path = "keymap.visual",
		pattern = ("*:[%s]*"):format(table.concat(visual_mode.mode_list)),
	},
}

local function lazy_load_global_maps(ev)
	local evtype = assert(event_map[ev.event])
	load_global_maps(assert(evtype.path))
end

local aug = vim.api.nvim_create_augroup("DistroKeymapModes", {})
for event, evtype in pairs(event_map) do
	vim.api.nvim_create_autocmd(event, {
		group = aug,
		once = true,
		desc = ("keymap: lazily set up %s keymaps"):format(evtype.name),
		callback = lazy_load_global_maps,
	})
end

-- This one can't be lazy
load_global_maps("keymap.normal")
