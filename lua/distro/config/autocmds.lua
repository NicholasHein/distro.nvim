local aug

-- LSP On Attach
aug = vim.api.nvim_create_augroup("LspOnAttach", { clear = true })
vim.api.nvim_create_autocmd("LspAttach", {
	group = aug,
	desc = "lsp: setup auto-formatting",
	callback = function(ev) require("distro.lsp.format").attach(ev.data.client_id, ev.buf) end,
})
vim.api.nvim_create_autocmd("LspAttach", {
	group = aug,
	desc = "lsp: setup hover document",
	callback = function(ev) require("distro.lsp.hover").attach(ev.data.client_id, ev.buf) end,
})
vim.api.nvim_create_autocmd("LspAttach", {
	group = aug,
	desc = "lsp: setup diffmode",
	callback = function(ev) require("distro.lsp.diffmode").attach(ev.data.client_id, ev.buf) end,
})

-- Auto-ColorColumn
aug = vim.api.nvim_create_augroup("AutoColorColumn", { clear = true })
vim.api.nvim_create_autocmd("OptionSet", {
	group = aug,
	pattern = "textwidth",
	desc = "colorcolumn: change status",
	callback = function(ev) require("distro.utils.colorcolumn.auto").sync(ev) end,
})
vim.api.nvim_create_autocmd("FileType", {
	group = aug,
	pattern = "*",
	desc = "colorcolumn: setup",
	callback = function(ev) require("distro.utils.colorcolumn.auto").sync(ev) end,
})

-- QuickFix
-- Vim automatically jumps to the quickfix window when :make detects an error...
-- THIS IS INCREDIBLY ANNOYING!!!
--
-- Normally, you could prevent this by using :make! but who has time to add an
-- extra "!"?
--
-- This autocommand disables the quickfix window for :make altogether by
-- clearing all errors.
aug = vim.api.nvim_create_augroup("QuickFixPrevention", { clear = true })
vim.api.nvim_create_autocmd("QuickFixCmdPost", {
	group = aug,
	pattern = { "make" },
	desc = "quickfix: disable",
	callback = function() vim.fn.setqflist({}, "r") end,
})

-- FIXME:
-- More intuitive tab closing
-- aug = vim.api.nvim_create_augroup("TabpageTracking", { clear = true })
-- vim.api.nvim_create_autocmd("TabEnter", {
-- 	group = aug,
-- 	pattern = "*",
-- 	desc = "tab tracking: add to history",
-- 	callback = function() require("distro.utils.tabpage.tracking").push_current() end,
-- })
-- vim.api.nvim_create_autocmd("TabClosed", {
-- 	group = aug,
-- 	pattern = "*",
-- 	desc = "tab tracking: goto previous",
-- 	callback = function() require("distro.utils.tabpage.tracking").pop_and_goto() end,
-- })

-- Terminal setup/teardown
aug = vim.api.nvim_create_augroup("TerminalConfig", { clear = true })
vim.api.nvim_create_autocmd("TermOpen", {
	group = aug,
	pattern = "*",
	desc = "terminal: configure local terminal options",
	callback = function()
		vim.opt_local.modifiable = true
		vim.opt_local.signcolumn = "no"
		vim.opt_local.showbreak = "NONE"
		vim.opt_local.wrap = true
		vim.opt_local.number = false
		vim.opt_local.cursorline = false
		vim.opt_local.cursorcolumn = false
	end,
})
vim.api.nvim_create_autocmd("TermOpen", {
	group = aug,
	pattern = "*",
	desc = "terminal: start insert mode immediately",
	callback = function(info)
		local buffer = info.buf
		vim.api.nvim_buf_call(buffer, function()
			vim.cmd [[ startinsert ]]
		end)
	end,
})
vim.api.nvim_create_autocmd("TermClose", {
	group = aug,
	pattern = "term://*",
	desc = "terminal: finish closing the terminal",
	callback = function(info)
		local buffer = info.buf
		vim.api.nvim_buf_call(buffer, function()
			vim.api.nvim_input("<CR>")
		end)
	end,
})
