local function map(...)
	vim.keymap.set("t", ...)
end

-- Mode
map([[<Esc>]], [[<C-Bslash><C-n>]])

-- Escape
map([[<M-Esc>]], [[<Esc>]])
map([[<M-,>]], [[<Esc>]])
