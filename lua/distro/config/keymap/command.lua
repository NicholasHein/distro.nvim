local function map(...)
	vim.keymap.set("c", ...)
end

vim.go.cedit = [[<C-o>]]

-- Text navigation
map([[<M-h>]], [[<Left>]])
map([[<M-l>]], [[<Right>]])
map([[<M-H>]], [[<C-Left>]])
map([[<M-L>]], [[<C-Right>]])
map([[<M-C-H>]], [[<Home>]])
map([[<M-C-L>]], [[<End>]])

-- History traversal
map([[<C-k>]], [[<Up>]])
map([[<C-j>]], [[<Down>]])

-- Clipboard
map([[<M-v>]], [[<C-R><C-R>]])
map([[<C-v>]], [[<C-R><C-R>"]])

-- Wild menu
local function if_wildmenu(yes, no)
	return function()
		if vim.fn.wildmenumode() ~= 0 then
			return yes
		else
			return no
		end
	end
end

map([[<CR>]], if_wildmenu([[<C-y>]], [[<CR>]]), { expr = true })
map([[<Esc>]], if_wildmenu([[<C-e>]], [[<C-c>]]), { expr = true })
