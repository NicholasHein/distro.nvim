local function literal_char_expr(chr)
	return [[<C-v>]] .. chr
end

local function map(...)
	vim.keymap.set("i", ...)
end

-- One-shot normal mode
map([[<C-Bslash>]], [[<C-o>]])

-- Traversal
map([[<M-j>]], [[<Down>]])
map([[<M-k>]], [[<Up>]])
map([[<M-h>]], [[<Left>]])
map([[<M-l>]], [[<Right>]])

-- View shifting
map([[<C-j>]], [[<C-o><C-e>]])
map([[<C-k>]], [[<C-o><C-y>]])
map([[<C-h>]], [[<C-o>zh]])
map([[<C-l>]], [[<C-o>zl]])

-- Word deleting
map([[<M-BS>]], [[<C-o>db]])
map([[<M-Del>]], [[<C-o>dw]])

-- Pasting
map([[<C-v>]], [[<C-r>"]], { desc = "paste unnamed register" })
map([[<M-v>]], [[<C-r>+]], { desc = "paste from clipboard" })

-- Literal CR
map([[<M-CR>]], [[<CR>]], { desc = [[literal CR]] })

local literal_chars = { "(", ")", "{", "}", "[", "]", "\"", "'", "`", "lt", ">" }
for _, chr in ipairs(literal_chars) do
	map(("<M-%s>"):format(chr), literal_char_expr(chr), { desc = ("literal `%s`"):format(chr) })
end
