local function map(...)
	vim.keymap.set("n", ...)
end

-- View shifting
map([[<C-k>]], [[<C-y>]])
map([[<C-j>]], [[<C-e>]])
map([[<C-h>]], [[zh]])
map([[<C-l>]], [[zl]])
map([[<M-C-Space>]], [[zz]])
map([[<M-C-K>]], [[zb]])
map([[<M-C-J>]], [[zt]])
map([[<M-C-H>]], [[ze]])
map([[<M-C-L>]], [[zs]])

-- Tabs
map([[<Tab>]], [[gt]])
map([[<S-Tab>]], [[gT]])
map([[<M-[>]], [[<cmd>tabmove -1<cr>]])
map([[<M-]>]], [[<cmd>tabmove +1<cr>]])
map([[<C-t>]], [[<cmd>tab split<cr>]])
map([[<M-Q>]], [[<cmd>tabclose<cr>]])

-- Windows
map([[<M-h>]], [[<C-w>h]])
map([[<M-j>]], [[<C-w>j]])
map([[<M-k>]], [[<C-w>k]])
map([[<M-l>]], [[<C-w>l]])
map([[<M-H>]], [[<C-w>H]])
map([[<M-J>]], [[<C-w>J]])
map([[<M-K>]], [[<C-w>K]])
map([[<M-L>]], [[<C-w>L]])
map([[<M-+>]], [[<C-w>+]])
map([[<M-->]], [[<C-w>-]])
map([[<M-gt>]], [[<C-w>>]])
map([[<M-<>]], [[<C-w><lt>]])
map([[<M-=>]], [[<C-w>=]])
map([[<M-Bar>]], [[<C-w><Bar>]])
map([[<M-/>]], [[<C-w>x]])
map([[<M-q>]], [[<C-w>q]])
map([[<C-Bslash>]], [[<cmd>only<cr>]])

-- Jump list
map([[<M-C-O>]], [[<C-o>]])
map([[<M-C-I>]], [[<C-i>]])

-- Text
map([[D]], [[0D]])

-- Copy
map([[yy]], [[yy]], { nowait = true })
map([[yY]], function() vim.fn.setreg("\"", vim.fn.getline("."), "al") end, { nowait = true })

-- Numbers
map([[<M-a>]], [[<C-a>]])
map([[<M-A>]], [[<C-x>]])

-- Highlighting
map([[<C-/>]], [[<cmd>nohlsearch<cr>]])
map([[<C-_>]], [[<cmd>nohlsearch<cr>]]) -- <C-_> == <C-/>

-- Folding
local function toggle_fold_expr()
	if vim.fn.foldclosed(vim.fn.line(".")) == -1 then
		return "zc"
	else
		return "za"
	end
end

map([[L]], toggle_fold_expr, { desc = "folding: expand next or toggle", expr = true })
map([[H]], [[zc]], { desc = "folding: collapse" })

map([[<leader>L]], [[zR]], { desc = "folding: expand all" })
map([[<leader>H]], [[zM]], { desc = "folding: collapse all" })

-- Grab/put buffers
local bufclip = require("distro.utils.buffer.clipboard").new({ log = true })
map([[<M-g>]], function() bufclip:store() end, { desc = "buffer: grab" })
map([[<M-C-G>]], function() bufclip:put_and_split {} end, { desc = "buffer: put and split" })
map([[<M-G>]], function() bufclip:put_and_split { vsplit = true } end, { desc = "buffer: put and vsplit" })
