local input = require("lib.editor.input")
local visual = require("lib.editor.visual")

local surroundings = {
	{ key = "(",  left = "(",  right = ")" },
	{ key = "{",  left = "{",  right = "}" },
	{ key = "[",  left = "[",  right = "]" },
	{ key = "\"", left = "\"", right = "\"" },
	{ key = "'",  left = "'",  right = "'" },
	{ key = "`",  left = "`",  right = "`" },
	{ key = "<",  left = "<",  right = ">" },
}

local function vmap(...) vim.keymap.set("v", ...) end

local function xmap(...) vim.keymap.set("x", ...) end

-- Folding
xmap([[H]], [[zf]], { desc = "folding: create and collapse" })
xmap([[L]], [[<cmd>normal! zd<CR>]], { desc = "folding: delete" })

-- Persistent pasting
vmap([[<M-p>]], [["0p]], { desc = "paste yanked or deleted text" })
vmap([[<M-P>]], [["0P]], { desc = "paste yanked or deleted text" })

-- Search
xmap([[<C-/>]], function()
	return [[<Esc>/]] .. table.concat(visual.selected_lines(), "\n") .. [[<cr>]]
end, { expr = true, desc = "search for visually selected text" })

-- Surrounding
for _, info in ipairs(surroundings) do
	xmap(("<M-%s>"):format(info.key), visual.expr_surround_selection(info.left, info.right), {
		desc = ("surround with %s%s"):format(info.left, info.right),
	})
end

local function surround_with_input()
	local str = ""
	while true do
		local chr = input.getc()
		if chr == input.to_termcode_repr([[<Esc>]]) then
			return ""
		elseif chr == input.to_termcode_repr([[<cr>]]) then
			break
		end

		str = str .. chr
	end

	if string.len(str) == 0 then
		return ""
	end

	-- Reverse of "str"
	local rts = string.reverse(str)
	return visual.expr_surround_selection(str, rts)
end

xmap([[<C-w>]], surround_with_input, { expr = true, desc = "surround with given input" })

-- Increment/decrement (because I use <C-a> for other things)
xmap([[<M-a>]], [[<C-a>]], { desc = "increment" })
xmap([[<M-A>]], [[<C-x>]], { desc = "decrement" })

-- Assorted shortcuts
xmap([[i]], [[<Esc>i]], { desc = "escape from selection and switch to insert mode" })
