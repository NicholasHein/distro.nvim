---@class distro.core.Error
---@overload fun(err: distro.core.Error, opts?: distro.core.ErrorOpts, level?: integer)
local Error = {}

local validate = require("distro.core.validate")

---@class distro.core.ErrorOpts<E>
---@field msg? fun(err: any): string | string

---@generic E
---@param e E
---@param opts? distro.core.ErrorOpts<E>
---@return distro.core.Error
function Error.new(e, opts)
	validate {
		e = { e, "table" },
		opts = {
			opts,
			function(o)
				if o == nil then return true end
				if o ~= type("table") then return false, "expected table|nil" end
				validate {
					["opts.msg"] = { o.msg, { "function", "string" }, true },
					-- ["opts.track_caller"] = { o.msg, "boolean", true },
				}
				return true -- Always valid if it doesn't error out
			end,
			"distro.core.ErrorOpts|nil"
		}
	}

	opts = vim.tbl_deep_extend("force", {
		msg = function(err)
			local mt = getmetatable(err) or {}
			if mt.__error ~= nil then
				if type(mt.__error) == "string" then
					return mt.__error
				elseif type(mt.__error) == "function" then
					return mt.__error(err)
				else
					error(("expected string|function for __error, got %s"):format(type(mt.__error)))
				end
			elseif mt.__tostring ~= nil then
				return mt.__tostring(err)
			elseif mt.__name ~= nil then
				return mt.__name
			else
				return ("error: %s"):format(vim.inspect(err))
			end
		end,
	}, opts or {})

	local oldmt = getmetatable(e) or {}
	local mt = vim.deepcopy(oldmt)
	mt.__super = oldmt

	mt.__index = function(t, key)
		local m = getmetatable(t)
		local ret = rawget(m, key)
		if ret ~= nil then
			return ret
		end

		return (rawget(m, "__super") or {})[key]
	end

	mt.__call = Error.panic
	mt.__tostring = function(t)
		local m = getmetatable(t) or {}
		if m.__error ~= nil then
			if type(m.__error) == "string" then
				return m.__error
			elseif type(m.__error) == "function" then
				return m.__error(t)
			else
				error(("expected `string|function` for __error, got %s"):format(type(m.__error)))
			end
		elseif m.__super.__tostring ~= nil then
			return m.__super.__tostring(t)
		elseif m.__name ~= nil then
			return m.__name
		else
			return ("error: %s"):format(vim.inspect(t))
		end
	end
	return setmetatable(e, mt)
end

---@param level? integer
---@return integer
local function adjust_level_for_caller(level)
	-- Let the caller disable position info like a normal call to `error()`
	if level ~= 0 then
		-- `level` defaults to 1 like with `error()`
		level = level ~= nil and level or 1
		-- Point to the caller's position rather than this function's
		level = level + 1
	end
	return level
end

---@generic T: distro.core.Error
---@param error T
---@param level? integer See `error()`
function Error.panic(error, level)
	level = adjust_level_for_caller(level)
	error(tostring(error), level)
end

---@generic T: distro.core.Error
---@param error T
function Error.panic_caller(error)
	-- 1 = this scope
	-- 2 = caller's scope
	-- 3 = caller's caller's scope (what we want)
	Error.panic(error, adjust_level_for_caller(2) --[[returns 3]])
end

do
	local scope_is_tracked = true

	---@generic T
	---@param f fun(): T
	---@param level? integer
	---@return T
	function Error.untracked_scope(f, level)
		-- Only pcall if not already in an untracked scope
		if scope_is_tracked == true then
			level = adjust_level_for_caller(level)

			-- Catch any errors so they can be thrown again from here
			scope_is_tracked = false
			local ok, result = pcall(f)
			scope_is_tracked = true

			if ok then
				return result
			else
				-- Throw the error again from the tracked level
				error(result, level)
			end
		else
			-- Already in an untracked scope, just invoke the function
			return f()
		end
	end
end

return setmetatable(Error --[[@as table]], {
	__index = Error,
	__call = function(_, err, opts, level)
		Error.new(err, opts):panic(adjust_level_for_caller(level))
	end
})
