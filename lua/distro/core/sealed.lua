---@class distro.core.Sealed

---@overload fun(_, value: any): any
local M = {}

---@generic T
---@param v T
---@return distro.core.Sealed
function M.seal(v)
	return rawset(v, "__newindex", function(_, _, _) error("sealed types are immutable") end)
end

return M
