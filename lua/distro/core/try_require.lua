local Result = require("distro.core.result")

---@class distro.core.TryRequire
---@field mod string
---@overload fun(mod: string, opts: distro.core.TryRequireCallOpts): distro.core.Result
---@overload fun(self, mod: string, opts: distro.core.TryRequireCallOpts): distro.core.Result
local TryRequire = {}

---@class distro.core.TryRequireCallOpts
---@field trace distro.core.TraceOpts

---@class distro.core.TryRequireError: distro.core.Error
---@field err string
---@field trace distro.core.TraceFrame[]
---@field opts? distro.core.TryErrorOpts
local TryRequireError = {}

---@param mod string
---@return distro.core.TryRequire
function TryRequire.new(mod)
	return setmetatable({
		mod = mod,
	}, {
		__index = TryRequire,
		__call = function(t, opts)
			return t:call(opts)
		end
	})
end

---@param opts? distro.core.TryRequireCallOpts
---@return distro.core.Result<unknown, distro.core.TryRequireError>
function TryRequire:call(opts)
	opts = vim.tbl_deep_extend("force", {}, opts or {})

	---@type distro.core.Result<unknown, distro.core.TryRequireError>
	local result = nil

	local try = require("distro.core.try")
	try.call(function()
		local output = require(self.mod)
		-- Executed only if require doesn't error out
		result = Result.ok(output) --[[@as distro.core.Result<unknown, distro.core.TryRequireError>]]
	end, {
		msg = ("failed to load `%s`"):format(self.mod),
		trace = opts.trace,
		---@param err string
		---@param trace distro.core.TraceFrame[]
		---@param o? distro.core.TryErrorOpts
		on_error = function(err, trace, o)
			result = Result.err({
				err = err,
				trace = trace,
				opts = o
			}) --[[@as distro.core.Result<unknown, distro.core.TryRequireError>]]
		end
	})

	return assert(result)
end

return setmetatable(TryRequire --[[@as table]], {
	__call = function(_, mod, opts)
		return TryRequire.new(mod):call(opts)
	end
})
