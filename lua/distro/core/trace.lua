---@class distro.core.Trace
local M = {}

---@param what infowhat
---@param level? integer
---@return debuginfo
function M.caller(what, level)
	level = level or 0
	--   0: debug.getinfo() itself
	--   1: M.caller() itself
	-- >=2: caller, caller's caller, ...
	local info = debug.getinfo(level + 2, what)
	return assert(info)
end

---@return string
function M.caller_source()
	local info = M.caller("S", 1 --[[ exclude M.caller_source() itself ]])
	return assert(info.source)
end

M.THIS_FILE = M.caller_source();

---@class distro.core.TraceOpts: table
---@field level? integer
---@field what? infowhat
---@field excluded_sources? string[]

---@class distro.core.TraceFrame: table
---@field source string
---@field func string
---@field line integer

---Return a stack trace
---@param opts? distro.core.TraceOpts
---@return distro.core.TraceFrame[]
function M.trace(opts)
	opts = vim.tbl_deep_extend("force", {
		level = 0,
		what = "Sln",
		excluded_sources = {},
	}, opts or {})
	--   0: debug.getinfo()
	--   1: M.trace()
	-- >=2: Caller
	opts.level = opts.level + 2
	table.insert(opts.excluded_sources, M.THIS_FILE)

	local trace = {}
	local level = opts.level
	while true do
		local info = debug.getinfo(level, "Sln")
		if not info then break end

		local exclude = false
		for _, exsrc in pairs(opts.excluded_sources) do
			if info.source:find(exsrc) then
				exclude = true
			end
		end

		if info.what ~= "C" and not exclude then
			local source = info.source:sub(2)
			source = vim.fn.fnamemodify(source, ":p:~:.")

			table.insert(trace, {
				source = source,
				func = info.name,
				line = info.currentline,
			})
		end
		level = level + 1
	end
	return trace
end

return setmetatable(M, {
	---Return a stack trace
	---@param opts? distro.core.TraceOpts
	---@return distro.core.TraceFrame[]
	__call = function(_, opts)
		return M.trace(opts)
	end
})
