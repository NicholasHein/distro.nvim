---NOTE: since generic classes/methods aren't well supported, do temporary
---workaround like `vim.RingBuf` does

local Error = require("distro.core.error")

---@class distro.core.Result<T, E>
---@field private _ok boolean
---@field private _value any | distro.core.Error
local Result = {}
Result.__index = Result

---@generic T, E
---@param ok boolean
---@param value T | E
---@return distro.core.Result
function Result.new(ok, value)
	if not ok and getmetatable(value) ~= Error then
		value = Error.new(value)
	end

	return setmetatable({
		_ok = ok,
		_value = value,
	}, Result)
end

---@generic T
---@param value T
---@return distro.core.Result
function Result.ok(value)
	return Result.new(true, value)
end

---@generic E
---@param error E
---@return distro.core.Result
function Result.err(error)
	return Result.new(false, error)
end

---@generic T
---@param value? T
---@return distro.core.Result
function Result.from(value)
	return Result.new(value ~= nil, value)
end

---@return boolean
function Result:is_ok()
	return self._ok
end

---@generic T, U, E
---@param f fun(output: T): distro.core.Result<U, E>
---@return distro.core.Result
function Result:and_then(f)
	if self._ok then
		return f(self._value)
	else
		return self
	end
end

---@generic T, E, F
---@param f fun(output: E): distro.core.Result<T, F>
---@return distro.core.Result
function Result:or_else(f)
	if self._ok then
		return self
	else
		return f(self._value)
	end
end

---@generic T, U
---@param f fun(output: T): U
---@return distro.core.Result
function Result:map(f)
	if self._ok then
		return Result.ok(f(self._value))
	else
		return self
	end
end

---@generic E, F
---@param f fun(output: E): F
---@return distro.core.Result
function Result:map_err(f)
	if self._ok then
		return self
	else
		return Result.err(f(self._value))
	end
end

---@generic T, U, E
---@param d fun(error: E): U
---@param f fun(output: T): U
---@return U
function Result:map_or_else(d, f)
	if self._ok then
		return f(self._value)
	else
		return d(self._value)
	end
end

---@generic T
---@param f fun(output: T)
---@return distro.core.Result
function Result:inspect(f)
	if self._ok then
		f(self._value)
	end
	return self
end

---@generic E
---@param f fun(error: E)
---@return distro.core.Result
function Result:inspect_err(f)
	if not self._ok then
		f(self._value)
	end
	return self
end

---@generic T
---@return T
function Result:unwrap()
	if not self._ok then
		error(self._value, 2)
	end
	return self._value
end

---@generic T
---@return T
function Result:unwrap_err()
	if self._ok then
		error("Result: result was not an error", 2)
	end
	return self._value
end

return setmetatable(Result, {
	__index = Result,
	__name = "Result",
	__tostring = function(t)
		if t._ok then
			return ("Result.ok(%s)"):format(t._value)
		else
			return ("Result.err(%s)"):format(t._value)
		end
	end
})
