---@class distro.core.Once
local Once = {}

local state = {
	global = {},
	buffer = {},
}

---@class distro.core.OnceOpts
---@field buf? integer | boolean

---@param func fun()
---@param opts? distro.core.OnceOpts
function Once.call(func, opts)
	opts = vim.tbl_deep_extend("force", {}, opts or {})

	local s
	local buf = opts.buf
	if buf == nil then
		s = state.global
	else
		if type(buf) == "boolean" then
			buf = vim.api.nvim_get_current_buf()
		end

		s = state.buffer[buf] or {}
		state.buffer[buf] = s
	end

	if s[func] == true then
		return false
	end

	func()
	s[func] = true
	return true
end

return setmetatable(Once, {
	---@param func fun()
	---@param opts? distro.core.OnceOpts
	__call = function(_, func, opts)
		Once.call(func, opts)
	end
})
