---@class distro.core.Try
local M = {}

local trace = require("distro.core.trace")

M.THIS_FILE = trace.caller_source()

---@class distro.core.TryOpts
---@field msg? string
---@field ignore? boolean
---@field trace? distro.core.TraceOpts
---@field on_error? fun(err: string, trace: distro.core.TraceFrame[], opts: distro.core.TryErrorOpts?)

---@class distro.core.TryErrorOpts
---@field msg? string

---Try to execute something safely
---@generic R
---@param fn fun(): R?
---@param opts? distro.core.TryOpts
---@return R?
function M.call(fn, opts)
	opts = vim.tbl_deep_extend("force", {
		on_error = M.on_error,
		trace = {
			level = 0,
			excluded_sources = {},
		},
	}, opts or {})

	local function error_handler(err)
		opts.trace.level = opts.trace.level + 1 -- Ignore error_handler()
		table.insert(opts.trace.excluded_sources, M.THIS_FILE)

		local frames = trace(opts.trace)
		opts.on_error(err, frames, { msg = opts.msg })
	end

	local function noop(...) end

	local ok, result = xpcall(fn, opts.ignore and noop or error_handler)
	return ok and result or nil
end

---Try to load a lua module
---@generic M
---@param mod string
---@param opts? distro.core.TryOpts
---@return M?
function M.require(mod, opts)
	opts = opts or {}
	opts.ignore = opts.ignore ~= nil and opts.ignore or true

	return M.call(function()
		return require(mod)
	end, opts)
end

---@param err string
---@param frames distro.core.TraceFrame[]
---@param opts? distro.core.TryErrorOpts
---@return string msg
function M.format_error(err, frames, opts)
	opts = opts or {}

	local lines = {
		opts.msg,
		err,
		#frames > 0 and "# stacktrace:" or nil
	}

	for _, frame in ipairs(frames) do
		table.insert(lines, ("    - %s:%u _in_ %s"):format(frame.source, frame.line, frame.func or "???"))
	end

	local msg = table.concat(table.filter(lines, function(value) return value ~= nil end), "\n")
	-- Replace tabs with spaces because they don't look right in `:echoerr`
	msg, _ = msg:gsub("\t", (" "):rep(4))
	return msg
end

---@param err string
---@param frames distro.core.TraceFrame[]
---@param opts? distro.core.TryErrorOpts
function M.on_error(err, frames, opts)
	local msg = M.format_error(err, frames, opts)
	vim.schedule(function()
		vim.notify(msg, vim.log.levels.ERROR)
	end)
end

return setmetatable(M, {
	---Try to execute something safely
	---@generic R
	---@param fn fun(): R?
	---@param opts? distro.core.TryOpts
	---@return R?
	__call = function(_, fn, opts)
		return M.call(fn, opts)
	end
})
