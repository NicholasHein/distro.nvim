---@module "vim.shared"

---@class distro.core.Validate
local M = {}

--- Chop off the prefix of error messages
---
--- Turns strings like this:
---
--- ```
--- [string ":lua"]:1: my actual error
--- ```
---
--- into this: `my actual error`.
---
---@param s string
---@return string
local function trim_error_prefix(s)
	local trimmed, _ = s:gsub([[^\[.*\]:[0-9]+: (.*)]], "%1")
	return trimmed
end

---@diagnostic disable-next-line: undefined-doc-name
---@param opt table<vim.validate.Type,vim.validate.Spec> (table) Names of parameters to validate. Each key is a parameter
---@diagnostic disable-next-line: undefined-doc-name
---@return boolean, (string | table<vim.validate.Type,vim.validate.Spec>)
local function wrap_opts(opt)
	if type(opt) ~= "table" then
		return false, ("opt: expected table, got %s"):format(type(opt))
	end

	for _, spec in pairs(opt) do
		if type(spec[2]) == "function" then
			local innerfn = spec[2]
			spec[2] = function(value)
				local ok, result = pcall(innerfn, value);
				if ok then
					return true
				else
					local msg = trim_error_prefix(result)
					return false, msg
				end
			end
		end
	end

	return true, opt
end

--- Try to recursively `vim.validate()`
---
--- @see M.validate
---
---@diagnostic disable-next-line: undefined-doc-name
---@param opt table<vim.validate.Type,vim.validate.Spec> (table) Names of parameters to validate. Each key is a parameter
---@return boolean, string?
function M.try_validate(opt)
	local ok, result = wrap_opts(opt)
	if not ok then
		return false, result --[[@as string]]
	end

	---@diagnostic disable-next-line: undefined-doc-name
	opt = result --[[@as table<vim.validate.Type,vim.validate.Spec>]]

	---@type boolean, string?
	local success, out = pcall(vim.validate, opt)
	if not success then
		out = trim_error_prefix(out --[[@as string]])
	end
	return success, out
end

--- Recursive `vim.validate()`
---
--- Similar to `vim.validate()` except if you use the second variation (ie.
--- `vim.validate(arg_value, fn, msg)`), you can call this function (or
--- `vim.validate()`) inside `fn` to validate the value.
---
---@diagnostic disable-next-line: undefined-doc-name
---@param opt table<vim.validate.Type,vim.validate.Spec> (table) Names of parameters to validate. Each key is a parameter
function M.validate(opt)
	local ok, msg = M.try_validate(opt)
	if not ok then
		error(msg, 2)
	end
end

return setmetatable(M, {
	__index = M,
	---@diagnostic disable-next-line: undefined-doc-name
	---@param opt table<vim.validate.Type,vim.validate.Spec> (table) Names of parameters to validate. Each key is a parameter
	__call = function(_, opt) M.validate(opt) end,
})
