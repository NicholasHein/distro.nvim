local M = {}

---@type distro.core.Once
M.once = require("distro.core.once")
---@type distro.core.Result
M.result = require("distro.core.result")
---@type distro.core.Try
M.try = require("distro.core.try")
---@type distro.core.TryRequire
M.try_require = require("distro.core.try_require")

M.sealed = require("distro.core.sealed")
M.validate = require("distro.core.validate")
M.try_validate = M.validate.try_validate

return M
