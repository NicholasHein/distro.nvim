local M = {}

function M.server(name)
	return require(("config.lsp.server.%s"):format(name))
end

return M
