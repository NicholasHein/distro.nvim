local M = {}

---@alias SplitType
---| '"rightbelow"'
---| '"belowright"'
---| '"leftabove"'
---| '"aboveleft"'
---| '"topleft"'
---| '"botright"'

---@class ScratchConfig
local config = {
	---List the buffer (:h 'buflisted')
	---@type boolean
	buflisted = true,
	---@type SplitType?
	split_type = "rightbelow",
}

---@return integer
local function create_scratch_buf()
	return vim.api.nvim_create_buf(config.buflisted, true)
end

---@param cmd string
---@param bufnr? integer
---@return integer winnr
local function split_scratch_win(cmd, bufnr)
	bufnr = bufnr or create_scratch_buf()

	vim.cmd(cmd)
	local winnr = vim.api.nvim_get_current_win()
	vim.api.nvim_set_current_buf(bufnr)
	vim.api.nvim_win_set_buf(winnr, bufnr)
	return winnr
end

function M.split()
	split_scratch_win(([[%s split]]):format(config.split_type))
end

function M.vsplit()
	split_scratch_win(([[%s vsplit]]):format(config.split_type))
end

function M.tab_split()
	split_scratch_win([[tab split]])
end

local commands = {
	Scratch = {
		fn = M.split,
		desc = "scratch buffer: horizontal split",
	},
	VScratch = {
		fn = M.vsplit,
		desc = "scratch buffer: vertical split",
	},
	TScratch = {
		fn = M.tab_split,
		desc = "scratch buffer: tab split",
	},
}

local function user_command_handler(opts)
	local command = assert(commands[opts.name])
	local callback = assert(command.fn)
	callback()
end

local is_registered = false

---@param force? boolean
---@return boolean ok
function M.register_commands(force)
	if not force and is_registered then return false end

	for name, cmd in pairs(commands) do
		vim.api.nvim_create_user_command(name, user_command_handler, { desc = cmd.desc })
	end

	is_registered = true
	return true
end

---@param force? boolean
---@return boolean ok
function M.unregister_commands(force)
	if not force and not is_registered then return false end

	for name, _ in pairs(commands) do
		vim.api.nvim_del_user_command(name)
	end

	is_registered = false
	return true
end

---@param o? ScratchConfig
function M.setup(o)
	config = vim.tbl_deep_extend("force", config, o or {})
	M.register_commands()
end

return M
