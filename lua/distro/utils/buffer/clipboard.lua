local M = {}

---@class Clipboard
---@field bufnr? integer
---@field logger Logger
local Clipboard = {}
Clipboard.__index = Clipboard

---@generic T: Clipboard
---@param o? T
---@return T
function Clipboard:new(o)
	return setmetatable(o or {}, self)
end

---@param bufnr? integer
function Clipboard:store(bufnr)
	bufnr = bufnr or vim.api.nvim_get_current_buf()
	self.bufnr = bufnr

	self.logger:info("grabbed buffer #%u", bufnr)
end

---Split to the saved buffer and unset it
---@param opts? { winnr: integer?, splitcmd: string?, vsplit: boolean? }
---@return integer?
function Clipboard:put_and_split(opts)
	local bufnr = self.bufnr
	self.bufnr = nil
	if not bufnr then
		self.logger:warn("no grabbed buffer")
		return nil
	elseif not vim.api.nvim_buf_is_valid(bufnr) then
		self.logger:warn("grabbed buffer #%u is invalid", bufnr)
		return nil
	end

	opts = opts or {}

	local winnr = opts.winnr or vim.api.nvim_get_current_win()
	vim.api.nvim_set_current_win(winnr)

	local splitcmd = opts.splitcmd
	if not splitcmd then
		if opts.vsplit then
			splitcmd = "rightbelow vsplit"
		else
			splitcmd = "rightbelow split"
		end
	end
	vim.cmd(splitcmd)

	winnr = vim.api.nvim_get_current_win()
	vim.api.nvim_win_set_buf(winnr, bufnr)
	return bufnr
end

---Create a buffer clipboard
---@param opts? { log: (Logger|boolean)? }
---@return Clipboard
function M.new(opts)
	opts = opts or {}
	local log = require("lib.log")

	local logger
	if opts.log then
		logger = type(opts.log) == "boolean" and log or opts.log
	else
		logger = opts.log.null
	end

	return Clipboard:new({ logger = logger })
end

return M
