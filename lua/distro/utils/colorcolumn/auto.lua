local M = {}

---@class ColorColumnOptions
local config = {
	value = "+1",
}

---Sync the color column state (autocmd-compatible)
---@param opts? { buf: integer?, force: boolean? }
function M.sync(opts)
	opts = opts or {}
	local buf = opts.buf or 0

	local textw = vim.bo[buf].textwidth
	if textw == 0 then
		vim.opt_local.colorcolumn = ""
	else
		vim.opt_local.colorcolumn = config.value
	end
end

function M.sync_all_buffers()
	for _, buf in ipairs(vim.api.nvim_list_bufs()) do
		if vim.api.nvim_buf_is_loaded(buf) then
			M.sync { buf = buf }
		end
	end
end

---@param o? ColorColumnOptions
function M.setup(o)
	config = vim.tbl_deep_extend("force", config, o or {})
	M.sync_all_buffers()
end

return M
