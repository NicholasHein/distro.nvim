local M = {}

---@class SplitTerminalConfig
local config = {
	win_options = {
		list = false,
		number = false,
		showbreak = false,
	},
	split_commands = {
		split = "split",
		vsplit = "vsplit",
		tab_split = "tab split",
	},
}

---@class SplitTerminalSpawnOpts
---@field cmd? string|string[]
---@field bufnr? integer
---@field clear_env? boolean
---@field cwd? string
---@field env? table<string, string>
---@field on_exit? fun(job: number, exit_code: number, event: string)
---@field on_stdout? fun(job: number, data: string[], event: string)
---@field on_stderr? fun(job: number, data: string[], event: string)
---@field stderr_buffered? boolean
---@field stdout_buffered? boolean
---@field stdin? string
---
---See :h jobstart-options

local function __spawn(cmd, opts)
	local init_cmd = { vim.o.shell, "-i" }

	local chan_id
	if vim.tbl_isempty(opts) then
		chan_id = vim.fn.termopen(init_cmd)
	else
		chan_id = vim.fn.termopen(init_cmd, opts)
	end

	if chan_id == 0 then
		error("split-terminal: invalid arguments")
	elseif chan_id == -1 then
		error(("split-terminal: '%s' is not executable"):format(cmd))
	end

	if not vim.tbl_isempty(cmd or {}) then
		cmd = table.concat(type(cmd) == "table" and cmd or { cmd }, " ")
		vim.defer_fn(function()
			vim.fn.chansend(chan_id, ("%s\r"):format(cmd))
		end, 300)
	end
	return chan_id
end

---@param opts? SplitTerminalSpawnOpts
---@return integer channel_id
function M.spawn(opts)
	opts = opts or {}

	local cmd = opts.cmd
	local bufnr = opts.bufnr or vim.api.nvim_create_buf(false, false)

	opts.cmd = nil
	opts.bufnr = nil

	for name, value in pairs(config.win_options) do
		vim.wo[name] = value
	end

	vim.api.nvim_buf_call(bufnr, function() __spawn(cmd, opts) end)
	vim.api.nvim_win_set_buf(0, bufnr)
end

---@param opts? SplitTerminalSpawnOpts
function M.split(opts)
	vim.cmd(config.split_commands.split)
	M.spawn(opts)
end

---@param opts? SplitTerminalSpawnOpts
function M.vsplit(opts)
	vim.cmd(config.split_commands.vsplit)
	M.spawn(opts)
end

---@param opts? SplitTerminalSpawnOpts
function M.tabsplit(opts)
	vim.cmd(config.split_commands.tab_split)
	M.spawn(opts)
end

---@param opts? SplitTerminalConfig
function M.setup(opts)
	config = vim.tbl_deep_extend("force", config, opts or {})

	local function cmd_handler(fn)
		return function(o)
			fn({ cmd = o.fargs })
		end
	end

	local cmds = {
		Term = {
			fn = M.split,
			desc = "terminal: open in split",
		},
		VTerm = {
			fn = M.vsplit,
			desc = "terminal: open in vsplit",
		},
		TTerm = {
			fn = M.tabsplit,
			desc = "terminal: open in tab split",
		},
	}

	for name, o in pairs(cmds) do
		vim.api.nvim_create_user_command(name, cmd_handler(o.fn), {
			nargs = "*",
			complete = "shellcmd",
			desc = o.desc,
			force = true,
		})
	end
end

return M
