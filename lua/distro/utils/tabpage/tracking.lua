local M = {}

local stack = {}

---@class TabpageTrackingConfig
local config = {
	history_size = 30,
}

---Add current tabpage to history
function M.push_current()
	table.insert(stack, vim.api.nvim_get_current_tabpage())
	if #stack >= config.history_size then
		stack = vim.list_slice(stack, #stack - config.history_size + 1)
	end
end

---Remove the latest tabpage from history and return it
---@return integer?
function M.pop()
	local tab
	repeat
		tab = table.remove(stack)
	until tab == nil or vim.api.nvim_tabpage_is_valid(tab)
	return tab
end

---Remove the latest non-current tabpage from history and go to it
---@return integer?
function M.pop_and_goto()
	local tab
	repeat
		tab = M.pop()
	until tab == nil or tab ~= vim.api.nvim_get_current_tabpage()

	if tab then
		vim.api.nvim_set_current_tabpage(tab)
	end
	return tab
end

---@param o? TabpageTrackingConfig
function M.setup(o)
	config = vim.tbl_deep_extend("force", config, o or {})
	stack = {}
end

return M
