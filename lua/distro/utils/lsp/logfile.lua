local M = {}

local uv = vim.loop

local fspoll = nil

---@class LspLogFileConfig
local config = {
	max_size = 64 * 1024 * 1024, -- 64M
	truncate_size = 1 * 1024 * 1024, -- 1M
	interval = 10 * 60 * 1000, -- 10 minutes
}

---@param bytes integer
---@return string
local function to_pretty_size(bytes)
	local order_unit_labels = {
		[1] = "B",
		[2] = "KiB",
		[3] = "MiB",
		[4] = "GiB",
	}

	local order
	local div = 1
	for i = 1, #order_unit_labels do
		order = i
		if div > bytes then break end
		div = math.pow(2, 10 * order)
	end

	return ("%u %s"):format(bytes / div, order_unit_labels[order])
end

---@param logfile? string
---@return integer
local function check_size(logfile)
	logfile = logfile or require("vim.lsp.log").get_filename()

	local stat = uv.fs_stat(logfile)
	assert(stat)
	return stat.size
end

---@param truncsize integer
---@param logfile? string
---@param currentsize? integer
---@return integer
local function limit_size(truncsize, logfile, currentsize)
	logfile = logfile or require("vim.lsp.log").get_filename()
	currentsize = currentsize or check_size(logfile)

	local tmpfd, tmppath = uv.fs_mkstemp("nvim_lsp_log_XXXXXX")
	assert(tmpfd)

	local logfd = uv.fs_open(logfile, "r", tonumber("644", 8))
	assert(logfd)

	local finalsize = uv.fs_sendfile(tmpfd, logfd, currentsize - truncsize, truncsize)
	assert(finalsize)

	local ret = uv.fs_rename(tmppath, logfile)
	assert(ret)
	return finalsize
end

local function stop_monitor()
	assert(fspoll ~= nil)
	local ret = fspoll:stop()
	assert(ret)
	fspoll = nil
end

---@param interval integer Milliseconds
---@param truncsize integer
---@param callback? fun(info: { path: string, size_prev: integer, size_now: integer })
---@param logfile? string
local function start_monitor(interval, truncsize, callback, logfile)
	logfile = logfile or require("vim.lsp.log").get_filename()
	if fspoll ~= nil then
		stop_monitor()
	end

	fspoll = uv.new_fs_poll()
	assert(fspoll)

	local ret = fspoll:start(logfile, interval, function(err, _, statnow)
		if err then
			vim.notify_once(vim.log.levels.ERROR, "Error monitoring log file: " .. err,
				{ title = "lsp-logfile" })
			stop_monitor()
			return
		end

		local newsize = limit_size(truncsize, logfile, statnow.size)
		if callback then
			callback({
				path = logfile,
				size_prev = statnow.size,
				size_now = newsize,
			})
		end
	end)
	assert(ret)
end

---@param o? LspLogFileConfig
function M.setup(o)
	config = vim.tbl_deep_extend("force", config, o or {})

	if config.max_size < config.truncate_size then
		vim.notify(vim.log.levels.ERROR, "max_size should be greater than or equal to truncate_size",
			{ title = "lsp-logfile" })
		return
	end

	local aug = vim.api.nvim_create_augroup("LspLogFile", {})
	vim.api.nvim_create_autocmd("LspAttach", {
		group = aug,
		once = true,
		desc = "lsp: limit log file size",
		callback = function()
			M.start_monitor(config.interval, function(info)
				vim.schedule(function()
					vim.notify_once(vim.log.levels.INFO,
						("LSP log file truncated to %s from %s"):format(
							to_pretty_size(info.size_now),
							to_pretty_size(info.size_prev)),
						{ title = "lsp-logfile" })
				end)
			end)
		end
	})
end

---@param interval? integer Milliseconds
---@param callback? fun(info: { path: string, size_prev: integer, size_now: integer })
---@param truncsize? integer
function M.start_monitor(interval, callback, truncsize)
	interval = interval or config.interval
	truncsize = truncsize or config.truncate_size
	return start_monitor(interval, truncsize, callback)
end

function M.stop_monitor()
	return stop_monitor()
end

---@return integer
function M.check_size()
	return check_size()
end

---@param truncsize integer
---@return integer
function M.limit_size(truncsize)
	return limit_size(truncsize)
end

return M
