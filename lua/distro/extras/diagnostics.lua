local M = {}

---@class distro.extras.DiagnosticsConfig
local config = {}

---@param opts? distro.extras.DiagnosticsConfig
function M.setup(opts)
	config = vim.tbl_deep_extend("force", config, opts or {})

	local subcommands = {
		show = {
			fn = M._command_show,
		},
		hide = {
			fn = M._command_hide,
		},
	}

	vim.api.nvim_create_user_command("Diag", function(cmdinfo)
		if #cmdinfo.fargs ~= 1 then
			vim.notify("diagnostics: expected `show`/`hide`", vim.log.levels.ERROR)
			return
		end

		local subcmd = subcommands[cmdinfo.fargs[1]]
		if not subcmd then
			vim.notify("diagnostics: unknown subcommand", vim.log.levels.ERROR)
			return
		end

		subcmd.fn(cmdinfo)
	end, {
		force = true,
		nargs = "?",
		bang = true,
		desc = "diagnostics: enable/disable diagnostics",
		complete = function(_, cmdline, cursorpos)
			local cmd = vim.api.nvim_parse_cmd(string.sub(cmdline, 0, cursorpos), {})
			if #cmd.args == 0 then
				return vim.tbl_keys(subcommands)
			else
				return {}
			end
		end,
	})
end

---@private
function M._command_show(o)
	---@type integer | nil
	local bufnr = 0
	if o.bang then bufnr = nil end

	vim.diagnostic.show(nil, bufnr)
end

---@private
function M._command_hide(o)
	---@type integer | nil
	local bufnr = 0
	if o.bang then bufnr = nil end

	vim.diagnostic.hide(nil, bufnr)
end

return M
