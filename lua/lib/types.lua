require("lib.type.string")
require("lib.type.table")
require("lib.type.io")

local M = {}

---Check if an object is callable
---@param o any
---@return boolean
function M.is_callable(o)
	return vim.is_callable(o)
end

---@class InspectOpts
---@field depth? integer Max depth
---@field newline? string Newline string (default: "\n")
---@field indent? string Indentation string (default: "  ")

---Format an object into a string
---@param o any
---@param opts InspectOpts
---@return string
function M.format(o, opts)
	return vim.inspect(o, opts)
end

---Pretty-print arguments
---@param ... any
---@return any
function M.pprint(...)
	return vim.print(...)
end

---@class FuncTypedParamSpec<T>: { [1]: T, [2]: `T`|string|string[], [3]: boolean?}
---@class FuncCheckedParamSpec<T>: { [1]: T, [2]: (fun(arg: any): boolean, string?), [3]: string? }

---Validate a function's parameter types
---@param spec table<string, FuncTypedParamSpec|FuncCheckedParamSpec>
---@return nil
function M.validate_params(spec)
	return vim.validate(spec)
end

return M
