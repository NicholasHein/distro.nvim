local Win = require("lib.ui.win")
local Buf = require("lib.ui.buf")
local Object = require("lib.object")
require("lib.type.table")

local M = {}

---@class LspFloatingPreview
---@field id string
---@field win Win
---@field buf Buf
local LspFloatingPreview = Object.class {}

local cache = {}

---Create a floating preview
---@param id string
---@param contents string[]
---@param syntax string
---@param opts? table
---@return LspFloatingPreview
function M.fpreview(id, contents, syntax, opts)
	if cache[id] then
		cache[id]:close()
	end

	local bufnr, winnr = vim.lsp.util.open_floating_preview(contents, syntax, table.override({
		width = 50,
		height = 10,
		wrap = false,
		max_width = 80,
		max_height = 30,
		close_events = { "CursorMoved", "CursorMovedI" },
	}, opts or {}))

	local prev = Object.new(LspFloatingPreview, {
		id = id,
		win = Win:from_id(winnr),
		buf = Buf:from_id(bufnr),
	})
	cache[id] = prev
	return prev
end

---Find a floating preview by id
---@param id string
---@return LspFloatingPreview?
function M.fpreview_by_id(id)
	return cache[id]
end

---Close the floating preview
function LspFloatingPreview:close()
	cache[self.id] = nil
	self.win:close(true)
	self.buf:delete({ force = true })
end

return M
