local M = {}

---@class TermCodeRepr: string

---Wait for and return a char from input
---@return TermCodeRepr
function M.getc()
	return vim.fn.nr2char(vim.fn.getchar())
end

---Translate terminal codes to an internal representation
---@param str string
---@return TermCodeRepr
function M.to_termcode_repr(str)
	return vim.api.nvim_replace_termcodes(str, true, true, true)
end

return M
