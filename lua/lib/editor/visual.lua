local M = {}

---@enum VisualMode
local modes = {
	linewise = "V",
	blockwise = vim.api.nvim_replace_termcodes("<C-v>", true, true, true),
	charwise = "v",
}

M.modes = modes

local mode_list = vim.tbl_values(modes)

M.mode_list = mode_list

---Get the current visual mode
---@return VisualMode?
function M.mode()
	local mode = vim.fn.mode()
	return vim.tbl_contains(mode_list, mode) and mode or nil
end

---@class VRange
---@field start { line: integer, col: integer }
---@field stop { line: integer, col: integer }

---Get the current selection
---@return VRange
function M.inclusive_selection()
	local _, l_start, c_start, _ = vim.fn.getpos("'<")
	local _, l_stop, c_stop, _ = vim.fn.getpos("'>")
	return {
		start = { line = l_start, col = c_start },
		stop = { line = l_stop, col = c_stop },
	}
end

---Get the current selection, adjusted for exclusivity
---@return VRange
function M.selection()
	local range = M.inclusive_selection()

	if vim.opt.selection:get() ~= "inclusive" and
			(range.start.col ~= range.stop.col or range.start.line ~= range.stop.line) then
		range.stop.col = range.stop.col - 1
	end
	return range
end

function M.selected_lines()
	local sel = M.selection()

	local lines = vim.fn.getline(sel.start.line, sel.stop.line)
	if #lines == 0 then
		return lines
	end

	local vmode = vim.fn.visualmode()
	if vmode == modes.charwise then
		-- Chop beginning of first line and end of last line
		lines[1] = string.sub(lines[1], sel.start.col)
		lines[#lines] = string.sub(lines[#lines], 1, sel.stop.col)
	elseif vmode == modes.blockwise then
		-- Nothing to do
	elseif vmode == modes.blockwise then
		-- Chop beginning and end of each line
		for i = 1, #lines do
			lines[i] = string.sub(lines[i], sel.start.col, sel.stop.col)
		end
	else
		lines = {}
	end
	return lines
end

function M.expr_surround_selection(left, right)
	right = right or left
	return [[<Esc>`>a]] .. right .. [[<Esc>`<i]] .. left .. [[<Esc>lv`>]] .. string.len(right) .. [[l]]
end

return M
