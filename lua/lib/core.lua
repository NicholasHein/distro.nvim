local M = {}

local log = require("lib.log.builtin"):new { name = "core" }

M.logger = log

---@class TraceOptions: table
---@field level? integer
---@field what? infowhat
---@field excluded_sources string[]

---Return a stack trace
---@param opts? TraceOptions
function M.trace(opts)
	opts = vim.tbl_deep_extend("force", {
		level = 2,
		what = "Sln",
		excluded_sources = { "lib/core.lua" },
	}, opts or {})
	opts = opts or {}

	local trace = {}
	local level = opts.level
	while true do
		local info = debug.getinfo(level, "Sln")
		if not info then break end

		local exclude = false
		for _, exsrc in pairs(opts.excluded_sources) do
			if info.source:find(exsrc) then
				exclude = true
			end
		end

		if info.what ~= "C" and not exclude then
			local source = info.source:sub(2)
			source = vim.fn.fnamemodify(source, ":p:~:.")

			table.insert(trace, {
				source = source,
				name = info.name,
				line = info.currentline,
			})
		end
		level = level + 1
	end
	return trace
end

---@class TryOptions: TraceOptions
---@field msg? string
---@field ignore? boolean
---@field on_error? fun(msg: string)

---Try to execute something safely
---@generic R
---@param fn fun(): R?
---@param opts? TryOptions
---@return R?
function M.try(fn, opts)
	opts = opts or {}

	local function error_handler(err)
		local msg = {}
		if opts.msg then
			table.insert(msg, opts.msg .. "\n")
		end
		table.insert(msg, err .. "\n")

		local trace = M.trace(opts)
		if #trace > 0 then
			table.insert(msg, "# stacktrace:")
		end

		for _, frame in ipairs(trace) do
			local line = ("    - %s:%u"):format(frame.source, frame.line)
			if frame.name then
				line = line .. (" _in_ **%s**"):format(frame.name)
			end
			table.insert(msg, line)
		end

		-- Replace tabs with spaces because they don't look right in `:echoerr`
		local full_message = table.concat(msg, "\n"):gsub("\t", (" "):rep(4))
		if opts.on_error then
			opts.on_error(full_message)
		else
			vim.schedule(function()
				log:err(full_message)
			end)
		end
	end

	local function noop(_)
	end

	local ok, result = xpcall(fn, opts.ignore and noop or error_handler)
	return ok and result or nil
end

---Try to load a lua module
---@generic M
---@param mod string
---@param opts? TryOptions
---@return M?
function M.try_require(mod, opts)
	opts = opts or {}
	opts.ignore = opts.ignore ~= nil and opts.ignore or true

	return M.try(function()
		return require(mod)
	end, opts)
end

return M
