local Object = require("lib.object")

---@class Result<T, E>: { inner: T|E }
local Result = Object.class({}, { new_func = false })

---@generic T, E
---@param r Result<T, E>
---@return T|E
local function to_inner(r)
	return r.inner
end

---Return res if Ok, otherwise return self (Err)
---@generic S: Result, R: Result
---@param self S
---@param res R
---@return S|R
---@diagnostic disable-next-line: unused-local
function Result.ok_and(self, res) error("virtual function") end

---Return res if Err, otherwise return self (Ok)
---@generic S: Result, R: Result
---@param self S
---@param res R
---@return S|R
---@diagnostic disable-next-line: unused-local
function Result.ok_or(self, res) error("virtual function") end

---Return fn() if Ok, otherwise return self (Err)
---@generic T, U, E
---@param self Result<T, E>
---@param fn fun(x: T): Result<U, E>
---@return Result res Result<U, E>
---@diagnostic disable-next-line: unused-local
function Result.and_then(self, fn) error("virtual function") end

---Return fn() if Err, otherwise return self (Ok)
---@generic T, E, F
---@param self Result<T, E>
---@param fn fun(x: E): Result<T, F>
---@return Result res Result<T, F>
---@diagnostic disable-next-line: unused-local
function Result.or_else(self, fn) error("virtual function") end

---Return inner error if Err
---@generic T, E
---@param self Result<T, E>
---@return E?
---@diagnostic disable-next-line: unused-local
function Result.err(self) error("virtual function") end

---Return true if Err
---@generic S: Result
---@param self S
---@return boolean
---@diagnostic disable-next-line: unused-local
function Result.is_err(self) error("virtual function") end

---Return true if Ok
---@generic S: Result
---@param self S
---@return boolean
---@diagnostic disable-next-line: unused-local
function Result.is_ok(self) error("virtual function") end

---Map an Ok value
---@generic T, U, E
---@param self Result<T, E>
---@param fn fun(x: T): U
---@return Result res Result<U, E>
---@diagnostic disable-next-line: unused-local
function Result.map_ok(self, fn) error("virtual function") end

---Map an Err value
---@generic T, E, F
---@param self Result<T, E>
---@param fn fun(x: E): F
---@return Result res Result<T, F>
---@diagnostic disable-next-line: unused-local
function Result.map_err(self, fn) error("virtual function") end

---Return a default if Err or applies a function on the Ok value
---@generic T, U, E
---@param self Result<T, E>
---@param default U
---@param fn fun(x: T): U
---@return U
---@diagnostic disable-next-line: unused-local
function Result.map_or(self, default, fn) error("virtual function") end

---Return result of default function if Err or applies a function on the Ok value
---@generic T, U, E
---@param self Result<T, E>
---@param def_fn fun(x: E): U
---@param fn fun(x: T): U
---@return U
---@diagnostic disable-next-line: unused-local
function Result.map_or_else(self, def_fn, fn) error("virtual function") end

---Return value if Ok, otherwise the default
---@generic T, E
---@param self Result<T, E>
---@param default T
---@return T
---@diagnostic disable-next-line: unused-local
function Result.unwrap_or(self, default) error("virtual function") end

---Return value if Ok, otherwise the result of the default function
---@generic T, E
---@param self Result<T, E>
---@param def_fn fun(x: E): T
---@return T
---@diagnostic disable-next-line: unused-local
function Result.unwrap_or_else(self, def_fn) error("virtual function") end

---@class Ok<T>: Result<T, any>
local Ok = Object.class({}, { parents = { Result }, new_func = false })

---Ok
---@generic T
---@param t T
---@return Ok res Ok<T>
function Result.Ok(t)
	return Object.new(Ok, { inner = t })
end

---Return res if Ok, otherwise return self (Err)
---@generic S: Ok, R: Result
---@param self S
---@param res R
---@return R
---@diagnostic disable-next-line: unused-local
function Ok.ok_and(self, res) return res end

---Return res if Err, otherwise return self (Ok)
---@generic S: Ok, R: Result
---@param self S
---@param res R
---@return S
---@diagnostic disable-next-line: unused-local
function Ok.ok_or(self, res) return self end

---Return fn() if Ok, otherwise return self (Err)
---@generic T, U, E
---@param self Ok<T>
---@param fn fun(x: T): Result<U, E>
---@return Result res Result<U, E>
function Ok.and_then(self, fn) return fn(to_inner(self)) end

---Return fn() if Err, otherwise return self (Ok)
---@generic T, E, F
---@param self Ok<T>
---@param fn fun(x: E): Result<T, F>
---@return Ok res Ok<T>
---@diagnostic disable-next-line: unused-local
function Ok.or_else(self, fn) return self end

---Return inner error if Err
---@generic S: Ok
---@param self S
---@return nil
---@diagnostic disable-next-line: unused-local
function Ok.err(self) return nil end

---Return true if Err
---@generic S: Ok
---@param self S
---@return boolean
---@diagnostic disable-next-line: unused-local
function Ok.is_err(self) return false end

---Return true if Ok
---@generic S: Ok
---@param self S
---@return boolean
---@diagnostic disable-next-line: unused-local
function Ok.is_ok(self) return true end

---Map an Ok value
---@generic T, U, E
---@param self Ok<T, E>
---@param fn fun(x: T): U
---@return Ok res Ok<U>
function Ok.map_ok(self, fn) return Result.Ok(fn(to_inner(self))) end

---Map an Err value
---@generic S: Ok, E, F
---@param self S
---@param fn fun(x: E): F
---@return S res Ok<T>
---@diagnostic disable-next-line: unused-local
function Ok.map_err(self, fn) return self end

---Return a default if Err or applies a function on the Ok value
---@generic T, U, E
---@param self Ok<T>
---@param default U
---@param fn fun(x: T): U
---@return U
---@diagnostic disable-next-line: unused-local
function Ok.map_or(self, default, fn) return fn(to_inner(self)) end

---Return result of default function if Err or applies a function on the Ok value
---@generic T, U, E
---@param self Ok<T>
---@param def_fn fun(x: E): U
---@param fn fun(x: T): U
---@return U
---@diagnostic disable-next-line: unused-local
function Ok.map_or_else(self, def_fn, fn) return fn(to_inner(self)) end

---Return value if Ok, otherwise the default
---@generic T, E
---@param self Ok<T>
---@param default T
---@return T
---@diagnostic disable-next-line: unused-local
function Ok.unwrap_or(self, default) return to_inner(self) end

---Return value if Ok, otherwise the result of the default function
---@generic T, E
---@param self Ok<T>
---@param def_fn fun(x: E): T
---@return T
---@diagnostic disable-next-line: unused-local
function Ok.unwrap_or_else(self, def_fn) return to_inner(self) end

---@class Err<E>: Result<any, E>
local Err = Object.class({}, { parents = { Result }, new_func = false })

---Err
---@generic E
---@param e E
---@return Err res Err<E>
function Result.Err(e)
	return Object.new(Err, { inner = e })
end

---Return res if Ok, otherwise return self (Err)
---@generic S: Err, R: Result
---@param self S
---@param res R
---@return S
---@diagnostic disable-next-line: unused-local
function Err.ok_and(self, res) return self end

---Return res if Err, otherwise return self (Ok)
---@generic S: Err, R: Result
---@param self S
---@param res R
---@return R
---@diagnostic disable-next-line: unused-local
function Err.ok_or(self, res) return res end

---Return fn() if Ok, otherwise return self (Err)
---@generic T, U, E
---@param self Err<E>
---@param fn fun(x: T): Result<U, E>
---@return Err err Err<E>
---@diagnostic disable-next-line: unused-local
function Err.and_then(self, fn) return self end

---Return fn() if Err, otherwise return self (Ok)
---@generic T, E, F
---@param self Err<T>
---@param fn fun(x: E): Result<T, F>
---@return Result res Result<T, F>
---@diagnostic disable-next-line: unused-local
function Err.or_else(self, fn) return fn(to_inner(self)) end

---Return inner error if Err
---@generic E
---@param self Err<E>
---@return E
---@diagnostic disable-next-line: unused-local
function Err.err(self) return to_inner(self) end

---Return true if Err
---@generic S: Err
---@param self S
---@return boolean
---@diagnostic disable-next-line: unused-local
function Err.is_err(self) return true end

---Return true if Ok
---@generic S: Err
---@param self S
---@return boolean
---@diagnostic disable-next-line: unused-local
function Err.is_ok(self) return false end

---Map an Ok value
---@generic T, U, E
---@param self Err<E>
---@param fn fun(x: T): U
---@return Err res Err<E>
---@diagnostic disable-next-line: unused-local
function Err.map_ok(self, fn) return self end

---Map an Err value
---@generic T, E, F
---@param self Err<E>
---@param fn fun(x: E): F
---@return Err res Err<F>
---@diagnostic disable-next-line: unused-local
function Err.map_err(self, fn) return Result.Err(fn(to_inner(self))) end

---Return a default if Err or applies a function on the Ok value
---@generic T, U, E
---@param self Err<E>
---@param default U
---@param fn fun(x: T): U
---@return U
---@diagnostic disable-next-line: unused-local
function Err.map_or(self, default, fn) return default end

---Return result of default function if Err or applies a function on the Ok value
---@generic T, U, E
---@param self Err<E>
---@param def_fn fun(x: E): U
---@param fn fun(x: T): U
---@return U
---@diagnostic disable-next-line: unused-local
function Err.map_or_else(self, def_fn, fn) return def_fn(to_inner(self)) end

---Return value if Ok, otherwise the default
---@generic S: Err, T
---@param self S
---@param default T
---@return T
---@diagnostic disable-next-line: unused-local
function Err.unwrap_or(self, default) return default end

---Return value if Ok, otherwise the result of the default function
---@generic T, E
---@param self Err<E>
---@param def_fn fun(x: E): T
---@return T
---@diagnostic disable-next-line: unused-local
function Err.unwrap_or_else(self, def_fn) return def_fn(to_inner(self)) end

return Result
