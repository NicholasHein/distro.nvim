---Check if the string has a matching prefix
---@param str string
---@param prefix string
---@return boolean
function string.startswith(str, prefix)
	return vim.startswith(str, prefix)
end

---Check if the string has a matching suffix
---@param str string
---@param suffix string
---@return boolean
function string.endswith(str, suffix)
	return vim.endswith(str, suffix)
end

---@class StringSplitOpts
---@field plain? boolean
---@field trimempty? boolean

---Split a string at each separator
---@param str string
---@param sep string
---@param opts? StringSplitOpts
---@return fun():string|nil
function string.gsplit(str, sep, opts)
	return vim.gsplit(str, sep, opts)
end

---Escape magic chars in lua-patterns.
---@param str string
---@return string
function string.pesc(str)
	return vim.pesc(str)
end

---Split a string at each separator
---@param str string
---@param sep string
---@param opts? StringSplitOpts
---@return string[]
function string.split(str, sep, opts)
	return vim.split(str, sep, opts)
end

---Trim whitespace from both sides of a string
---@param str string
---@return string
function string.trim(str)
	return vim.trim(str)
end

return string
