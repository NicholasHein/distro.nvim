local uv = vim.loop

---@class StatTable
---@field dev integer
---@field mode integer
---@field nlink integer
---@field uid integer
---@field gid integer
---@field rdev integer
---@field ino integer
---@field size integer
---@field blksize integer
---@field blocks integer
---@field flags integer
---@field gen integer
---@field atime { sec: integer, nsec: integer }
---@field mtime { sec: integer, nsec: integer }
---@field ctime { sec: integer, nsec: integer }
---@field birthtime { sec: integer, nsec: integer }
---@field type string

---Stat the path
---@param path string
---@return StatTable?, string?, string?
function io.stat(path)
	return uv.fs_stat(path)
end

return io
