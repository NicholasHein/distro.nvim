---@diagnostic disable-next-line: deprecated
unpack = unpack or table.unpack

---Recursive comparison operation (unless eq meta-method is defined for both)
---@param tbl table
---@param other table
---@return boolean
function table.requals(tbl, other)
	return vim.deep_equal(tbl, other)
end

---Recursive comparison to check if a table is a subset of another table
---@param tbl table
---@param other table
---@return boolean
function table.is_subset(tbl, other)
	local function is_subset(a, b)
		if a == b then
			return true
		elseif type(a) ~= type(b) or type(a) ~= "table" then
			return false
		end

		for k, v in pairs(a) do
			if not is_subset(v, b[k]) then
				return false
			end
		end
		return true
	end

	return is_subset(tbl, other)
end

---Recursive comparison to check if a table is a superset of another table
---@param tbl table
---@param other table
---@return boolean
function table.is_superset(tbl, other)
	return table.is_subset(other, tbl)
end

---Recursively clones the table
---@param tbl table
---@return table
function table.deep_copy(tbl)
	return vim.deepcopy(tbl)
end

---Extend a table
---@param tbl table
---@param behavior string
---|"'error'" # Raise an error for duplicate keys
---|"'keep'" # Use values from the original table (tbl)
---|"'force'" # Use values from the other tables (...)
---@param ... table
---@return table
function table.merge_pairs(tbl, behavior, ...)
	return vim.tbl_extend(behavior, tbl, ...)
end

---Recursively extend a table
---@param tbl table
---@param behavior string
---|"'error'" # Raise an error for duplicate keys
---|"'keep'" # Use values from the original table (tbl)
---|"'force'" # Use values from the other tables (...)
---@param ... table
---@return table
function table.deep_merge(tbl, behavior, ...)
	return vim.tbl_deep_extend(behavior, tbl, ...)
end

---Merge tables on top of the current table
---@param tbl table
---@param ... table
---@return table
function table.merge_over(tbl, ...)
	return table.deep_merge(tbl, "force", ...)
end

table.override = table.merge_over

---Merge tables below the current table
---@param tbl table
---@param ... table
---@return table
function table.merge_under(tbl, ...)
	return table.deep_merge(tbl, "keep", ...)
end

table.underride = table.merge_under

---Create a table with default values for non-existant members
---@param default_fn? fun(key: any): any
---@return table
function table.new_default(default_fn)
	return vim.defaulttable(default_fn)
end

---Iterate over a table sorted by its keys
---@param tbl table<any, any>
---@return fun(): any, any
function table.spairs(tbl)
	return vim.spairs(tbl) --[[@as fun()]]
end

---Return a table that maps existing values to true
---@generic K, V
---@param tbl table<K, V>
---@return table<V, boolean>
function table.to_value_lookup(tbl)
	local result = {}
	for _, val in pairs(tbl) do
		result[val] = true
	end
	return result
end

---Add a reverse-lookup to a table
---@param tbl table
---@return table
function table.add_mirrored(tbl)
	return vim.tbl_add_reverse_lookup(tbl)
end

---Return the number of non-nil values in a table
---@param tbl table
---@return integer
function table.count(tbl)
	return vim.tbl_count(tbl)
end

---Filter a table with a predicate
---@generic V
---@param tbl table<any, V>
---@param fn fun(value: V): boolean
---@return V[]
function table.filter(tbl, fn)
	return vim.tbl_filter(fn, tbl)
end

---Apply a function on all values in a table
---@generic T
---@param tbl table<any, T>
---@param func fun(value: T): any
---@return table
function table.map(tbl, func)
	return vim.tbl_map(func, tbl)
end

---Flatten a list-like table
---@param tbl table
---@return table
function table.flatten(tbl)
	return vim.tbl_flatten(tbl)
end

---Index into a table using a string path
---@param tbl table
---@param ... string
---@return any?
function table.traverse(tbl, ...)
	return vim.tbl_get(tbl, ...)
end

---Check if the table is empty
---@param tbl table
---@return boolean
function table.is_empty(tbl)
	return vim.tbl_isempty(tbl)
end

do
	---@diagnostic disable-next-line: deprecated
	local islist = vim.islist or vim.tbl_islist

	---Check if the table is an array/list
	---@param tbl table
	---@return boolean
	function table.is_list(tbl)
		return islist(tbl)
	end
end

---Get a list of the keys in a table
---@generic T: table
---@param tbl table<T, any>
---@return T[]
function table.keys(tbl)
	return vim.tbl_keys(tbl)
end

---Get a list of the keys in a table
---@generic T
---@param tbl table<any, T>
---@return T[]
function table.values(tbl)
	return vim.tbl_values(tbl)
end

return table
