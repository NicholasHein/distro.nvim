---Clamp a value within a range
---@param val number
---@param min number
---@param max number
---@return number
function math.clamp(val, min, max)
	return math.min(math.max(val, min), max)
end

---Get the sign of a number (+1: positive, -1: negative, nil: 0)
---@param val number
---@return number?
function math.sign(val)
	return val ~= 0 and math.floor(val / math.abs(val)) or nil
end

---@param val integer
---@param bits integer
---@return integer
function math.shr(val, bits)
	return math.floor(val / 2 ^ bits)
end

---@param val integer
---@param bits integer
---@return integer
function math.shl(val, bits)
	return math.floor(val * 2 ^ bits)
end

---Return an list/iterator over a range of numbers
---@param to number
---@param from? number (default: 0)
---@param increment? number (default: [+-]1)
---@return table
function math.range(to, from, increment)
	from = from or 0
	increment = increment or math.sign(to - from) or 1

	local function at(i)
		assert(i >= 0)

		local val = from + (increment * i)
		if (increment > 0 and val <= to) or (increment < 0 and val >= to) or increment == 0 then
			return val
		end
		return nil
	end

	local function iter(_, i)
		local val = at(i)
		if val then
			return i + 1, val
		end
		return nil
	end

	local function to_array()
		local ret = {}
		for i = from, to, increment do
			table.insert(ret, i)
		end
		return ret
	end

	local ret = {}
	local mt = { __name = "range" }
	-- __len only works in 5.2
	if vim.version.lt(vim.version.parse(_VERSION), {5, 2}) then
		ret = to_array()
		mt.__call = function(t)
			return t
		end
	else
		mt.__len = function(_)
			return math.floor((to - from) / increment)
		end
		mt.__index = function(_, n)
			return at(n - 1)
		end
		mt.__pairs = function(t)
			return iter, t, 0
		end
		mt.__call = function(_)
			return to_array()
		end
	end
	return setmetatable(ret, mt)
end

return math
