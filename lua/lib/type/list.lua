local table = require("lib.type.table")

---@class list<T>: table<integer, T>
list = {}

---Convert a value into a list
---@generic T
---@param val T|T[]
---@return T[]
function list.from(val)
	if type(val) == "table" and table.is_list(val) then
		return val
	else
		return { val }
	end
end

table.as_list = list.from

---Extend a list
---@generic L: list
---@param list L
---@param src L
---@param start? integer
---@param finish? integer
---@return L
function list.extend(list, src, start, finish)
	return vim.list_extend(list, src, start, finish)
end

table.iextend = list.extend

---Extend list with only the unique values of another list
---@generic L: list
---@param list L
---@param src L
---@param start? integer
---@param finish? integer
function list.join(list, src, start, finish)
	local val_lookup = table.to_value_lookup(list)
	for i = start or 1, finish or #src do
		local val = src[i]
		local has_duplicate = val_lookup[val]
		if not has_duplicate then
			table.insert(list, val)
			val_lookup[val] = true
		end
	end
end

---Create a copy of a range of a list
---@generic L: list
---@param list L
---@param start? integer
---@param finish? integer
---@return L
function list.slice(list, start, finish)
	return vim.list_slice(list, start, finish)
end

table.islice = list.slice

---Check if a list contains a value
---@generic L: list
---@param list L
---@param value any
---@return boolean
function list.contains(list, value)
	return vim.tbl_contains(list, value)
end

table.icontains = list.contains

return list
