local Logger = require("lib.log.logger")
local Object = require("lib.object")

---@class FileLogger: Logger
---@field path string
---@field file file*
---@field maxsize? integer
---@field on_create? fun(self: FileLogger)
local FileLogger = Object.class({ id = "file" }, { parents = { Logger } })

---@param path string
---@return integer?
function FileLogger.getsize(path)
	local size = vim.fn.getfsize(path)
	if size == -1 then
		return nil -- File does not exist
	elseif size == -2 then
		return math.huge -- Size too large to store in a vim "Number" type
	end
	assert(size >= 0)
	return size
end

---@param path string
---@return boolean
function FileLogger.fexists(path)
	local uv = vim.loop
	local ret = uv.fs_stat(path)
	return ret and true or false
end

---@param o table|string
---@return FileLogger?
---@return string? errmsg
function FileLogger:new(o)
	if type(o) == "string" then
		o = { path = o }
	end
	assert(o.path)

	if o.maxsize then
		local size = self.getsize(o.path)
		if size >= o.maxsize then
			return nil
		end
	end

	local created = not self.fexists(o.path)

	local file, err = io.open(o.path, "a+")
	if not file then
		return nil, err
	end

	created = created or (self.getsize(o.path) == 0)
	if created then
		file:write("# Time | Level | Message\n")
	end

	o.file = file
	local log = Object.new(self, o)
	if created then
		o.on_create(log)
	end
	return log
end

function FileLogger:format(lvl, fmt, ...)
	local msg = Logger.format(self, lvl, fmt, ...)

	local l = vim.log.levels
	local levels = {
		[l.TRACE] = "TRACE",
		[l.DEBUG] = "DEBUG",
		[l.INFO] = "INFO",
		[l.WARN] = "WARN",
		[l.ERROR] = "ERROR",
	}

	local lstr = levels[lvl] or "INVAL"
	local timestamp = os.date("%s")

	return ("%s | %s | %s\n"):format(timestamp, lstr, msg)
end

function FileLogger:write(_, msg)
	self.file:write(msg)
end

do
	local once_cache = {}
	function FileLogger:write_once(lvl, msg)
		-- Strip the timestamp
		local key = msg:gsub("^[^|]+ | (.+)", "%1")
		if not once_cache[key] then
			self:write(lvl, msg)
			once_cache[key] = true
		end
	end
end

return FileLogger
