local Object = require("lib.object")

---@alias LogLevel integer
local LogLevel = vim.log.levels


---@class Logger
---@field id? string
---@field name? string
---@field prefix? string|string[]
---@field child? Logger
---@field format? fun(self, lvl: LogLevel, fmt: string, ...): string
---@field filter? fun(self, lvl: LogLevel, msg: string): boolean
---@field write fun(self, lvl: LogLevel, msg: string)
---@field write_once fun(self, lvl: LogLevel, msg: string)
local Logger = Object.class {}

---Format a string for logging
---@param lvl LogLevel
---@param fmt string
---@param ... any
---@return string
---@diagnostic disable-next-line: unused-local
function Logger:format(lvl, fmt, ...)
	local msg = string.format(fmt, ...)

	---@type string[]
	local prefixes = {}
	if self.prefix == nil then
		prefixes = {}
	elseif type(self.prefix) == "string" then
		prefixes = { self.prefix --[[@as string]] }
	elseif type(self.prefix) == "table" then
		prefixes = self.prefix --[[@as (string[])]]
	end

	if #prefixes > 0 then
		msg = ("%s: %s"):format(table.concat(prefixes, ": "), msg)
	end
	if self.name then
		msg = ("[%s] %s"):format(self.name, msg)
	end
	return msg
end

---Filter messages
---@param lvl LogLevel
---@param msg string
---@return boolean
---@diagnostic disable-next-line: unused-local
function Logger:filter(lvl, msg)
	return lvl ~= vim.log.levels.DEBUG or vim.go.verbose > 0
end

---Log a message at a level
---@param lvl LogLevel
---@param msg string
---@diagnostic disable-next-line: unused-local
function Logger:write(lvl, msg)
	error("virtual function not implemented")
end

---Log a message at a level only one time (if supported)
---@param lvl LogLevel
---@param msg string
function Logger:write_once(lvl, msg)
	self:write(lvl, msg)
end

---Log a formatted message at a level
---@param lvl LogLevel
---@param fmt string
---@param ... any
function Logger:log(lvl, fmt, ...)
	local msg = self:format(lvl, fmt, ...)
	if not self:filter(lvl, msg) then
		return
	end

	self:write(lvl, msg)
	if self.child then
		self.child:log(lvl, fmt, ...)
	end
end

---Log a formatted message at a level only one time (if supported)
---@param lvl LogLevel
---@param fmt string
---@param ... any
function Logger:log_once(lvl, fmt, ...)
	local msg = self:format(lvl, fmt, ...)
	if not self:filter(lvl, msg) then
		return
	end

	self:write_once(lvl, msg)
	if self.child then
		self.child:log_once(lvl, fmt, ...)
	end
end

---Log an error message
---@param fmt string
---@param ... any
function Logger:err(fmt, ...) self:log(LogLevel.ERROR, fmt, ...) end

---Log a warning message
---@param fmt string
---@param ... any
function Logger:warn(fmt, ...) self:log(LogLevel.WARN, fmt, ...) end

---Log an informational message
---@param fmt string
---@param ... any
function Logger:info(fmt, ...) self:log(LogLevel.INFO, fmt, ...) end

---Log a debugging message
---@param fmt string
---@param ... any
function Logger:debug(fmt, ...) self:log(LogLevel.DEBUG, fmt, ...) end

---Log a tracing message
---@param fmt string
---@param ... any
function Logger:trace(fmt, ...) self:log(LogLevel.TRACE, fmt, ...) end

---Log an error message only one time
---@param fmt string
---@param ... any
function Logger:err_once(fmt, ...) self:log_once(LogLevel.ERROR, fmt, ...) end

---Log a warning message only one time
---@param fmt string
---@param ... any
function Logger:warn_once(fmt, ...) self:log_once(LogLevel.WARN, fmt, ...) end

---Log an informational message only one time
---@param fmt string
---@param ... any
function Logger:info_once(fmt, ...) self:log_once(LogLevel.INFO, fmt, ...) end

---Log a debugging message only one time
---@param fmt string
---@param ... any
function Logger:debug_once(fmt, ...) self:log_once(LogLevel.DEBUG, fmt, ...) end

---Log a tracing message only one time
---@param fmt string
---@param ... any
function Logger:trace_once(fmt, ...) self:log_once(LogLevel.TRACE, fmt, ...) end

return Logger
