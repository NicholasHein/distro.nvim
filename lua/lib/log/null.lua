local Logger = require("lib.log.logger")
local Object = require("lib.object")

---@class NullLogger: Logger
local NullLogger = Object.class({ id = "null" }, { parents = { Logger } })

function NullLogger:write(_, _) end

function NullLogger:write_once(_, _) end

return NullLogger
