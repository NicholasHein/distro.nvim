local Logger = require("lib.log.logger")
local Object = require("lib.object")

---@class Builtin: Logger
local Builtin = Object.class({ id = "builtin" }, { parents = { Logger } })

function Builtin:new(o)
	return Object.new(self, o)
end

function Builtin:write(lvl, msg)
	vim.notify(msg, lvl)
end

function Builtin:write_once(lvl, msg)
	vim.notify_once(msg, lvl)
end

---@param chunks { [1]: string, [2]: string? }[]
---@param opts? { history: boolean?, verbose: boolean? }
function Builtin:echo(chunks, opts)
	opts = opts or {}
	vim.api.nvim_echo(chunks, opts.history == true, { verbose = opts.verbose })
end

return Builtin
