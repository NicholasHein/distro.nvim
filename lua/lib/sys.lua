local Object = require("lib.object")
local M = {
	local_prefix = vim.env.HOME .. "/.local" --[[@as Path]],
}

---@class Path: string

---Build a path
---@param ... (Path|string|table)[]
---@return Path
function M.make_path(...)
	local components = vim.tbl_flatten({ ... })
	return table.concat(components, "/") --[[@as Path]]
end

---Find an executable in $PATH
---@param command_name string
---@return Path?
function M.find_exe_in_path(command_name)
	local path = vim.fn.exepath(command_name)
	if path == "" then return nil end
	return path --[[@as Path]]
end

---Check if an executable can be found in $PATH
---@param command_name string
---@return boolean
function M.is_exe_in_path(command_name)
	return M.find_exe_in_path(command_name) and true or false
end

---@class Command
---@field cmdline string|string[]
---@field keep_final_newline boolean
local Command = Object.class {
	cmdline = nil,
	keep_final_newline = false,
}

---@class CommandResult
---@field output string[]
---@field exit_code integer
local CommandResult = Object.class {
	output = nil,
	exit_code = nil,
}

---@param o? Command
---@return Command
function Command:new(o)
	return Object.new(self, o)
end

---@param o? CommandResult
---@return CommandResult
function CommandResult:new(o)
	return Object.new(self, o)
end

---Execute a system command
---@param stdin? string|string[]
---@return CommandResult
function Command:execute(stdin)
	local stdout = vim.fn.systemlist(self.cmdline, stdin, self.keep_final_newline)
	return CommandResult:new { output = stdout, exit_code = vim.v.shell_error }
end

---Check for a successful exit code
---@return boolean
function CommandResult:is_ok()
	return self.exit_code == 0
end

---Create a command from a command line
---@param ... string|string[] Command line
---@return Command
function M.command(...)
	return Command:new {
		cmdline = vim.tbl_flatten({ ... }),
		keep_final_newline = false,
	}
end

local user_full_name_cached = {
	uname = nil,
	full_name = nil,
}

---Get full name of the user
---@param uname? string
---@param invalidate_cached_value? boolean
---@return string?
function M.user_get_full_name(uname, invalidate_cached_value)
	uname = uname or vim.env.USER
	if user_full_name_cached.full_name and uname == user_full_name_cached.uname and invalidate_cached_value ~= true then
		return user_full_name_cached.full_name
	end

	local res = M.command("getent", "passwd", uname):execute()
	if res:is_ok() then
		local text = res.output[1]

		user_full_name_cached.uname = uname
		user_full_name_cached.full_name = string.match(text, "^.+:.+:.+:.*:(.*):.*:.*$")
		return user_full_name_cached.full_name
	else
		return nil
	end
end

function M.datetime()
	return os.date("!*t")
end

return M
