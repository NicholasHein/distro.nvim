require("lib.type.table")
local Object = require("lib.object")

---@class Win
---@field id integer
local Win = Object.class({}, { new_func = false })

---@param id integer
---@return Win
function Win:from_id(id)
	return Object.new(self, { id = id })
end

---Get the window ID
---@return integer
function Win:as_id()
	return self.id
end

---Create a window object from the current window
---@return Win
function Win:current()
	return self:from_id(vim.api.nvim_get_current_win())
end

---@class WinFloatOptions
---@field buffer? integer|Buf
---@field enter? boolean
---@field width? integer
---@field height? integer
---@field border? string
---@field options? table<string, any>

---Create a new floating window
---@param opts? WinFloatOptions
---@return Win
function Win:new_float(opts)
	local dwidth = math.ceil(math.min(vim.o.columns, math.max(80, vim.o.columns - 20)))
	local dheight = math.ceil(math.min(vim.o.lines, math.max(20, vim.o.lines - 10)))
	opts = ({
		buffer = 0,
		enter = true,
		width = dwidth,
		height = dheight,
		border = "single",
		options = {
			winhighlight = nil,
			winblend = 5,
		},
	}):deep_merge("force", opts or {})

	local row = math.ceil(vim.o.lines - opts.height) * 0.5 - 1
	local col = math.ceil(vim.o.columns - opts.width) * 0.5 - 1

	local win = vim.api.nvim_open_win(opts.buffer, opts.enter, {
		row = row,
		col = col,
		relative = "editor",
		width = opts.width,
		height = opts.height,
		border = opts.border,
	})

	for name, value in pairs(opts.options) do
		vim.wo[win][name] = value
	end
	return Object.new(self, { id = win })
end

---Set the window as the current one
function Win:set_current()
	vim.api.nvim_set_current_win(self.id)
end

---@alias WinSplitDir string
---|'"default"'
---|'"horizontal"'
---|'"vertical"'
---|'"tab"'

---Split the window in a direction
---@param dir WinSplitDir
---@return Win
function Win:split(dir)
	self:set_current()

	local dir_cmds = {
		default = "split",
		horizontal = "rightbelow split",
		vertical = "rightbelow vsplit",
		tab = "tab split",
	}

	vim.api.nvim_exec2(dir_cmds[dir], { output = true })
	return self:current()
end

---Split and set the new buffer
---@param dir WinSplitDir
---@param buf Buf|integer
---@return Win
function Win:split_with_buf(dir, buf)
	local newwin = self:split(dir)
	newwin:set_buffer(buf)
	return newwin
end

---Get the current buffer number
---@return integer
function Win:get_bufnr()
	return vim.api.nvim_win_get_buf(self.id)
end

---Get the window's current buffer object
---@return Buf
function Win:get_buffer()
	local Buf = require("lib.ui.buf")
	return Buf:from_id(self:get_bufnr())
end

---Set the window's current buffer
---@param buf Buf|integer
function Win:set_buffer(buf)
	local Buf = require("lib.ui.buf")

	local bufid
	if type(buf) == "number" then
		bufid = buf
	else
		bufid = Buf.as_id(buf--[[@as Buf]] )
	end

	vim.api.nvim_win_set_buf(self.id, bufid)
end

---Set the size in rows/columns
---@param size { width: integer?, height: integer? }
function Win:set_size(size)
	if size.width then
		vim.api.nvim_win_set_width(self.id, size.width)
	end
	if size.height then
		vim.api.nvim_win_set_height(self.id, size.height)
	end
end

---Get the size in rows/columns
---@return { width: integer?, height: integer? }
function Win:get_size()
	return {
		width = vim.api.nvim_win_get_width(self.id),
		height = vim.api.nvim_win_get_height(self.id),
	}
end

---Change the size by adding/subtracting rows/columns
---@param sizeadd { width: integer?, height: integer? }
---@return { width: integer?, height: integer? }
function Win:resize(sizeadd)
	local size = self:get_size()
	local newsize = {}
	if sizeadd.width then
		newsize.width = size.width + sizeadd.width
	end
	if sizeadd.height then
		newsize.height = size.height + sizeadd.height
	end
	self:set_size(newsize)
	return newsize
end

---Close a window
---@param force? boolean
---@return boolean
function Win:close(force)
	if not vim.api.nvim_win_is_valid(self.id) then
		return false
	end
	vim.api.nvim_win_close(self.id, force or false)
	return true
end

---Return the position of the cursor
---@return { line: integer, col: integer }
function Win:cursor()
	local line, col = unpack(vim.api.nvim_win_get_cursor(self.id))
	return { line = line, col = col }
end

---Return text on a line before the given position
---@param pos { line: integer, col: integer }
---@return string
function Win:text_on_line_before(pos)
	if pos.col == 0 then
		return "" -- Cursor in col #0 means this is impossible
	end

	local lines = vim.api.nvim_buf_get_lines(self:get_bufnr(), pos.line - 1, pos.line, true)
	if #lines == 0 then
		return "" -- Unloaded buffer
	end

	return lines[1]:sub(1, pos.col)
end

---Return the character preceeding the cursor
---@return string?
function Win:cursor_char_before()
	local before = self:text_on_line_before(self:cursor())
	if before == "" then
		return nil
	end

	return before:sub(-1, -1)
end

---Check if the character before the cursor is a word (based on nvim-cmp example)
---@return boolean
function Win:cursor_follows_word()
	local prev = self:cursor_char_before()
	if prev == nil then return false end
	return prev:match("%s") == nil
end

return Win
