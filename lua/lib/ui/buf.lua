require("lib.type.table")
local Object = require("lib.object")
local iter = require("lib.iter")

---@class Buf
---@field id integer
local Buf = Object.class({}, { new_func = false })

---@class BufOptions
---@field listed? boolean
---@field scratch? boolean
---@field options? table<string, any>

---Create a new buffer
---@param opts? BufOptions
---@return Buf
function Buf:new(opts)
	opts = ({
		listed = true,
		scratch = false,
		opions = {},
	}):deep_merge("force", opts or {})

	local buf = vim.api.nvim_create_buf(opts.listed, opts.scratch)
	for name, value in pairs(opts.options) do
		vim.bo[buf][name] = value
	end

	return Object.new(self, { id = buf })
end

---Create a new scratch buffer
---@return Buf
function Buf:new_scratch()
	return Buf:new({
		listed = true,
		scratch = true,
		options = {
			bufhidden = "hide",
			swapfile = false,
		},
	})
end

---@param id integer
---@return Buf
function Buf:from_id(id)
	return Object.new(self, { id = id })
end

---Get the buffer ID
---@return integer
function Buf:as_id()
	return self.id
end

---Iterate loaded buffers
---@return OrderedIter
function Buf:iter_loaded()
	local i = iter.new(vim.api.nvim_list_bufs()):map(function(key, buf)
		return key, vim.api.nvim_buf_is_loaded(buf) and self:from_id(buf) or nil
	end)
	return iter.enumerate(i)
end

---Create a buffer object from the current window
---@return Buf
function Buf:current()
	return Buf:from_id(vim.api.nvim_get_current_buf())
end

---Set the buffer as the current one
function Buf:set_current()
	vim.api.nvim_set_current_buf(self.id)
end

---Delete a buffer
---@param opts { force: boolean?, unload: boolean? }
---@return boolean
function Buf:delete(opts)
	if not vim.api.nvim_buf_is_valid(self.id) then
		return false
	end
	vim.api.nvim_buf_delete(self.id, opts or {})
	return true
end

return Buf
