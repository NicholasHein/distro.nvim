local M = {}

---Return true if the Ex command exists
---@param name string
---@return boolean
function M.command_exists(name)
	return vim.fn.exists((":%s"):format(name)) ~= 0
end

---@class DisplayString: string
---@field [1] string
local DisplayString = { "" }
DisplayString.__index = DisplayString

function DisplayString:new(str)
	return setmetatable({ str }, self)
end

---@return string
function DisplayString:as_str()
	return self[1] or ""
end

DisplayString.__tostring = DisplayString.as_str

---@return integer
function DisplayString:strlen()
	return #(self:as_str())
end

---@return integer
function DisplayString:cell_width()
	return vim.api.nvim_strwidth(self:as_str())
end

---@generic A: string, B: string
---@param a A
---@param b B
---@return DisplayString
function DisplayString.concat(a, b)
	return DisplayString:new(tostring(a) .. tostring(b))
end

DisplayString.__concat = DisplayString.concat

---@param i integer
---@param j? integer
---@return DisplayString
function DisplayString:sub(i, j)
	return self:new(self[1]:sub(i, j))
end

---@param n integer
---@param sep? string
---@return DisplayString
function DisplayString:rep(n, sep)
	return self:new(self[1]:rep(n, sep))
end

---Cell-representation of a string on a display
---@param str string
---@return DisplayString
function M.to_dstr(str)
	return DisplayString:new(str)
end

return M
