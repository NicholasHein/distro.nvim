local Handle = require("lib.event.handle")
local Object = require("lib.object")
local uv = vim.loop

---@alias StrBuffer string|string[]

---@class Stream: Handle
local Stream = Object.class({}, { parents = { Handle } })
Stream.new = Object.new

---@class StreamReader
---@field on_stop fun()
local StreamReader = Object.class {}
StreamReader.new = Object.new

---@class StreamWriter
local StreamWriter = Object.class({}, { parents = { Handle } })
StreamWriter.new = Object.new

---Start reading from a stream
---@param callback fun(err: string?, data: string?)
---@return StreamReader
function Stream:read(callback)
	assert(uv.read_start(self.inner, callback))
	local function on_stop()
		assert(uv.read_stop(self.inner))
	end
	return StreamReader:new { on_stop = on_stop }
end

---Stop reading
function StreamReader:stop()
	self.on_stop()
end

---Write data (and/or send a handle over a pipe)
---@param data StrBuffer
---@param callback? fun(err: string?)
---@param send_handle? Handle
---@return StreamWriter
function Stream:write(data, callback, send_handle)
	local writer
	if send_handle == nil then
		writer = assert(uv.write(self.inner, data, callback))
	else
		writer = assert(uv.write2(self.inner, data, send_handle.inner, callback))
	end
	return StreamWriter:new { inner = writer }
end

---Try writing data without blocking (and/or send a handle over a pipe)
---@param data StrBuffer
---@param send_handle? Handle
---@return integer
function Stream:try_write(data, send_handle)
	local len
	if send_handle == nil then
		len = assert(uv.try_write(self.inner, data))
	else
		len = assert(uv.try_write2(self.inner, data, send_handle.inner))
	end
	return len
end

---Check if the stream is readable
---@return boolean
function Stream:is_readable() return uv.is_readable(self.inner) end

---Check if the stream is writable
---@return boolean
function Stream:is_writable() return uv.is_writable(self.inner) end

---@class StreamShutdown: Handle
local StreamShutdown = Object.class({}, { parents = { Handle } })
StreamShutdown.new = Object.new

---Shutdown a stream
---@param callback? fun(err: string?)
---@return StreamShutdown
function Stream:shutdown(callback)
	return StreamShutdown:new { inner = assert(uv.shutdown(self.inner, callback)) }
end

---Start listening for incoming connections on a stream
---@param backlog integer
---@param callback fun(err: string?)
function Stream:listen(backlog, callback)
	assert(uv.listen(self.inner, backlog, callback))
end

---Accept a client stream
---@param client Stream
function Stream:accept(client)
	assert(uv.accept(self.inner, client.inner))
end

return Stream
