local Handle = require("lib.event.handle")
local Object = require("lib.object")
local uv = vim.loop

---@class Async: Handle
local Async = Object.class({}, { parents = { Handle } })
Async.new = Object.new

---Call an async handle's callback
---@param ... any
function Async:send(...)
	assert(uv.async_send(self.inner, ...))
end

return Async
