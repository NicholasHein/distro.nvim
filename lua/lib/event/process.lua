local Handle = require("lib.event.handle")
local Object = require("lib.object")
local uv = vim.loop

---@class Process: Handle
---@field pid integer
local Process = Object.class({ pid = -1 }, { parents = { Handle }, new_func = false })

---@class ProcessSpawnOptions
---@field args? string[]
---@field stdio? { [1]: integer|userdata?, [2]: integer|userdata?, [3]: integer|userdata? }
---@field env? table<string, string>
---@field cwd? string
---@field uid? integer
---@field gid? integer
---@field verbatim? boolean
---@field detached? boolean
---@field hide? boolean

---Spawn a new process
---@param path string
---@param options ProcessSpawnOptions
---@param on_exit fun(code: integer, signal: integer)
---@return Process
function Process:spawn(path, options, on_exit)
	local hdata, pid = uv.spawn(path, options, on_exit)
	return Object.new(self, { inner = hdata, pid = pid })
end

---Create a process handle from a PID
---@param pid integer
---@return Process
function Process:from_pid(pid)
	return Object.new(self, { inner = nil, pid = pid })
end

---Kill a process
---@param signum integer|string
function Process:kill(signum)
	if self.inner then
		assert(uv.process_kill(self.inner, signum))
	else
		assert(uv.kill(self.pid, signum))
	end
end

return Process
