local Handle = require("lib.event.handle")
local Stream = require("lib.event.stream")
local Object = require("lib.object")
local uv = vim.loop

---@class Pipe: Stream, Handle
local Pipe = Object.class({}, { parents = { Stream } })

---Create a new pipe
---@param ipc? boolean
---@return Pipe
function Pipe:new(ipc)
	return Object.new(self, {
		inner = assert(uv.new_pipe(ipc)),
	})
end

---Open an existing file descriptor as a pipe
---@param fd integer
function Pipe:reopen(fd)
	assert(uv.pipe_open(self.inner, fd))
end

---Bind to a named pipe
---@param path string
function Pipe:bind(path)
	assert(uv.pipe_bind(self.inner, path))
end

---@class PipeConnection: Handle
local PipeConnection = Object.class({}, { parents = { Handle } })

---Connect to a named pipe
---@param path string
---@param callback? fun(err: string?)
---@return PipeConnection
function Pipe:connect(path, callback)
	return PipeConnection:new {
		inner = assert(uv.pipe_connect(self.inner, path, callback)),
	}
end

---@class PipeFlags
---@field nonblock? boolean

---Create a pair of connected pipe handles
---@param rflags? PipeFlags
---@param wflags? PipeFlags
---@return { read: Pipe, write: Pipe }
function Pipe.pipe(rflags, wflags)
	local pipefds = assert(uv.pipe(rflags, wflags))

	return {
		read = Pipe:new():reopen(pipefds.read),
		write = Pipe:new():reopen(pipefds.write),
	}
end

return Pipe
