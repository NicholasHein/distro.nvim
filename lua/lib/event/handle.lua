local Object = require("lib.object")
local uv = vim.loop

---@class HandleData: userdata

---@class Handle
---@field inner HandleData
---@field refs integer
local Handle = Object.class {}

function Handle:new(o)
	o = o or {}
	o.refs = 1
	return Object.new(self, o)
end

---Close a handle
---@param callback? function
function Handle:close(callback)
	uv.close(self.inner, callback)
end

---Increment the reference count
function Handle:ref()
	uv.ref(self.inner)
	self.refs = self.refs + 1
end

---Decrement the reference count
function Handle:unref()
	uv.unref(self.inner)
	self.refs = self.refs - 1
	if self.refs == 0 then
		self:close()
	end
end

---Get the file number
---@return integer?
function Handle:fileno()
	local fileno = assert(uv.fileno(self.inner))
	return fileno
end

return Handle
