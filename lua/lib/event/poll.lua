local Handle = require("lib.event.handle")
local Object = require("lib.object")
local uv = vim.loop

---@class Poll: Handle
local Poll = Object.class({}, { parents = { Handle } })
Poll.new = Object.new

---@alias PollEvents string
---|'"r"' # Read
---|'"w"' # Write
---|'"d"' # Disconnect
---|'"p"' # Prioritized
---|'"rw"'
---|'"rd"'
---|'"wd"'
---|'"rwd"'
---|'"rp"'
---|'"wp"'
---|'"rwp"'
---|'"dp"'
---|'"rdp"'
---|'"wdp"'
---|'"rwdp"'

---Start polling
---@param events PollEvents
---@param callback any
function Poll:start(events, callback)
	assert(uv.poll_start(self.inner, events, callback))
end

---Stop polling
function Poll:stop()
	assert(uv.poll_stop(self.inner))
end

return Poll
