local Handle = require("lib.event.handle")
local Object = require("lib.object")
local uv = vim.loop

---@class Timer: Handle
local Timer = Object.class({}, { parents = { Handle } })
Timer.new = Object.new

---Start a timer
---@param timeout integer Milliseconds
---@param interval integer
---@param callback function
function Timer:start(timeout, interval, callback)
	assert(uv.timer_start(self.inner, timeout, interval, callback))
end

---Stop a timer
function Timer:stop()
	assert(uv.timer_stop(self.inner))
end

return Timer
