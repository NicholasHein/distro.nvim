---@alias Object
local Object = {}
Object.__index = Object

---Create a new meta-object
---@generic O
---@param cls O
---@param o any
---@return O
function Object.new(cls, o)
	return setmetatable(o or {}, cls)
end

---@class ClassAttribs
---@field parents? table[]
---@field new_func? string|boolean

---Create a class
---@generic O
---@param cls O
---@param attribs ClassAttribs?
---@return O
function Object.class(cls, attribs)
	attribs = attribs or {}

	if attribs.parents and #attribs.parents > 0 then
		-- One (or more) parents
		local parent = attribs.parents[1]
		for k, v in pairs(parent) do
			if k:find("__") == 1 then
				cls[k] = v
			end
		end

		if #attribs.parents == 1 then
			setmetatable(cls, parent)
		else
			-- Multiple parents
			local parents = attribs.parents or {}

			local mt = {}
			mt.__index = function(_, key)
				for i = 1, #parents do
					local val = parents[i][key]
					if val then return val end
				end
			end
			setmetatable(cls, mt)
		end
	end

	if attribs.new_func ~= false then
		local new_func_name = "new"
		if attribs.new_func ~= true and attribs.new_func ~= nil then
			assert(type(attribs.new_func) == "string" and attribs.new_func ~= "")
			new_func_name = attribs.new_func --[[@as string]]
		end
		cls[new_func_name] = Object.new
	end

	-- Prepare `cls` to be the metatable of its instances
	cls.__index = cls
	return cls
end

---Check if an object is an instance of a class
---@generic O
---@param cls O
---@param obj any
---@return boolean
function Object.is_instance(cls, obj)
	local mt = getmetatable(obj)
	while mt do
		if mt == cls then
			return true
		end
		mt = getmetatable(mt)
	end
	return false
end

return Object
