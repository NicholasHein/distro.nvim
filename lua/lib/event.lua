local uv = vim.loop

local M = {}



---Create a new timer
---@return Timer
function M.new_timer()
	local Timer = require("lib.event.timer")

	local res, err, name = uv.new_timer()
	return Timer:new { inner = Timer.unwrap_result(res, err, name) }
end

---Defer a function
---@param fn function
---@param timeout integer Milliseconds
---@return Timer
function M.defer(fn, timeout)
	local Timer = require("lib.event.timer")

	return Timer:new { inner = vim.defer_fn(fn, timeout) }
end

---Create an async callback
---@param callback fun(...)
---@return Async
function M.async(callback)
	local Async = require("lib.event.async")

	local res, err, name = uv.new_async(callback)
	return Async:new { inner = Async.unwrap_result(res, err, name) }
end

---Create a poll handle
---@param fd integer
---@return Poll
function M.poll_fd(fd)
	local Poll = require("lib.event.poll")

	local res, err, name = uv.new_poll(fd)
	return Poll:new { inner = Poll.unwrap_result(res, err, name) }
end

---Create a poll handle for a socket
---@param fd integer
---@return Poll
function M.poll_socket(fd)
	local Poll = require("lib.event.poll")

	local res, err, name = uv.new_socket_poll(fd)
	return Poll:new { inner = Poll.unwrap_result(res, err, name) }
end

---Spawn a new process
---@param path string
---@param options ProcessSpawnOptions
---@param on_exit fun(code: integer, signal: integer)
---@return Process
function M.spawn(path, options, on_exit)
	local Process = require("lib.event.process")
	return Process:spawn(path, options, on_exit)
end

---Create a process handle from a PID
---@param pid integer
---@return Process
function M.process_from_pid(pid)
	local Process = require("lib.event.process")
	return Process:from_pid(pid)
end

---Create a connected pair of pipes
---@param rflags? PipeFlags
---@param wflags? PipeFlags
---@return { read: Pipe, write: Pipe }
function M.pipe_pair(rflags, wflags)
	local Pipe = require("lib.event.pipe")

	return Pipe.pipe(rflags, wflags)
end

---Bind to a named pipe
---@param path string
---@return Pipe
function M.bind(path)
	local Pipe = require("lib.event.pipe")

	local pipe = Pipe:new()
	pipe:bind(path)
	return pipe
end

return M
