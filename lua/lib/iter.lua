local Object = require("lib.object")

local M = {}

---@class Iterable<K, V>: table<K, V>
---@class OrderedIterable<V>: Iterable<number, V>

---@class Iter<K, V>: Iterable<K, V>
---@field iterable Iterable
Iter = Object.class {
	iterable = {},
}

---@class OrderedIter<V>: Iter<number, V>
---@field iterable Iterable
OrderedIter = Object.class({
	iterable = {},
}, { parents = { Iter } })

---@class ProcessedIter<K, V>: Iter<K, V>
---@field iterable Iterable
---@field mapper function(k, v): any, any?
ProcessedIter = Object.class({
	iterable = {},
	mapper = function(k, v) return k, v end,
}, { parents = { Iter } })

---@class FilteredIter<K, V>: Iter<K, V>
---@field iterable Iterable
---@field predicate function(k, v): boolean
FilteredIter = Object.class({
	iterable = {},
	predicate = function(_, _) return true end,
}, { parents = { Iter } })

---@generic I: Iter
---@param o? I
---@return I
function Iter:new(o)
	return Object.new(self, o)
end

function Iter:__pairs()
	return next, self.iterable, nil
end

---Iterate an iterator
---@generic I: Iter, K, V
---@return (fun(iter: I): K, V), I
function Iter:iter()
	return pairs(self)
end

---Collect iterated objects
---@generic K, V
---@return table<K, V>
function Iter:collect()
	local ret = {}
	for k, v in self:iter() do
		ret[k] = v
	end
	return ret
end

---Process iterated objects
---@generic K, V
---@param fn fun(k: K, v: V): any, any?
---@return ProcessedIter
function Iter:map(fn)
	return ProcessedIter:new { iterable = self, mapper = fn }
end

---Iterate by calling a closure
---@generic K, V
---@param fn fun(k: K, v: V): any
function Iter:for_each(fn)
	for k, v in pairs(self) do
		fn(k, v)
	end
end

---Filter iterated objects
---@generic K, V
---@param pred fun(k: K, v: V): boolean
---@return FilteredIter
function Iter:filter(pred)
	return FilteredIter:new { iterable = self, predicate = pred }
end

---@generic I: OrderedIter
---@param o? I
---@return I
function OrderedIter:new(o)
	return Iter.new(self, o)
end

function OrderedIter:__pairs()
	return coroutine.wrap(function()
		for i, v in ipairs(self.iterable) do
			coroutine.yield(i, v)
		end
	end)
end

---@generic I: ProcessedIter
---@param o? I
---@return I
function ProcessedIter:new(o)
	return Iter.new(self, o)
end

function ProcessedIter:__pairs()
	return coroutine.wrap(function()
		for k, v in pairs(self.iterable) do
			local newk, newv = self.mapper(k, v)
			if newk ~= nil and newv ~= nil then
				coroutine.yield(newk, newv)
			end
		end
	end)
end

---@generic I: FilteredIter
---@param o? I
---@return I
function FilteredIter:new(o)
	return Iter.new(self, o)
end

function FilteredIter:__pairs()
	return coroutine.wrap(function()
		for k, v in pairs(self.iterable) do
			if self.predicate(k, v) then
				coroutine.yield(k, v)
			end
		end
	end)
end

---Create an iterator
---@generic T: Iterable
---@param tbl T
---@return Iter
function M.new(tbl)
	return Iter:new { iterable = tbl }
end

---Create an ordered iterator
---@generic T: OrderedIterable
---@param tbl T
---@return OrderedIter
function M.enumerate(tbl)
	return OrderedIter:new { iterable = tbl }
end

---Create an processed iterator
---@generic T: Iterable, K, V
---@param tbl T
---@param fn fun(k: K, v: V): any, any?
---@return ProcessedIter
function M.transform(tbl, fn)
	return ProcessedIter:new { iterable = tbl, mapper = fn }
end

---Create an filtered iterator
---@generic T: Iterable, K, V
---@param tbl T
---@param pred fun(k: K, v: V): boolean
---@return FilteredIter
function M.filter(tbl, pred)
	return FilteredIter:new { iterable = tbl, predicate = pred }
end

M.Iter = Iter
M.OrderedIter = OrderedIter
M.ProcessedIter = ProcessedIter
M.FilteredIter = FilteredIter

return M
