require("lib.type.list")

local M = {}

---@class KeymapArgs
---@field [1] string|fun()
---@field mode? string|string[]
---@field opts? table

---@class KeymapFilter
---@field mode? string|string[]
---@field buffer? number|boolean

---@alias CachedKeymap table

---Configure a keymap
---@param lhs string|string[]
---@param args KeymapArgs
function M.set(lhs, args)
	local lhs_list = list.from(lhs)
	local mode = list.from(args.mode or { "n" })

	for _, l in pairs(lhs_list) do
		vim.keymap.set(mode, l, args[1], args.opts)
	end
end

---Set all mappings
---@param map table<string, KeymapArgs>
function M.map_global(map)
	for lhs, args in pairs(map) do
		args.opts = args.opts or {}
		args.opts.buffer = nil
		M.set(lhs, args)
	end
end

---Set all mappings with a buffer
---@param map table<string, KeymapArgs>
---@param bufnr number
function M.map_local(map, bufnr)
	for lhs, args in pairs(map) do
		args.opts = args.opts or {}
		args.opts.buffer = bufnr
		M.set(lhs, args)
	end
end

---@param f? KeymapFilter
---@return KeymapFilter
local function filter_normalize(f)
	f = f or {}
	f.mode = list.from(f.mode or { "" })
	if f.buffer then
		f.buffer = type(f.buffer) == "boolean" and 0 or f.buffer
	end
	return f
end

---Delete a keymap
---@param lhs string|string[]
---@param filter? KeymapFilter
function M.del(lhs, filter)
	filter = filter_normalize(filter)

	local lhs_list = list.from(lhs)
	for _, l in pairs(lhs_list) do
		vim.keymap.del(filter.mode, l, { buffer = filter.buffer })
	end
end

---Return a cached version of the keymap
---@param filter? KeymapFilter
---@return CachedKeymap[]
function M.cache(filter)
	filter = filter_normalize(filter)

	local cache = {}
	for _, mode in pairs(filter.mode) do
		if filter.buffer then
			list.extend(cache, vim.api.nvim_buf_get_keymap(filter.buffer, mode))
		else
			list.extend(cache, vim.api.nvim_get_keymap(mode))
		end
	end
	return cache
end

---Remove and return a keymap
---@param lhs string|string[]
---@param filter? KeymapFilter
---@param cache? CachedKeymap[]
---@return CachedKeymap[]
function M.remove(lhs, filter, cache)
	local lhs_list = list.from(lhs)
	filter = filter_normalize(filter)

	if not cache then
		cache = M.cache(filter)
	end

	local saved = {}
	for _, keymap in pairs(cache) do
		if list.contains(lhs_list, keymap.lhs) and (filter.mode == "" or list.contains(filter.mode, keymap.mode)) then
			table.insert(saved, keymap)
		end
	end

	M.del(lhs, filter)
	return saved
end

---Restore cached keymaps
---@param cache CachedKeymap[]
function M.restore(cache)
	for _, keymap in pairs(cache) do
		local opts = {
			callback = keymap.callback,
			desc = keymap.desc,
			expr = keymap.expr,
			noremap = keymap.noremap,
			nowait = keymap.nowait,
			script = keymap.script,
			sid = keymap.sid,
			silent = keymap.silent,
		}

		if keymap.buffer == 0 then
			vim.api.nvim_set_keymap(keymap.mode, keymap.lhs, keymap.rhs, opts)
		else
			vim.api.nvim_buf_set_keymap(keymap.buffer, keymap.mode, keymap.lhs, keymap.rhs, opts)
		end
	end
end

---Replace keymaps and cache removed ones
---@param lhs string|string[]
---@param args KeymapArgs
---@return CachedKeymap[]
function M.replace(lhs, args)
	local lhs_list = list.from(lhs)
	local mode = list.from(args.mode or { "n" })

	local filter = { mode = mode, buffer = (args.opts or {}).buffer }
	local cache = M.cache(filter)

	local saved = {}
	for _, keymap in pairs(cache) do
		if list.contains(lhs_list, keymap.lhs) and list.contains(mode, keymap.mode) then
			table.insert(saved, keymap)
		end
	end

	M.del(lhs, filter)
	return cache
end

---Override buffer-local keymaps with global mappings
---@param map table<string, KeymapArgs>
---@param buffer number
---@return CachedKeymap[]
local function buf_mask_all(map, buffer)
	local cache = M.cache({ buffer = buffer })

	local saved = {}
	for _, keymap in pairs(cache) do
		if map[keymap.lhs] then
			local mode = list.from(map[keymap.lhs].mode or { "n" })

			if list.contains(mode, keymap.mode) then
				table.insert(saved, keymap)
				M.del(keymap.lhs, { mode = keymap.mode, buffer = buffer })
			end
		end
	end

	return cache
end

---Override buffer-local keymaps with global ones
---@param map table<string, KeymapArgs>
---@return CachedKeymap[]
function M.mask_buf_mappings(map)
	local cache = {}

	for _, buf in pairs(vim.api.nvim_list_bufs()) do
		if vim.api.nvim_buf_is_loaded(buf) then
			local saved = buf_mask_all(map, buf)
			list.extend(cache, saved)
		end
	end

	for lhs, args in pairs(map) do
		M.set(lhs, args)
	end
	return cache
end

return M
