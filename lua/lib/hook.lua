local M = {}

---@class Hook: function
---@field [1] function
local Hook = {}
Hook.__index = Hook

---@param o? Hook
---@return Hook
function Hook:new(o)
	return setmetatable(o or {}, self)
end

---@generic A: function, B: function
---@param a A
---@param b B
---@return Hook
function Hook.__add(a, b)
	vim.validate({
		a = { a, { "f", "t" } },
		b = { a, { "f", "t" } },
	})

	return Hook:new({
		function(...)
			local res = {}
			res[1] = a(...)
			res[2] = b(...)
			return res
		end
	})
end

---@generic A: function, B: function
---@param a A
---@param b B
---@return Hook
function Hook.__bor(a, b)
	vim.validate({
		a = { a, { "f", "t" } },
		b = { a, { "f", "t" } },
	})

	return Hook:new({
		function(...)
			return b(a(...))
		end
	})
end

---@generic H: Hook
---@param self H
---@param ... any
---@return any?
function Hook.__call(self, ...)
	assert(type(self[1]) == "function")
	return self[1](...)
end

---Create a hook
---@param opts? Hook
---@return Hook
function M.new(opts)
	return Hook:new(opts)
end

---Wrap a hook function
---@param fn function
---@return Hook
function M.wrap(fn)
	return Hook:new { fn }
end

setmetatable(M, {
	---@param fn function
	---@return Hook
	__call = function(_, fn) return M.wrap(fn) end,
})

return M
