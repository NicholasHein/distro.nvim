local BuiltinLogger = require("lib.log.builtin")

local log = BuiltinLogger:new()
log.new = nil

function log:null()
	local NullLogger = require("lib.log.null")
	return NullLogger:new()
end

return log
