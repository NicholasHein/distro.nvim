local LspClient = require("lib.lsp.client")
local Object = require("lib.object")
require("lib.type.table")

---@class LspServerDefinition
---@field name string
---@field server? table
---@field before_setup? fun(self): boolean?
---@field before_init? fun(self, params: string[], config: table)
---@field init? fun(self, client: LspClient): boolean?
---@field attach? fun(self, client: LspClient, bufnr: integer)
---@field exit? fun(self, client: LspClient, code: integer, signal: integer?)
---@field is_deprecated? boolean

---@class LspServer : LspServerDefinition
---@field logger Logger
local LspServer = Object.class {}

---Create an LSP server config
---@param o? table
---@return LspServer
function LspServer:new(o)
	o = o or {}

	local BuiltinLogger = require("lib.log.builtin")
	return Object.new(self, table.override({
		server = {},
		logger = BuiltinLogger:new {
			name = "lsp-server",
			prefix = o.name or "???",
		},
	}, o or {}))
end

---Define an LSP server class
---@generic T: LspServer
---@param o LspServerDefinition
---@return T
function LspServer.define(o)
	return Object.class(o, { parents = { LspServer } })
end

---Get the server configuration
---@return table
function LspServer:config()
	return table.override({
		_super = self,
		before_setup = self.before_setup and function()
			local ret = self.before_setup(self)
			if ret == true and self.is_deprecated then
				vim.defer_fn(function()
					vim.notify_once(
						("%s is deprecated!"):format(self.name),
						vim.log.levels.WARN,
						{ title = "LSP Server" }
					)
				end, 1000)
				return false
			end
			return ret ~= false
		end,
		before_init = self.before_init and function(params, config)
			return self.before_init(self, params, config)
		end,
		on_init = self.init and function(client)
			local ret = self.init(self, LspClient:from(client))
			return ret ~= false and true or false
		end,
		on_attach = self.attach and function(client, bufnr)
			self.attach(self, LspClient:from(client), bufnr)
		end,
		on_exit = self.exit and function(code, signal, client)
			self.exit(self, LspClient:from(client), code, signal)
		end,
	}, self.server or {})
end

return LspServer
