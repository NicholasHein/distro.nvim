local LspRequest = require("lib.lsp.request")
local Object = require("lib.object")
local iter = require("lib.iter")
require("lib.type.io")
require("lib.type.string")
require("lib.type.table")

---@alias LspClientObject table A vim.lsp.client object

---@class LspClient
---@field inner table
---@field logger Logger
local LspClient = Object.class({}, { new_func = false, name = "LspClient" })

---Return an LSP client from a vim.lsp.client object
---@generic C: LspClient
---@param self C
---@param client_object LspClientObject
---@return C
function LspClient.from(self, client_object)
	local BuiltinLogger = require("lib.log.builtin")
	if client_object._super then
		return client_object._super
	end

	self = vim.deepcopy(self)
	self.__index = function(c, key)
		local mt = getmetatable(c) or {}
		local v = rawget(mt, key)
		if not v then
			local super = rawget(c, "_super") or {}
			return super[key]
		end
		return v
	end

	local client = Object.new(self, {
		inner = client_object,
		logger = BuiltinLogger:new {
			name = "lsp-client",
			prefix = ("%s:%u"):format(client_object.name, client_object.id),
		},
	})

	if self ~= LspClient then
		client_object._super = client
	end
	return client
end

---Return an LSP client from an ID
---@param id integer
---@return LspClient
function LspClient:from_id(id)
	local client = assert(vim.lsp.get_client_by_id(id))
	return self:from(client)
end

---Return the contained vim.lsp.client object
---@return LspClientObject
function LspClient:as_object()
	return self.inner
end

---Return the client ID
---@return integer
function LspClient:id()
	return self.inner.id
end

---Iterate active clients
---@generic C: LspClient
---@param filter? { id: integer?, bufnr: integer?, name: string? }
---@return fun(): integer, C
function LspClient:iter_active(filter)
	local client_objs = vim.lsp.get_active_clients(filter)
	return coroutine.wrap(function()
		for i, obj in ipairs(client_objs) do
			coroutine.yield(i, self:from(obj))
		end
	end)
end

---Apply a function on each client
---@param bufnr integer Buffer number or 0 for the current buffer
---@param func fun(client: LspClient, client_id: integer, bufnr: integer)
function LspClient:for_each(bufnr, func)
	vim.lsp.for_each_buffer_client(bufnr, function(client, client_id, buf)
		func(self:from(client), client_id, buf)
	end)
end

---Return true if the client is stopped
---@return boolean
function LspClient:is_stopped()
	return vim.lsp.client_is_stopped(self.inner.id)
end

---Stop the client
---@param force? boolean
function LspClient:stop(force)
	vim.lsp.stop_client(self.inner, force)
end

---Attach the client to a buffer
---@param bufnr integer
function LspClient:buf_attach(bufnr)
	vim.lsp.buf_attach_client(bufnr, self.inner.id)
end

---Detach the client from a buffer
---@param bufnr integer
function LspClient:buf_detach(bufnr)
	vim.lsp.buf_detach_client(bufnr, self.inner.id)
end

---Return a list of attached buffers
---@return integer[]
function LspClient:buffers()
	return vim.lsp.get_buffers_by_client_id(self.inner.id)
end

---Iterate the list of attached buffers
---@return fun(): integer, integer
function LspClient:iter_buffers()
	return coroutine.wrap(function()
		for i, bufnr in ipairs(self:buffers()) do
			coroutine.yield(i, bufnr)
		end
	end)
end

---Send a request to the server
---@param method string
---@param opts { params: table?, bufnr: integer?, handler: LspRequestHandler? }
---@return unknown
function LspClient:request(method, opts)
	opts = {}

	local handler = nil
	if opts.handler then
		handler = LspRequest.request_handler(opts.handler)
	end

	local ok, rid = self.inner.request(method, opts.params or vim.lsp.util.make_position_params(), handler,
		opts.bufnr)
	if ok then
		return Object.new(LspRequest, {
			id = rid,
			cancel = function(req)
				return self.inner.cancel_request(req:as_id())
			end,
		})
	else
		return nil
	end
end

---Change the client's settings (use at end of on_init function)
---@param ext table
---@return table
function LspClient:extend_settings(ext)
	local config = self.inner.config
	config.settings = table.override(config.settings, ext)

	self.inner.notify("workspace/didChangeConfiguration", { settings = config.settings })
	return config.settings
end

---Get server capabilities
---@return table
function LspClient:server_capabilities()
	return self.inner.server_capabilities
end

---@param path string|string[]
---@return string[]
local function cap_path_to_list(path)
	if type(path) == "string" then
		path = string.split(path, "[.]")
	end
	return path
end

---@param path string|string[]
---@return string
local function cap_path_to_string(path)
	if type(path) ~= "string" then
		path = table.concat(path, ".")
	end
	return path
end

---Get a client capability via path
---@param path string|string[]
---@return any?
function LspClient:resolve_capability(path)
	local p = cap_path_to_list(path)
	return table.traverse(self.inner.config.capabilities, unpack(p))
end

---Assert that a client capability equals an expected value
---@generic T
---@param path string|string[]
---@param expected T
---@param fmt? string
---@param ... any
---@return T?
function LspClient:assert_capability(path, expected, fmt, ...)
	local v = self:resolve_capability(path)
	if v == expected then
		return v
	end

	if fmt then
		self.logger:warn_once(fmt, ...)
	else
		self.logger:warn_once("client capability `%s` is not `%s`", cap_path_to_string(path), tostring(expected))
	end
	return v
end

---@alias LspWorkspaceFolder { name: string, uri: string }

---List the workspace folders
---@return LspWorkspaceFolder[]
function LspClient:workspace_folders()
	local folders = self.inner.workspace_folders
	if not folders then
		local rootdir = self.inner.config.root_dir
		folders = rootdir and { { name = rootdir, uri = ("file://%s"):format(rootdir) } }
	end
	return folders or {}
end

---Search below each workspace directory for a path/paths
---@param paths string[]
---@return table<string, LspWorkspaceFolder[]>
function LspClient:search_workspace(paths)
	local results = {}
	for _, folder in ipairs(self:workspace_folders()) do
		for _, path in ipairs(paths) do
			if io.stat(folder.name .. "/" .. path) then
				results[path] = folder
			end
		end
	end
	return results
end

---@return LspServer?
function LspClient:server()
	local LspServer = require("lib.lsp.server")

	local server = self.inner.config._super
	if Object.is_instance(LspServer, server) then
		return server
	else
		return nil
	end
end

return LspClient
