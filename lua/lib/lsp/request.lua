local Object = require("lib.object")

---@alias LspErrorCode integer 0 if ok, otherwise vim.lsp.protocol.ErrorCodes.*
---@alias LspRequestHandler fun(err: table?, result: table?, ctx: table, config: table): LspErrorCode?, string?, table?
---@alias LspNotificationHandler fun(result: table, ctx: table, config: table)

---@class LspRequest
---@field id integer
---@field cancel fun(self: LspRequest): boolean
local LspRequest = Object.class({}, { new_func = false })

---@return integer
function LspRequest:as_id()
	return self.id
end

---Wrap a request handler
---@param fn LspRequestHandler
---@return fun(err: table?, result: table?, ctx: table, config: table): boolean, { [1]: LspErrorCode, [2]: string, [3]: table? }
function LspRequest.request_handler(fn)
	return function(err, res, ctx, cfg)
		local rerr, rmsg, rdata = fn(err, res, ctx, cfg)
		return rerr == 0 or rerr == nil, vim.lsp.rpc.rpc_response_error(rerr, rmsg, rdata)
	end
end

return LspRequest
