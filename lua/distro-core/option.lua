---@class distro-core.Option<T>
---@field private [1] unknown
local Option = {}
Option.__index = Option

---@private
---@generic T
---@param value T
---@return distro-core.Option
function Option.new(value)
	return setmetatable({ value }, Option)
end

local EMPTY = Option.new(nil)

---@generic T
---@param value T
---@return distro-core.Option
function Option.some(value)
	return Option.new(value)
end

---@return distro-core.Option
function Option.none()
	return EMPTY
end

---@generic T
---@param value T?
---@return distro-core.Option
function Option.from_nilable(value)
	if value == nil then
		return Option.none()
	else
		return Option.new(value)
	end
end

---@return boolean
function Option:is_some()
	return self ~= EMPTY
end

---@return boolean
function Option:is_none()
	return self == EMPTY
end

---@generic T, U
---@param f fun(value: T): U
---@return distro-core.Option
function Option:map(f)
	if self:is_some() then
		return Option.some(f(self[1]))
	else
		return Option.none()
	end
end

---@generic T, U
---@param f fun(value: T): U?
---@return distro-core.Option
function Option:map_nilable(f)
	if self:is_some() then
		return Option.from_nilable(f(self[1]))
	else
		return Option.none()
	end
end

---@generic T
---@return T
function Option:unwrap()
	if self:is_some() then
		return self[1]
	else
		local Error = require("distro-core.error")
		Error.panic("no value", 2)

		error("unreachable", 1)
	end
end

---@generic T
---@param value T
---@return T
function Option:unwrap_or(value)
	if self:is_some() then
		return self[1]
	else
		return value
	end
end

---@generic T
---@param f fun(): T
---@return T
function Option:unwrap_or_else(f)
	if self:is_some() then
		return self[1]
	else
		return f()
	end
end

---@generic T
---@param f fun(value: T): distro-core.Option
---@return distro-core.Option
function Option:and_then(f)
	if self:is_some() then
		return f(self[1])
	else
		return self
	end
end

---@param f fun(): distro-core.Option
---@return distro-core.Option
function Option:or_else(f)
	if self:is_some() then
		return self
	else
		return f()
	end
end

---@generic T
---@param f fun(value: T)
---@return distro-core.Option
function Option:inspect(f)
	if self:is_some() then
		f(self[1])
	end
	return self
end

---@generic E
---@param err E
---@return distro-core.Result
function Option:ok_or(err)
	local Result = require("distro-core.result")
	if self:is_some() then
		return Result.ok(self[1])
	else
		return Result.err(err)
	end
end

---@generic E
---@param f fun(): E
---@return distro-core.Result
function Option:ok_or_else(f)
	local Result = require("distro-core.result")
	if self:is_some() then
		return Result.ok(self[1])
	else
		return Result.err(f())
	end
end

return Option
