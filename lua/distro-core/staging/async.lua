require("distro-core.staging")

local Future = require("distro-core.async.future")

---@class distro-core.async
local async = {}
async.Future = Future

function async.schedule()
	if vim.in_fast_event() then
		return
	end
	Future.new(vim.schedule):await()
end

---@return distro-core.Future
function async.sleep(ms)
	return Future.new(function(waker)
		vim.defer_fn(waker --[[@as fun()]], ms)
	end)
end

function async.run(idle_fn, callback, ...)
	local thread = coroutine.create(idle_fn)
	local is_cancelled = false
	local step
	step = function(...)
		if is_cancelled or not thread then
			return
		end

		local ok, output = coroutine.resume(thread, ...)
		if ok then
			if coroutine.status(thread) == "suspended" then
				if getmetatable(output) == Future then
					local future = output
					future(step)
				else
					-- Yield to parent coroutine
					step(coroutine.yield(output))
				end
			else
				callback(true, output)
				thread = nil
			end
		else
			callback(false, output)
			thread = nil
		end
	end

	step(...)
	return function()
		is_cancelled = true
		thread = nil
	end
end

function async.scope(idle_fn)
	return function(...)
		return async.run(idle_fn, function(ok, err)
			if not ok then
				error(err, 0)
			end
		end, ...)
	end
end

function async.run_blocking(idle_fn, ...)
	local done, ok, result
	local cancel_fn = async.run(idle_fn, function(o, r)
		done = true
		ok = o
		result = r
	end, ...)

	if done or vim.wait(0x7fffffff, function()
				return done == true
			end, 50) then
		if not ok then
			error(result, 2)
		end
		return result
	else
		cancel_fn()
		error("async function did not complete in time.", 2)
	end
end

return async
