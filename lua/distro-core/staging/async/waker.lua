require("distro-core.staging")

---@class distro-core.Waker
---@field private _wake fun()
---@field private _called boolean
---@overload fun(waker: distro-core.Waker)
local Waker = {}
Waker.__index = Waker

---@param wake fun(...)
function Waker.new(wake)
	return setmetatable({ _wake = wake, _called = false }, Waker --[[@as table]])
end

function Waker:wake()
	assert(not self._called)
	self._called = true
	self._wake()
end

function Waker:__call()
	self:wake()
end

return Waker
