require("distro-core.staging")

local Waker = require("distro-core.async.waker")

---@class distro-core.Future<T>
---@field private _poll fun(waker: distro-core.Waker): any?
local Future = {}
Future.__index = Future

---@generic T
---@param poll fun(waker: distro-core.Waker): T?
---@return distro-core.Future
function Future.new(poll)
	return setmetatable({
		_poll = poll,
		_done = false,
	}, Future)
end

---@param callback fun(...)
function Future:__call(callback)
	self._poll(Waker.new(callback))
end

---@generic T
---@return T
function Future:await()
	local ok, value = coroutine.yield(self)
	if not ok then
		error(value[1], 0)
	end
	return unpack(value)
end

return Future
