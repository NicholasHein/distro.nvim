local Module = require("distro-core.module")

local module_base = "distro-core"
local module_staged_base = "staging"
local modules_staged = {
	"async",
	"async.future",
	"async.uv",
	"async.waker",
	"sync.completion",
	"sync.mutex",
	"sync.wait_list",
}

for _, module in ipairs(modules_staged) do
	local modname_alias = ("%s.%s"):format(module_base, module)
	local modname_staged = ("%s.%s.%s"):format(module_base, module_staged_base, module)

	Module.lazy_preload(modname_staged, { alias = modname_alias })
end
