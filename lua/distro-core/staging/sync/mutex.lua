require("distro-core.staging")

local WaitList = require("distro-core.sync.wait_list")

---@class distro-core.MutexGuard
---@field private _unlock fun()
---@field private _poison fun()
---@field private _is_unlocked boolean
local MutexGuard = {}

function MutexGuard:unlock()
	assert(not self._is_unlocked)
	self._unlock()
	self._is_unlocked = true
end

function MutexGuard:__gc()
	if not self._is_unlocked then
		self._poison()
		error("mutex was never unlocked", 0)
	end
end

---@class distro-core.Mutex
---@field private _poisoned boolean
---@field private _owned? boolean
---@field private _waiters distro-core.WaitList
local Mutex = {}

---@return distro-core.Mutex
function Mutex.new()
	return setmetatable({
		_poisoned = false,
		_owned = false,
		_waiters = WaitList.new(),
	}, Mutex)
end

---@private
function Mutex:_poison()
	self._poisoned = true
	self._owned = nil
	self._waiters:wake_all()
end

---@private
function Mutex:_unlock()
	self._owned = false
	self._waiters:wake_one()
end

---@return distro-core.MutexGuard
function Mutex:lock()
	if self._owned == true then
		self._waiters:wait()
		-- Notified
		assert(self._owned ~= true)
	end

	if self._poisoned then
		error("mutex was poisoned because guard was collected without unlock", 0)
	end

	self._owned = true
	return setmetatable({
		_is_unlocked = false,
		_unlock = function() self:_unlock() end,
		_poison = function() self:_poison() end,
	}, MutexGuard)
end

return Mutex
