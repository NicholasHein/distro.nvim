require("distro-core.staging")

local Future = require("distro-core.async.future")

---@class distro-core.WaitList
---@field private _wakers distro-core.Waker[]
local WaitList = {}
WaitList.__index = WaitList

---@return distro-core.WaitList
function WaitList.new()
	return setmetatable({ _waiters = {} }, WaitList)
end

---@return boolean
function WaitList:try_wake_one()
	local waker = table.remove(self._wakers)
	if waker ~= nil then pcall(waker) end
	return waker ~= nil
end

function WaitList:wake_one()
	local woke_one = self:try_wake_one()
	assert(woke_one)
end

function WaitList:wake_all()
	while #self._wakers > 0 do
		self:wake_one()
	end
	self._wakers = {}
end

---@return distro-core.Future
function WaitList:join()
	return Future.new(function(waker)
		self._wakers[#self._wakers + 1] = waker
	end)
end

function WaitList:wait()
	self:join():await()
end

---@return integer
function WaitList:count()
	return #self._wakers
end

return WaitList
