require("distro-core.staging")

local WaitList = require("distro-core.sync.wait_list")

---@class distro-core.Completion
---@field private _done integer
---@field private _value any
---@field private _waitlist distro-core.WaitList
local Completion = {}

---@return distro-core.Completion
function Completion.new()
	return setmetatable({
		_done = 0,
		_value = nil,
		_waitlist = WaitList.new(),
	}, Completion)
end

---@return boolean
function Completion:done()
	return self._done ~= 0
end

function Completion:complete(value)
	assert(self._done == 0)
end

return Completion
