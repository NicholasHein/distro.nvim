---@class distro-core.Module
local Module = {}

---@param modname string
---@return unknown
local function module_reload(modname)
	local mod = require(modname)
	package.loaded[modname] = mod
	return mod
end

---@param modname string
function Module.unload(modname)
	package.loaded[modname] = nil
end

---@param modname string
---@param value any
function Module.inject(modname, value)
	package.loaded[modname] = value
end

---@param modname string
---@return unknown
function Module.lazy_require(modname)
	return setmetatable({}, {
		__index = function(_, key)
			local mod = module_reload(modname)
			return mod[key]
		end,
		__call = function(_, ...)
			local mod = module_reload(modname)
			return mod(...)
		end,
		__newindex = function(_, key, value)
			local mod = module_reload(modname)
			mod[key] = value
		end,
		__pairs = function(_)
			local mod = module_reload(modname)
			return pairs(mod)
		end,
	})
end

---@class distro-core.ModuleLazyPreloadOpts
---@field alias? string

---@param modname string Name of module to `require()`
---@param opts? distro-core.ModuleLazyPreloadOpts
---@return unknown
function Module.lazy_preload(modname, opts)
	opts = opts or {}
	local pkgloadname = opts.alias or modname

	local lazymod = Module.lazy_require(modname)
	package.loaded[pkgloadname] = lazymod
	return lazymod
end

---@param modname string
---@return distro-core.Result
function Module.try_require(modname)
	local Result = require("distro-core.result")
	return Result.try(require, modname)
end

return Module
