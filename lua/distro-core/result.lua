local Error = require("distro-core.error")

---@class distro-core.Result<T, E>
---@field private [1] any | distro-core.Error
local Result = {}
Result.__index = Result

---@private
---@generic T
---@param value T
---@return distro-core.Result
function Result.new(value)
	return setmetatable({
		value,
	}, Result)
end

---@generic T
---@param value T
---@return distro-core.Result
function Result.ok(value)
	return Result.new(value)
end

---@generic E
---@param error E
---@return distro-core.Result
function Result.err(error)
	if not Error.is_error(error) then
		error = Error.new(error)
	end
	return Result.new(error)
end

---@generic T, E
---@param ok boolean
---@param value_or_error T | E
---@return distro-core.Result
function Result.maybe(ok, value_or_error)
	if ok then
		return Result.ok(value_or_error)
	else
		return Result.err(value_or_error)
	end
end

---@return boolean
function Result:is_ok()
	return not Error.is_error(self[1])
end

---@return boolean
function Result:is_err()
	return Error.is_error(self[1])
end

---@generic T, D
---@param value D
---@return T | D
function Result:unwrap_or(value)
	if self:is_ok() then
		return self[1]
	else
		return value
	end
end

---@generic T, E, D
---@param f fun(err: E): D
---@return T | D
function Result:unwrap_or_else(f)
	if self:is_ok() then
		return self[1]
	else
		return f(self[1]:error())
	end
end

---@generic T
---@return T?
function Result:unwrap_or_nil()
	return self:unwrap_or(nil)
end

---@generic E
---@return E?
function Result:unwrap_err_or_nil()
	if self:is_err() then
		return self[1]:error()
	end
end

---@generic T
---@return T
function Result:unwrap()
	if self:is_ok() then
		return self[1]
	else
		local e = self[1] --[[@as distro-core.Error]]
		Error.panic(e, 2)

		error("unreachable", 1)
	end
end

---@generic T, E
---@return boolean
---@return T?
function Result:unwrap_or_notify()
	if self:is_ok() then
		return true, self[1]
	else
		local e = self[1] --[[@as distro-core.Error]]
		Error.notify(e, 2)
		return false
	end
end

---@generic T, U
---@param f fun(value: T): U
---@return distro-core.Result
function Result:map(f)
	if self:is_ok() then
		return Result.ok(f(self[1]))
	else
		return self
	end
end

---@generic E, F
---@param f fun(err: E): F
---@return distro-core.Result
function Result:map_err(f)
	if self:is_err() then
		return Result.err(f(self[1]:error()))
	else
		return self
	end
end

---@generic T, U, E, F
---@param d fun(err: E): F
---@param f fun(value: T): U
---@return distro-core.Result
function Result:map_or_else(d, f)
	if self:is_ok() then
		return Result.ok(f(self[1]))
	else
		return Result.err(d(self[1]:error()))
	end
end

---@generic T, U
---@param f fun(value: T): U
---@return distro-core.Result
function Result:try_map(f)
	if self:is_ok() then
		local ok, result = pcall(f, self[1])
		if ok then
			return Result.ok(result)
		else
			return Result.err(result)
		end
	else
		return self
	end
end

---@generic T
---@param f fun(value: T): distro-core.Result
---@return distro-core.Result
function Result:and_then(f)
	if self:is_ok() then
		return f(self[1])
	else
		return self
	end
end

---@generic E
---@param f fun(err: E): distro-core.Result
---@return distro-core.Result
function Result:or_else(f)
	if self:is_err() then
		return f(self[1]:error())
	else
		return self
	end
end

---@generic T
---@param fn fun(value: T)
---@return distro-core.Result
function Result:inspect(fn)
	if self:is_ok() then
		fn(self[1])
	end
	return self
end

---@generic E
---@param fn fun(value: E)
---@return distro-core.Result
function Result:inspect_err(fn)
	if self:is_err() then
		fn(self[1]:error())
	end
	return self
end

---@return distro-core.Option
function Result:to_ok()
	local Option = require("distro-core.option")
	if self:is_ok() then
		return Option.some(self[1])
	else
		return Option.none()
	end
end

---@generic T
---@param f fun(...): T
---@param ...? any
---@return distro-core.Result
function Result.pcall(f, ...)
	local ok, res = pcall(f, ...)
	return Result.maybe(ok, res)
end

---@generic T, E
---@param f fun(...): T
---@param errfn fun(err: string): E
---@param ... any
---@return distro-core.Result
function Result.xpcall(f, errfn, ...)
	local ok, res = xpcall(f, errfn, ...)
	return Result.maybe(ok, res)
end

---@generic T
---@param f fun(...): T
---@param ... any
---@return distro-core.Result
function Result.try(f, ...)
	return Result.xpcall(f, function(err)
		local Trace = require("distro-core.trace")
		return Error.new(err, { backtrace = Trace.trace(1) })
	end, ...)
end

---@generic T
---@param f fun(try: fun(result: distro-core.Result)): T
---@return distro-core.Result # distro-core.Result<T>
function Result.try_await(f)
	local thread = coroutine.create(f)
	local step
	step = function(...)
		local ok, result = coroutine.resume(thread, ...)
		if not ok then
			return Result.err(result)
		end
		if coroutine.status(thread) == "dead" then
			if getmetatable(result) == Result then
				return result
			else
				return Result.ok(result)
			end
		elseif getmetatable(result) == Result then
			if result:is_err() then
				return result
			else
				return step(result[1])
			end
		else
			return step(coroutine.yield(result))
		end
	end
	return step(coroutine.yield)
end

return Result
