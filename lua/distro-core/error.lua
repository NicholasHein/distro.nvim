---@class distro-core.Error
---@field private [1] any
---@field private _source? distro-core.Error
---@field private _tostring fun(e: distro-core.Error): string
---@field private _backtrace? debuginfo[]
---@overload fun()
local Error = {}
Error.__index = Error

---@param e distro-core.Error
---@return string
local function error_tostring(e)
	local inner = e:error()
	if type(inner) == "string" then
		return inner
	else
		local mt = getmetatable(inner) or {}
		if mt.__tostring then
			return tostring(inner)
		else
			return ("`%s`"):format(vim.inspect(inner))
		end
	end
end

---@class distro-core.ErrorOpts
---@field source? distro-core.Error
---@field tostring? fun(e: distro-core.Error): string
---@field backtrace? debuginfo[]

---@generic E
---@param error E
---@param opts? distro-core.ErrorOpts
---@return distro-core.Error
function Error.new(error, opts)
	opts = opts or {}
	return setmetatable({
		error,
		_source = opts.source,
		_tostring = opts.tostring or error_tostring,
		_backtrace = opts.backtrace,
	}, Error --[[@as table]])
end

---@generic E
---@return E
function Error:error()
	return self[1]
end

---@generic E, F
---@return F?
function Error:source()
	return self._source
end

---@param t any
---@return boolean
function Error.is_error(t)
	return getmetatable(t) == Error
end

---@return string
function Error:tostring()
	return self:_tostring()
end

---@return string
function Error:format()
	local msg = self:tostring()
	local source_msgs = {}

	local e = self:source()
	while e ~= nil do
		local str = e:tostring()
		local line
		if e:source() ~= nil then
			line = ("|`-- %s"):format(str)
		else
			line = (" `-- %s"):format(str)
		end
		source_msgs[#source_msgs + 1] = line
		e = e:source()
	end

	if #source_msgs > 0 then
		return ("%s:\n%s"):format(msg, table.concat(source_msgs, "\n"))
	else
		return msg
	end
end

---@private
---@param backtrace debuginfo[]
---@return string
local function format_backtrace(backtrace)
	vim.print({ backtrace = backtrace })
	local lines = {}

	for i, frame --[[@as debuginfo]] in ipairs(backtrace) do
		local shortfile = vim.fn.fnamemodify(frame.source:sub(2), ":p:~:.")
		lines[#lines + 1] = ("    %d: %s:%u _in_ %s"):format(i, shortfile, frame.currentline, frame.name or "???")
	end

	return table.concat(lines, "\n")
end

---@param e distro-core.Error | string
---@param level? integer
function Error.panic(e, level)
	level = (level or 1) + 1

	local msg
	if Error.is_error(e) then
		msg = (e --[[@as distro-core.Error]]):format()
	else
		assert(type(e) == "string")
		msg = e
	end
	error(msg, level)
end

---@param e distro-core.Error | string
---@param level? integer
function Error.notify(e, level)
	local Trace = require("distro-core.trace")

	local msg, backtrace
	if Error.is_error(e) then
		msg = (e --[[@as distro-core.Error]]):format()
		backtrace = e._backtrace
	else
		assert(type(e) == "string")
		msg = e
	end

	level = level or 1
	if not backtrace then
		backtrace = Trace.trace(level + 1, "Sln")
	end

	local logmsg = table.concat({
		msg,
		"# stacktrace:",
		format_backtrace(backtrace),
	}, "\n")

	vim.notify(logmsg, vim.log.levels.ERROR)
end

function Error:__tostring()
	return self:_tostring()
end

function Error:__call()
	Error:panic()
end

---@overload fun(err: any): distro-core.Error
return setmetatable(Error --[[@as table]], {
	__call = function(_, ...)
		return Error.new(...)
	end,
}) --[[@as distro-core.Error]]
