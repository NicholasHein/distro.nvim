local Module = require("distro-core.module")

local Error = Module.lazy_require("distro-core.error")
local Option = Module.lazy_require("distro-core.option")
local Result = Module.lazy_require("distro-core.result")

return {
	Error = Error,
	Module = Module,
	Option = Option,
	Result = Result,
}
