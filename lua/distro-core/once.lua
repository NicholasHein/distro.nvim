---@class distro-core.Once<F>
---@field private [1] fun(...): ...
---@overload fun(...): ...
local Once = {}
Once.__index = Once

---@param f fun(...): ...
---@return distro-core.Once
function Once.new(f)
	local once = setmetatable({
		f,
	}, Once --[[@as metatable]])
	return once
end

---@param t distro-core.Once | any
---@return boolean
function Once.was_called(t)
	return getmetatable(t) == Once
end

function Once:__call(...)
	local f = self[1]
	setmetatable(self, { __call = function() end })
	return f(...)
end

---@overload fun(f: fun(...): ...): distro-core.Once
return setmetatable(Once --[[@as table]], {
	__call = function(_, f)
		return Once.new(f)
	end
}) --[[@as distro-core.Once]]
