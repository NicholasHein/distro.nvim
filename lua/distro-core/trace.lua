---@class distro-core.Trace
---@overload fun(level?: (integer | (async fun(...): ...)), what?: infowhat, opts?: distro-core.TraceOpts)
local M = {}

---@class distro-core.TraceMatch
---@field file_name? string
---@field file_pattern? string
---@field func? function
---@field func_name? string
---@field func_pattern? string

---Return true if all of the fields in `match` match the given `info`
---@param info debuginfo
---@param match distro-core.TraceMatch
---@return boolean
local function trace_info_matches(info, match)
	if info.source then
		if match.file_name then
			if not info.source:find(match.file_name, 1, true) then return false end
		end

		if match.file_pattern then
			if not info.source:find(match.file_name) then return false end
		end
	end

	if info.func and match.func then
		if info.func ~= match.func then return false end
	end

	if info.name then
		if match.func_name then
			if not info.name:find(match.func_name, 1, true) then return false end
		end

		if match.func_pattern then
			if not info.name:find(match.func_pattern) then return false end
		end
	end

	return true
end

---@param exclusions distro-core.TraceMatch[]
---@param info debuginfo
---@return distro-core.TraceMatch?
local function trace_info_find_exclusion(exclusions, info)
	for _, exclusion in ipairs(exclusions) do
		if trace_info_matches(info, exclusion) then
			return exclusion
		end
	end
	return nil
end

do
	---@type distro-core.TraceMatch[]
	local exclusions = {}

	---@return distro-core.TraceMatch[]
	function M.exclusions()
		return exclusions
	end

	---@param match distro-core.TraceMatch
	function M.exclude(match)
		exclusions[#exclusions + 1] = match
	end

	-- Exclude this module
	M.exclude({ file_name = debug.getinfo(1, "S").source })
end

---@param what infowhat
---@param level? integer
---@return debuginfo
function M.caller(what, level)
	level = level or 0
	--   0: debug.getinfo() itself
	--   1: M.caller() itself
	-- >=2: caller, caller's caller, ...
	local info = debug.getinfo(level + 2, what)
	return assert(info)
end

---@return string
function M.caller_source()
	local info = M.caller("S", 1 --[[ exclude M.caller_source() itself ]])
	return assert(info.source)
end

---@class distro-core.TraceOpts: table
---@field exclude? distro-core.TraceMatch

---Return a stack trace
---@param level? integer | (async fun(...): ...)
---@param what? infowhat
---@param opts? distro-core.TraceOpts
---@return debuginfo[]
function M.trace(level, what, opts)
	level = level or 1
	what = what or "Sln"
	opts = opts or {}

	level = level + 1
	local exclusions = vim.list_extend(opts.exclude or {}, M.exclusions())

	local frames = {}
	while true do
		local info = debug.getinfo(level, what)
		if not info then break end

		if trace_info_find_exclusion(exclusions, info) ~= nil then
			-- Exclude all calls below this frame
			frames = {}
		elseif info.what ~= "C" then
			frames[#frames + 1] = info
		end

		level = level + 1
	end

	return frames
end

return setmetatable(M --[[@as table]], {
	__call = function(_, level, what, opts)
		return M.trace(level, what, opts)
	end
}) --[[@as distro-core.Trace]]
