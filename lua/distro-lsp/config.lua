---@param name string
---@return fun(): boolean
local function has_plugin_func(name)
	return function()
		local ok, _ = pcall(require, name)
		return ok
	end
end

---@class distro-lsp.config.Opts
local opts = {
	---@type table<string, boolean>
	plugins = setmetatable({
		---@type boolean | (fun(): boolean)
		lspconfig = has_plugin_func("lspconfig"),
	}, {
		__index = function(t, k)
			local v = rawget(t, k)
			if type(v) == "function" then
				v = v()
				t[k] = v
			end
			return v
		end
	}),
}

---@class distro-lsp.Config
local Config = {}

---@param o distro-lsp.config.Opts
function Config:extend(o)
	opts = vim.tbl_deep_extend("force", opts, o)
end

return setmetatable(Config, {
	__index = function(_, k)
		local v = rawget(Config, k)
		if v == nil then
			v = opts[k]
		end
		return v
	end,
	__newindex = function(_, k, v)
		opts[k] = v
	end
})
