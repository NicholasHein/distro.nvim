local Server = require("distro-lsp.server")

---@alias distro-lsp.server.clangd.BackgroundIndexPriority
---| '"background"' # Minimum priority; runs on idle CPUs
---| '"low"' # Reduced priority compared to interactive work
---| '"normal"' # Same priority as other clangd work

---@class distro-lsp.server.clangd.Opts
---@field background_index_priority? distro-lsp.server.clangd.BackgroundIndexPriority

---@class distro-lsp.server.Clangd: distro-lsp.Server
local Clangd = Server.new("clangd")
Clangd.__index = Clangd

---@param o? distro-lsp.server.clangd.Opts
---@return distro-lsp.server.Clangd
function Clangd.new(o)
	return setmetatable({}, Clangd)
end

-- TODO: set --background-index-priority = low

return Clangd
