local M = {}

---@param o? distro-lsp.config.Opts
function M.setup(o)
	local Config = require("distro-lsp.config")
	Config:extend(o or {})
end

return M
