local function tbl_clear(t)
	for key, _ in pairs(t) do
		t[key] = nil
	end
	return t
end

local function tbl_set_all(t, v)
	for key, value in pairs(v) do
		t[key] = value
	end
	return t
end

---@type table<string, (table | (fun(config: table): table))[]>
local config_exts = {}

---@param config table
local function before_on_setup_hook(config)
	local extensions = config_exts[config.name] or {}
	local newconfig = config
	for _, ext in ipairs(extensions) do
		if type(ext) == "function" then
			newconfig = ext(newconfig)
		elseif type(ext) == "table" then
			newconfig = vim.tbl_deep_extend("force", newconfig, ext)
		end
	end
	tbl_set_all(tbl_clear(config), newconfig)
end

local M = {}

do
	local ok, lspconfig = pcall(require, "lspconfig")
	if ok then
		lspconfig.util.on_setup = lspconfig.util.add_hook_before(lspconfig.util.on_setup, before_on_setup_hook)
	end

	---@return boolean
	function M.is_installed()
		return ok
	end
end

---@param name string
---@param ext table | (fun(config: table): table)
function M.register_extension(name, ext)
	config_exts[name] = vim.list_extend(config_exts[name] or {}, { ext })
end

return M
