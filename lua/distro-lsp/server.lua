---@module "vim.lsp.protocol"
local protocol = require("vim.lsp.protocol")

---@enum vim.lsp.protocol.Methods
local LspMethods = protocol.Methods

---@class distro-lsp.Server
---@field name string
---@field before_init? fun(params: lsp.InitializeParams, config: vim.lsp.ClientConfig)
---@field on_init? elem_or_list<fun(params: lsp.InitializeParams, config: vim.lsp.ClientConfig)>
---@field on_exit? elem_or_list<fun(code: integer, signal: integer, client_id: integer)>
---@field on_attach? elem_or_list<fun(client: vim.lsp.Client, bufnr: integer)>
---@field on_error? fun(code: integer, err: string)
---@field private _inner distro-lsp.server.Inner
local Server = {}
Server.__index = Server

---@private
---@class distro-lsp.server.Inner
---@field _capabilities? lsp.ClientCapabilities
local ServerInner = {}
ServerInner.__index = ServerInner

---@param name string
---@return distro-lsp.Server
function Server.new(name)
	vim.validate { name = { name, "string" } }
	return setmetatable({
		name = name,
		_inner = setmetatable({}, ServerInner --[[@as table]]),
	}, Server --[[@as table]])
end

---@param id vim.lsp.protocol.Methods
---@param handler lsp.Handler
function Server:handler_register(id, handler)
	self:handler_register_all { [id] = handler }
end

---@param t table<vim.lsp.protocol.Methods, lsp.Handler>
function Server:handler_register_all(t)
	vim.validate { t = { t, "table" } }
	self._config = vim.tbl_deep_extend("force", self._config, { handlers = t })
end

---@return vim.lsp.ClientConfig
function Server:to_config()
	return vim.tbl_deep_extend("force", self._config, {
		on_init = self.init,
		on_attach = self.attach,
	})
end

return Server
