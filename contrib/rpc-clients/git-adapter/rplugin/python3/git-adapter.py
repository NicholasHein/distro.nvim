import pynvim
import git


@pynvim.plugin
class GitAdapter(object):
    def __init__(self, nvim):
        self.nvim = nvim

    @pynvim.autocmd("LspAttach")
    def on_lspattach()
